package maestro.mybooks.stores;

/**
 * Created by Artyom on 6/10/2015.
 */
public class StoreEntryItem {

    private String Title;
    private String Link;
    private String Type;

    public StoreEntryItem() {
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getLink() {
        return Link;
    }

    public void setLink(String link) {
        Link = link;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }
}
