package maestro.mybooks.stores;

import java.util.ArrayList;

/**
 * Created by Artyom on 6/10/2015.
 */
public class BaseStore {

    private String BaseUrl;
    private String SearchUrl;
    private ArrayList<StoreEntryItem> mAdditionalLinks;

    public BaseStore(String baseUrl) {
        BaseUrl = baseUrl;
    }

    public String getSearchUrl() {
        return SearchUrl;
    }

    public void setSearchUrl(String searchUrl) {
        SearchUrl = searchUrl;
    }

    public void addAdditionalLink(StoreEntryItem item) {
        if (mAdditionalLinks == null) {
            mAdditionalLinks = new ArrayList<>();
        }
        mAdditionalLinks.add(item);
    }

    public StoreEntryItem getAdditionalLink(int position) {
        return mAdditionalLinks != null && position < mAdditionalLinks.size() ? mAdditionalLinks.get(position) : null;
    }

    public boolean haveAdditionalLinks() {
        return mAdditionalLinks != null && mAdditionalLinks.size() > 0;
    }

}
