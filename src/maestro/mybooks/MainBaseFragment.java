package maestro.mybooks;

import android.app.Activity;
import android.support.v4.app.DialogFragment;

/**
 * Created by Artyom on 6/10/2015.
 */
public class MainBaseFragment extends DialogFragment {

    public Main main;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof Main) {
            main = (Main) activity;
        }
    }

    @Override
    public void onDetach() {
        main = null;
        super.onDetach();
    }
}
