package maestro.mybooks.utils;

import android.content.res.Resources;
import maestro.support.v1.svg.SVGHelper;

/**
 * Created by Artyom on 6/10/2015.
 */
public class SVGInstance extends SVGHelper.SVGHolder {

    private static SVGInstance instance;

    public static synchronized SVGInstance getInstance() {
        return instance;
    }

    public SVGInstance(Resources resources) {
        super(resources);
        instance = this;
    }

}
