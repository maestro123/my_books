package maestro.mybooks.utils;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by Artyom on 15.09.2014.
 */
public class Typefacer {

    public static Typeface rRegular;
    public static Typeface rLight;
    public static Typeface rBold;
    public static Typeface rMedium;
    public static Typeface rMediumItalic;

    public static void initialize(Context context) {
        rRegular = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Regular.ttf");
        rLight = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Light.ttf");
        rBold = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Bold.ttf");
        rMedium = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Medium.ttf");
        rMediumItalic = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-MediumItalic.ttf");
    }

}
