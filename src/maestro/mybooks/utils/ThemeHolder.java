package maestro.mybooks.utils;

import android.graphics.Color;

/**
 * Created by Artyom on 6/10/2015.
 */
public class ThemeHolder {

    private static volatile ThemeHolder instance;

    public static synchronized ThemeHolder getInstance() {
        return instance != null ? instance : (instance = new ThemeHolder());
    }

    private int PrimaryColor = Color.parseColor("#009688");
    private int SecondaryColor = Color.parseColor("#00695C");
    private int TextPrimaryColor = Color.parseColor("#212121");
    private int TextSecondaryColor = Color.parseColor("#727272");
    private int IconColor = Color.parseColor("#727272");

    public int getPrimaryColor() {
        return PrimaryColor;
    }

    public int getSecondaryColor() {
        return SecondaryColor;
    }

    public int getTextPrimaryColor() {
        return TextPrimaryColor;
    }

    public int getTextSecondaryColor() {
        return TextSecondaryColor;
    }

    public int getIconColor() {
        return IconColor;
    }
}
