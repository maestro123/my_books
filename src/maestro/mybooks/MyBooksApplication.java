package maestro.mybooks;

import android.app.Application;
import maestro.mybooks.utils.SVGInstance;
import maestro.mybooks.utils.Typefacer;
import maestro.support.v1.svg.SVGHelper;

/**
 * Created by Artyom on 6/10/2015.
 */
public class MyBooksApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Typefacer.initialize(this);
        SVGHelper.init(this);
        new SVGInstance(getResources());
    }
}
