package maestro.mybooks.ui;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.dream.android.mim.MIM;
import maestro.mybooks.Main;
import maestro.mybooks.MainBaseFragment;
import maestro.mybooks.R;
import maestro.mybooks.utils.SVGInstance;
import maestro.mybooks.utils.ThemeHolder;
import maestro.mybooks.utils.Typefacer;

import java.util.ArrayList;

/**
 * Created by artyom on 9/11/14.
 */
public class NavigationFragment extends MainBaseFragment implements AdapterView.OnItemClickListener {

    public static final String TAG = NavigationFragment.class.getSimpleName();

    private ListView mList;
    private NavigationAdapter mAdapter;
    private ImageView mIcon, mBackgroundImage;
    private TextView mUserName;
    private TextView mSpaceInfo;

    private MIM mim;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.navigation_fragment_view, null);
        mList = (ListView) v.findViewById(R.id.list);
        mList.setBackgroundColor(Color.WHITE);
        mList.setDividerHeight(0);
        final View mHeaderView = View.inflate(getActivity(), R.layout.navigation_header, null);
        mBackgroundImage = (ImageView) mHeaderView.findViewById(R.id.background_image);
        mIcon = (ImageView) mHeaderView.findViewById(R.id.icon);
        mUserName = (TextView) mHeaderView.findViewById(R.id.user_name);
        mSpaceInfo = (TextView) mHeaderView.findViewById(R.id.space_info);
        mUserName.setTypeface(Typefacer.rMedium);
        mSpaceInfo.setTypeface(Typefacer.rRegular);
        mList.addHeaderView(mHeaderView);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mList.setAdapter(mAdapter = new NavigationAdapter(getActivity()));
        mList.setOnItemClickListener(this);
        mAdapter.update(getItems());
        if (savedInstanceState == null) {
            NavigationItem item = mAdapter.getItem(1);
//            main.openFragment(item.Type);
//            main.commit();
        }
        mBackgroundImage.setBackgroundColor(ThemeHolder.getInstance().getPrimaryColor());

//        mim = new MIM(getActivity().getApplicationContext()).maker(new MIMResourceMaker())
//                .size((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 168, getResources().getDisplayMetrics()),
//                        (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 300, getResources().getDisplayMetrics()));
//
//        mim.to(mBackgroundImage, String.valueOf(R.drawable.drawer_image)).async();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (main != null) {
//            main.openFragment(mAdapter.getItem(position - 1).Type);
//            mAdapter.notifyDataSetChanged();
        }
    }

    private final class NavigationAdapter extends BaseAdapter {

        private static final int TYPE_SPACE = -2;
        public static final int TYPE_DIVIDER = -1;
        public static final int TYPE_NORMAL = 1;

        private Context mContext;
        private LayoutInflater mInflater;
        private NavigationItem[] mItems;
        private int verticalPadding;
        private int padding8dp;

        public NavigationAdapter(Context context) {
            mContext = context;
            mInflater = LayoutInflater.from(context);
            verticalPadding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, context.getResources().getDisplayMetrics());
            padding8dp = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, context.getResources().getDisplayMetrics());
        }

        public void update(NavigationItem[] items) {
            mItems = items;
        }

        @Override
        public int getCount() {
            return mItems != null ? mItems.length : 0;
        }

        @Override
        public NavigationItem getItem(int position) {
            return mItems[position];
        }

        @Override
        public long getItemId(int i) {
            NavigationItem item = getItem(i);
            return item.AdapterType == TYPE_NORMAL ? item.NavigationType.ordinal() : item.AdapterType;
        }

        @Override
        public int getItemViewType(int position) {
            return getItem(position).AdapterType;
        }

        @Override
        public boolean isEnabled(int position) {
            return getItem(position).AdapterType == TYPE_NORMAL;
        }

        @Override
        public View getView(int position, View v, ViewGroup parent) {
            final int itemType = getItemViewType(position);
            if (itemType == TYPE_SPACE) {
                if (v == null || !(v.getTag() instanceof SpaceHolder)) {
                    v = new Space(getActivity());
                    v.setMinimumHeight(verticalPadding);
                    v.setTag(new SpaceHolder());
                }
            } else if (itemType == TYPE_DIVIDER) {
                DividerHolder holder;
                if (v == null || !(v.getTag() instanceof DividerHolder)) {
                    v = mInflater.inflate(R.layout.navigation_item_divider, null);
                    holder = new DividerHolder(v);
                } else {
                    holder = (DividerHolder) v.getTag();
                }
                NavigationItem item = getItem(position);
                if (!TextUtils.isEmpty(item.Title)) {
                    holder.Title.setText(getItem(position).Title);
                    holder.Title.setVisibility(View.VISIBLE);
                    holder.SpaceView.setVisibility(View.GONE);
                } else {
                    holder.Title.setVisibility(View.GONE);
                    holder.SpaceView.setVisibility(View.VISIBLE);
                }
            } else if (itemType == TYPE_NORMAL) {
                Holder holder;
                if (v == null || !(v.getTag() instanceof Holder)) {
                    v = mInflater.inflate(R.layout.navigation_item_view, null);
                    holder = new Holder(v);
                    v.setTag(holder);
                } else {
                    holder = (Holder) v.getTag();
                }
                NavigationItem item = getItem(position);
//                int actualColor = item.Type.equals(main.getCurrentOpenType()) ? ThemeHolder.IconPrimaryColor : ThemeHolder.IconSecondaryColor;
                SVGInstance.getInstance().applySVG(holder.Image, item.Icon, ThemeHolder.getInstance().getIconColor());
                holder.Title.setText(item.Title);
                holder.Title.setTextColor(ThemeHolder.getInstance().getTextPrimaryColor());
//                holder.Title.setTextColor(actualColor);
            }
            return v;
        }

        class Holder {
            TextView Title;
            ImageView Image;

            public Holder(View itemView) {
                Title = (TextView) itemView.findViewById(R.id.title);
                Image = (ImageView) itemView.findViewById(R.id.image);
                Title.setTypeface(Typefacer.rMedium);
            }
        }

        class SpaceHolder {

        }

        class DividerHolder {
            TextView Title;
            View SpaceView;

            public DividerHolder(View itemView) {
                Title = (TextView) itemView.findViewById(R.id.title);
                Title.setTypeface(Typefacer.rMedium);
                SpaceView = itemView.findViewById(R.id.additional_space);
            }
        }

    }

    public final class NavigationItem {

        public Main.NAVIGATION_TYPE NavigationType;
        public String Title;
        public Object AdditionalObject;
        public int Icon;
        public int AdapterType = NavigationAdapter.TYPE_NORMAL;

        public NavigationItem(Main.NAVIGATION_TYPE type, String title, int icon) {
            NavigationType = type;
            Title = title;
            Icon = icon;
        }

        public NavigationItem(int adapterType) {
            AdapterType = adapterType;
        }

        public NavigationItem(String title, int adapterType) {
            AdapterType = adapterType;
            Title = title;
        }

    }

    private NavigationItem[] getItems() {
        final ArrayList<NavigationItem> items = new ArrayList<>();
        items.add(new NavigationItem(NavigationAdapter.TYPE_SPACE));
        items.add(new NavigationItem(Main.NAVIGATION_TYPE.PAGE, getString(R.string.recent), R.raw.ic_local_library));
        items.add(new NavigationItem(Main.NAVIGATION_TYPE.PAGE, getString(R.string.all_books), R.raw.ic_local_library));
        items.add(new NavigationItem(getString(R.string.book_stores), NavigationAdapter.TYPE_DIVIDER));
        items.add(new NavigationItem(Main.NAVIGATION_TYPE.STORE, "FeedBooks", R.raw.ic_local_mall));
        items.add(new NavigationItem(Main.NAVIGATION_TYPE.STORE, "FeedBooks", R.raw.ic_local_mall));
        items.add(new NavigationItem(Main.NAVIGATION_TYPE.STORE, "FeedBooks", R.raw.ic_local_mall));
        items.add(new NavigationItem(getString(R.string.collections), NavigationAdapter.TYPE_DIVIDER));
        items.add(new NavigationItem(Main.NAVIGATION_TYPE.COLLECTION, "Test collection", R.raw.ic_collection));
        items.add(new NavigationItem(NavigationAdapter.TYPE_SPACE));
        return items.toArray(new NavigationItem[items.size()]);
    }

}
