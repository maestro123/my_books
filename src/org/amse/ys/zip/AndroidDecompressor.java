package org.amse.ys.zip;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.zip.InflaterInputStream;

/**
 * Created by yanis on 2/25/15.
 */
public class AndroidDecompressor {

    private InputStream mInflaterStream = null;
    private Integer mId;
    private String mWho;

    /**
     * byte b[] -- target buffer for bytes; might be null
     */
    public int read(byte b[], int off, int len) throws IOException{
        if (b == null){
            b = new byte[len];
        }
        return mInflaterStream.read(b, off, len);
    }

    public int read() throws IOException {
        return mInflaterStream.read();
    }

    protected AndroidDecompressor() {}

    private static final AtomicInteger mDecompressorId = new AtomicInteger(0);
    private static final HashMap<Integer, AndroidDecompressor> mDecompressors = new HashMap<>();

    static void storeDecompressor(AndroidDecompressor decompressor) {
        synchronized (mDecompressors) {
            Integer id = 0;
            for (Map.Entry<Integer, AndroidDecompressor> entry : mDecompressors.entrySet()) {
                if (entry.getValue() == null) {
                    id = entry.getKey();
                    break;
                }
            }
            if (id == 0) {
                id = mDecompressorId.incrementAndGet();
            }
            decompressor.mId = id;
            mDecompressors.put(id, decompressor);
        }
    }

    static AndroidDecompressor init(MyBufferedInputStream is, LocalFileHeader header, String who) throws IOException {
        if (is == null || is.available() == 0){
            throw new ZipException("stream is empty");
        }
        AndroidDecompressor decompressor;
        switch (header.CompressionMethod) {
            case 0:
                decompressor = new AndroidDecompressor();
                decompressor.mInflaterStream = is;
                decompressor.mWho = who;
                return decompressor;
            case 8:
                synchronized (mDecompressors) {
                    decompressor = new AndroidDecompressor();
                    decompressor.mInflaterStream = new InflaterInputStream(is);
                    decompressor.mWho = who;
                    return decompressor;
                }
            default:
                throw new ZipException("Unsupported method of compression");
        }
    }

    public int available() throws IOException {
        return mInflaterStream.available();
    }

    public void close(){
        synchronized (mDecompressors) {
            mDecompressors.put(mId, null);
            try {
                mInflaterStream.close();
            } catch (IOException e) {
            }
        }
    }
}
