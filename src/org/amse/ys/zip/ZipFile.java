package org.amse.ys.zip;

import org.geometerplus.zlibrary.core.util.InputStreamHolder;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

public final class ZipFile {
    private final static Comparator<String> ourIgnoreCaseComparator = new Comparator<String>() {
        @Override
        public final int compare(String s0, String s1) {
            return s0.compareToIgnoreCase(s1);
        }
    };

    private final InputStreamHolder myStreamHolder;
    private final Map<String, LocalFileHeader> myFileHeaders =
            new TreeMap<String, LocalFileHeader>(ourIgnoreCaseComparator);

    private boolean myAllFilesAreRead;

    public ZipFile(InputStreamHolder streamHolder) {
        myStreamHolder = streamHolder;
    }

    public Collection<LocalFileHeader> headers() {
        try {
            readAllHeaders();
        } catch (Exception ignored){
        }

        synchronized (myFileHeaders) {
            return myFileHeaders.values();
        }
    }

    private boolean readFileHeader(MyBufferedInputStream baseStream, String fileToFind) throws IOException {
        LocalFileHeader header = new LocalFileHeader();
        header.readFrom(baseStream);

        if (header.Signature != LocalFileHeader.FILE_HEADER_SIGNATURE) {
            return false;
        }
        String fname = header.FileName;
        if (fname != null) {
            synchronized (myFileHeaders) {
                myFileHeaders.put(fname, header);
            }
            if (fname.equalsIgnoreCase(fileToFind)) {
                return true;
            }
        }
        if ((header.Flags & 0x08) == 0) {
            baseStream.skip(header.CompressedSize);
        } else {
            findAndReadDescriptor(baseStream, header);
        }
        return false;
    }

    private void readAllHeaders() throws IOException {
        if (myAllFilesAreRead) {
            return;
        }
        myAllFilesAreRead = true;

        MyBufferedInputStream baseStream = getBaseStream();
        baseStream.setPosition(0);
        synchronized (myFileHeaders) {
            myFileHeaders.clear();
        }

        try {
            while (baseStream.available() > 0) {
                readFileHeader(baseStream, null);
            }
        } finally {
            //if (!(myStreamHolder instanceof ZLZipEntryFile) || !((ZLZipEntryFile) this.myStreamHolder).isEntryInsideArchive()) {
            baseStream.close();
            storeBaseStream(baseStream);
            //}
        }
    }

    /**
     * Finds descriptor of the last header and installs sizes of files
     */
    private void findAndReadDescriptor(MyBufferedInputStream baseStream, LocalFileHeader header) throws IOException {
        final Decompressor decompressor = Decompressor.init(baseStream, header, "ZipFile: " + header.FileName);
        int uncompressedSize = 0;
        while (true) {
            int blockSize = decompressor.read(null, 0, 2048);
            if (blockSize <= 0) {
                break;
            }
            uncompressedSize += blockSize;
        }
        header.UncompressedSize = uncompressedSize;
        Decompressor.storeDecompressor(decompressor);
    }

    private final LinkedList<MyBufferedInputStream> myStoredStreams = new LinkedList<MyBufferedInputStream>();

    synchronized void storeBaseStream(MyBufferedInputStream baseStream) {
//        myStoredStreams.add(baseStream);
    }

    synchronized MyBufferedInputStream getBaseStream() throws IOException {
        final MyBufferedInputStream stored = myStoredStreams.poll();
        if (stored != null) {
            return stored;
        }
        return new MyBufferedInputStream(myStreamHolder);
    }

    private ZipInputStream createZipInputStream(LocalFileHeader header, String who) throws IOException {
        return new ZipInputStream(this, header, who);
    }

    public boolean entryExists(String entryName) {
        try {
            return getHeader(entryName) != null;
        } catch (IOException e) {
            return false;
        }
    }

    public int getEntrySize(String entryName) throws IOException {
        return getHeader(entryName).UncompressedSize;
    }

    public InputStream getInputStream(String entryName) throws IOException {
        return createZipInputStream(getHeader(entryName), "get " + entryName);
    }

    public LocalFileHeader getHeader(String entryName) throws IOException {
        synchronized (myFileHeaders) {
            if (!myFileHeaders.isEmpty()) {
                LocalFileHeader header = myFileHeaders.get(entryName);
                if (header != null) {
                    return header;
                }
                if (myAllFilesAreRead) {
                    throw new ZipException("Entry " + entryName + " is not found");
                }
            }
        }
        // ready to read file header
        MyBufferedInputStream baseStream = getBaseStream();
        baseStream.setPosition(0);
        try {
            while (baseStream.available() > 0 && !readFileHeader(baseStream, entryName)) {
            }
            synchronized (myFileHeaders) {
                final LocalFileHeader header = myFileHeaders.get(entryName);
                if (header != null) {
                    return header;
                }
            }
        } finally {
            baseStream.close();
            storeBaseStream(baseStream);
        }
        throw new ZipException("Entry " + entryName + " is not found");
    }
}
