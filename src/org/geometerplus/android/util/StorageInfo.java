package org.geometerplus.android.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.geometerplus.android.AdobeSDKWrapper.DebugLog;
import org.geometerplus.fbreader.Paths;

import android.content.Intent;
import android.os.Environment;

public class StorageInfo {
	private StorageInfo() {
		storageStateObservers = new ArrayList<StorageInfo.StorageStateObserver>();
	}

	private static class LazySingletonHolder {
		public static StorageInfo singletonInstance = new StorageInfo();
	}

	public static StorageInfo getInstance() {
		return LazySingletonHolder.singletonInstance;
	}

	public interface StorageStateObserver {
		void onPrimaryStorageStateChanged(boolean primaryStorageAvailable);

		void onAnyStorageStateChanged(Map<String, Boolean> storagesStates);
	}

	private static final String TAG = StorageInfo.class.getSimpleName();

	// Actual logic
	private static final long MIN_REFRESH_INTERVAL = 50; // milliseconds

	private static final String[] PROBLEMATIC_POSSIBLE_SDCARD_PATHS = {
			"/mnt/extsd", "/flash/sdcard", "/flash", "/usb1" };

	private boolean mExternalStorageAvailable = false;
	private boolean mExternalStorageWriteable = false;

	private boolean init = false;

	private String primaryExternalStoragePath;

	private Set<String> secondaryStorages = new HashSet<String>();

	private final Map<String, Boolean> storagesStatesForGeneralCheck = new HashMap<String, Boolean>();

	private List<StorageStateObserver> storageStateObservers;

	private long lastStorageDetectTime = 0;

	public void addObserver(StorageStateObserver observer) {
		storageStateObservers.add(observer);
	}

	public void removeObserver(StorageStateObserver observer) {
		storageStateObservers.remove(observer);
	}

	public void removeAllObservers() {
		storageStateObservers.clear();
	}

	public void notifyObserversPrimaryStorageChanged(boolean isStorageAvailable) {
		for (StorageStateObserver observer : storageStateObservers) {
			observer.onPrimaryStorageStateChanged(isStorageAvailable);
		}
	}

	private void notifyObserversAnyStorageChanged() {
        synchronized (storagesStatesForGeneralCheck){
            for (StorageStateObserver observer : storageStateObservers) {
                observer.onAnyStorageStateChanged(storagesStatesForGeneralCheck);
            }
        }
	}

	public void init() {
		if (!init) {
			initStoragesInfo();
			init = true;
		}

		initStoragesStates();
	}

	private void initStoragesStates() {
		updatePrimaryExternalStorageState();

        synchronized (storagesStatesForGeneralCheck){
            storagesStatesForGeneralCheck.clear();

            storagesStatesForGeneralCheck.put(primaryExternalStoragePath,
                    isPrimaryExternalStorageReady());

            for (String secondaryExternalStoragePath : getSecondaryStorages()) {
                storagesStatesForGeneralCheck.put(secondaryExternalStoragePath,
                        isStorageReady(secondaryExternalStoragePath));
            }
        }
	}

	private void refreshStorageInfo() {
		long currentTime = System.currentTimeMillis();

		// Dont allow storage check ofter than MIN_REFRESH_INTERVAL
		if (currentTime - lastStorageDetectTime > MIN_REFRESH_INTERVAL) {
			lastStorageDetectTime = currentTime;

			initStoragesInfo();
			initStoragesStates();
		}
	}

	private void initStoragesInfo() {
		secondaryStorages.clear();
		String[] storages = null;
		primaryExternalStoragePath = Paths.cardDirectory();

		String allStoragees = getAllExternalStoragesPaths();
		//*wtf
		try {
			storages = allStoragees.split("\r\n");
		} catch (Exception e) {
			
			DebugLog.e(e.getClass().toString(), e.getLocalizedMessage());
			// TODO: handle exception
		}

		if (storages != null) {
			for (int i = 0; i < storages.length; i++) {
				if (!storages[i].contentEquals(primaryExternalStoragePath)) {
					secondaryStorages.add(storages[i]);
				}
			}
		}
	}

	/**
	 * The correctness of this method is not guaranteed, but for now it's the
	 * best available way to find SD card path in cases, when
	 * {@link Paths#cardDirectory()} refers to built in device external storage.
	 * 
	 * @return paths to all external storages currectly mounted to divece.
	 */
	private String getAllExternalStoragesPaths() {
		try {
			Process process = Runtime.getRuntime().exec(
					new String[] { "mount" });

			BufferedReader reader = new BufferedReader(new InputStreamReader(
					process.getInputStream()));
			int read;
			char[] buffer = new char[1024];
			StringBuffer output = new StringBuffer();
			while ((read = reader.read(buffer)) > 0) {
				output.append(buffer, 0, read);
			}
			reader.close();
			process.waitFor();

			String[] lines = output.toString().split("\n");

			StringBuilder str = new StringBuilder();
			for (String line : lines) {
				String lowerLine = line.toLowerCase();
				if (lowerLine.contains("fuse")) {
					addStorageEntry(str, line);
				}

				if (lowerLine.contains("vfat") && // Only vfat
						lowerLine.contains("vold") && // Only Volume Manager
														// entries
						!lowerLine.contains("asec") && // No Remove Ap2SD
						!lowerLine.contains("secure")) {
					addStorageEntry(str, line);
				}
			}

			appendHardcodedSDCardPlacesIfPossible(str);

			return str.toString();
		} catch (Exception ex) {
			DebugLog.e(
					TAG,
					"getAlternateExternalStoragePath: "
							+ ex.getLocalizedMessage());
			return null;
		}
	}

	private void addStorageEntry(StringBuilder builder, String line) {
		String[] entry = line.split(" ");
		builder.append(entry[1]).append("\r\n");
	}

	private void appendHardcodedSDCardPlacesIfPossible(StringBuilder str) {
		for (String possibleStoragePath : PROBLEMATIC_POSSIBLE_SDCARD_PATHS) {
			if (isStorageReady(possibleStoragePath, true, false)) {
				str.append(possibleStoragePath).append("\r\n");
				DebugLog.w(TAG, "<" + possibleStoragePath
						+ "> found with help of new fix ");
			}
		}
	}

	private void updatePrimaryExternalStorageState() {
		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			mExternalStorageAvailable = mExternalStorageWriteable = true;
		} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
			mExternalStorageAvailable = true;
			mExternalStorageWriteable = false;
		} else {
			mExternalStorageAvailable = mExternalStorageWriteable = false;
		}
	}

	/**
	 * Checks state of all storages and notifies all observers if there are any
	 * changes.
	 * 
	 * @param intent
	 */
	public void checkStorage(Intent intent) {
		refreshStorageInfo();

		boolean oldPrimaryStorageReady = isPrimaryExternalStorageReady();
		updatePrimaryExternalStorageState();
		DebugLog.i(TAG, "Storage: "
				+ (intent != null ? intent.getData() : "force check")
				+ "; AVAILABLE = " + isPrimaryExternalStorageReady());
		boolean newPrimaryStorageReady = isPrimaryExternalStorageReady();
		boolean primaryStorageStateChanged = (oldPrimaryStorageReady != newPrimaryStorageReady);

		boolean anyStorageStateChanged = primaryStorageStateChanged;

        synchronized (storagesStatesForGeneralCheck){
		    storagesStatesForGeneralCheck.put(primaryExternalStoragePath, newPrimaryStorageReady);

            for (Map.Entry<String, Boolean> entry : storagesStatesForGeneralCheck.entrySet()) {
                boolean storageState = isStorageReady(entry.getKey());
                if (storageState != entry.getValue()) {
                    entry.setValue(storageState);
                    anyStorageStateChanged = true;
                }
            }
        }

		if (primaryStorageStateChanged) {
			notifyObserversPrimaryStorageChanged(newPrimaryStorageReady);
		}

		if (anyStorageStateChanged) {
			notifyObserversAnyStorageChanged();
		}
	}

	public boolean isPrimaryExternalStorageReady() {
		primaryExternalStoragePath = Paths.cardDirectory();
		return isStorageReady(primaryExternalStoragePath, true, false);
	}

	public boolean isStorageReady(String storagePath) {
		return isStorageReady(storagePath, false);
	}

	public boolean isStorageReady(String storagePath,
			boolean writePermissionNeeded) {
		return isStorageReady(storagePath, writePermissionNeeded, true);
	}

	/**
	 * Returns storage state.<br>
	 * <br>
	 * Some native formats require the storage to be writable to work correct,
	 * so only writable storages are accepted.
	 * 
	 * @param storagePath
	 *            can be directory or file
	 * @param writePermissionNeeded
	 *            if true, read-only storages are counted as ready
	 * @return true if storage is <b>READABLE</b> and (optionally) WRITEABLE.
	 */
	public boolean isStorageReady(String storagePath,
			boolean writePermissionNeeded, boolean log) {

		boolean available = false;

		if (storagePath == null) {
			Iterator<String> iterator = secondaryStorages.iterator();
			if (iterator.hasNext()) {
				storagePath = iterator.next();
			} else {
				DebugLog.w(TAG, "No secondary external folder");
			}
		}

		if (storagePath != null) {
			if (isZipped(storagePath)) {
				available = getStateForZipFile(storagePath);

				DebugLog.w(TAG, "Zip: available:" + available);
			} else {
				final File path = new File(storagePath);

				available = path.exists() && path.canRead();
				if (writePermissionNeeded) {
					available = available && path.canWrite();
				}

				if (log) {
					DebugLog.w(
							TAG,
							"<" + storagePath + "> Exists: " + path.exists()
									+ "; Read: " + path.canRead() + "; Write: "
									+ path.canWrite() + "; IsDir: "
									+ path.isDirectory());// + "; Files: "
									//+ Arrays.toString(path.list()));
				}
			}
		}

		return available;
	}

	private boolean isZipped(String storagePath) {
		boolean isZip = false;
		if (storagePath.contains(".zip:")) {
			isZip = true;
		}
		return isZip;
	}

	private boolean getStateForZipFile(String storagePath) {
		String zipFilePath = storagePath.substring(0,
				storagePath.lastIndexOf(':'));
		DebugLog.w(TAG, "Zip path: " + zipFilePath);

		if (isZipped(zipFilePath)) {
			// avoid infinite recursion
			return false;
		} else {
			return isStorageReady(zipFilePath);
		}
	}

	public void forceCheckStorage() {
		checkStorage(null);
	}

	public String getPrimaryExternalStorage() {
		refreshStorageInfo();

		return primaryExternalStoragePath;
	}

	public List<String> getAllStorages() {
		refreshStorageInfo();

		ArrayList<String> allStorages = new ArrayList<String>();

		allStorages.add(primaryExternalStoragePath);
		allStorages.addAll(secondaryStorages);

		return allStorages;
	}

	private synchronized List<String> getSecondaryStorages() {
		ArrayList<String> tempSecondaryStorages = new ArrayList<String>();

		tempSecondaryStorages.addAll(secondaryStorages);

		return tempSecondaryStorages;
	}

	public List<String> getSecondaryStoragesThatAreNotInsidePrimary() {
		refreshStorageInfo();

		ArrayList<String> secondaryStoragesThatAreNotInsidePrimary = new ArrayList<String>();

		for (String secondaryStorage : secondaryStorages) {
			if (!secondaryStorage.contains(primaryExternalStoragePath)) {
				secondaryStoragesThatAreNotInsidePrimary.add(secondaryStorage);
			}
		}

		return secondaryStoragesThatAreNotInsidePrimary;
	}
}