package org.geometerplus.android.util;

import org.geometerplus.android.AdobeSDKWrapper.DebugLog;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Same as {@link StorageStateBroadcastReceiver}, but must run on :library process.
 * @author Anton Rutkevich anton.rutkevich@boolbalabs.com<br>
 * Oct 9, 2012 BoolbaLabs LLC.
 *
 */
public class LibraryStorageStateBroadcastReceiver extends BroadcastReceiver {
	@Override
	public void onReceive(Context context, Intent intent) {
		DebugLog.w("LibraryStorageStateBroadcastReceiver", "Thread " + context.getMainLooper().getThread().getId());
		StorageInfo.getInstance().checkStorage(intent);
	}
}