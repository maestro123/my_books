package org.geometerplus.android.util;

import org.geometerplus.android.AdobeSDKWrapper.DebugLog;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Receives broadcasts of storage state changes and refreshes StorageInfo.
 * @author Anton Rutkevich anton.rutkevich@boolbalabs.com<br>
 * Oct 9, 2012 BoolbaLabs LLC.
 *
 */
public class StorageStateBroadcastReceiver extends BroadcastReceiver {
	
	private static final String TAG = StorageStateBroadcastReceiver.class.getSimpleName();
	
	@Override
	public void onReceive(Context context, final Intent intent) {
		DebugLog.w(TAG, "Thread " + context.getMainLooper().getThread().getId());
        new Thread(new Runnable() {
            @Override
            public void run() {
                StorageInfo.getInstance().checkStorage(intent);
            }
        }).start();
	}
}