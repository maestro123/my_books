/*
 * Copyright (C) 2009-2011 Geometer Plus <contact@geometerplus.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General public synchronized License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General public synchronized License for more details.
 *
 * You should have received a copy of the GNU General public synchronized License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

package org.geometerplus.android.fbreader.library;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteStatement;
import android.util.Pair;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.prestigio.android.ereader.read.djvu.DjVuBookmark;
import com.prestigio.android.ereader.utils.Utils;
import com.prestigio.ereader.book.CollectionsManager;
import org.geometerplus.android.AdobeSDKWrapper.DebugLog;
import org.geometerplus.android.util.SQLiteUtil;
import org.geometerplus.fbreader.library.*;
import org.geometerplus.zlibrary.core.config.ZLConfig;
import org.geometerplus.zlibrary.core.filesystem.ZLFile;
import org.geometerplus.zlibrary.core.options.ZLIntegerOption;
import org.geometerplus.zlibrary.core.options.ZLStringOption;
import org.geometerplus.zlibrary.text.view.ZLTextFixedPosition;
import org.geometerplus.zlibrary.text.view.ZLTextPosition;
import org.geometerplus.zlibrary.ui.android.library.ZLAndroidApplication;

import java.io.File;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

public final class SQLiteBooksDatabase extends BooksDatabase {

    private static SQLiteBooksDatabase ourInstance = null;
    private static final AtomicBoolean mMigrateInProgress = new AtomicBoolean(false);
    private static final ReentrantLock mLock = new ReentrantLock();
    private static final CountDownLatch migrateGate = new CountDownLatch(1);
    private static final LinkedList<InitializedCallback> mQueue = new LinkedList<>();

    public synchronized static void migrate() {
        if (ourInstance == null) {
            ourInstance = new SQLiteBooksDatabase(ZLAndroidApplication.Instance());
        }
        synchronized (migrateGate) {
            migrateGate.countDown();
            SQLiteBooksDatabase.Instance();
            for (InitializedCallback item : mQueue) {
                item.run();
            }
        }
    }

    public synchronized static void removeCallback(InitializedCallback callback) {
        synchronized (migrateGate) {
            mQueue.remove(callback);
        }
    }

    public synchronized static boolean initialized(InitializedCallback callback) {
        synchronized (migrateGate) {
            if (migrateGate.getCount() > 0) {
                mQueue.add(callback);
                SQLiteBooksDatabase.migrate();
                return false;
            } else {
                callback.run();
                return true;
            }
        }
    }

    public synchronized static SQLiteBooksDatabase Instance() {
        try {
            migrateGate.await();
        } catch (InterruptedException e) {
            return null;
        }
        FileInfoSet.init(ourInstance);
        return ourInstance;
    }

    private volatile SQLiteDatabase myDatabase;

    private SQLiteBooksDatabase(final Context context) {

        try {
            myDatabase = context.openOrCreateDatabase("books.db", Context.MODE_PRIVATE, null);
        } catch (Exception e) {
            e.printStackTrace();
        }

        boolean ok;
        try {
            ok = migrate(context);
        } catch (Exception e) {
            ok = false;
        }
        if (!ok) {
            boolean deleted = false;
            try {
                deleted = context.deleteDatabase("books.db");
            } catch (Exception e) {
                DebugLog.e(e.getClass().toString(), e.getLocalizedMessage());
            }
            if (deleted) {
                try {
                    myDatabase = context.openOrCreateDatabase("books.db", Context.MODE_PRIVATE, null);
                } catch (Exception e) {
                    DebugLog.e(e.getClass().toString(), e.getLocalizedMessage());
                }
                try {
                    ok = migrate(context);
                } catch (Exception e) {
                    ok = false;
                }
                if (!ok) {
                    Tracker t = ZLAndroidApplication.Instance().getTracker();
                    t.send(new HitBuilders.EventBuilder()
                            .setCategory("SQLiteBooksDatabase")
                            .setAction("migrate")
                            .setLabel("cannot_migrate")
                            .setValue(1)
                            .build());
                    // TODO: WAT?
                }
            } else {
                Tracker t = ZLAndroidApplication.Instance().getTracker();
                t.send(new HitBuilders.EventBuilder()
                        .setCategory("SQLiteBooksDatabase")
                        .setAction("migrate")
                        .setLabel("cannot_drop_database")
                        .setValue(1)
                        .build());
                //EasyTracker.getTracker().sendEvent("SQLiteBooksDatabase", "migrate", "cannot_drop_database", (long) 1);
            }
        }
    }

    public synchronized void executeAsATransaction(Runnable actions) {
        boolean transactionStarted = false;
        try {
            myDatabase.beginTransaction();
            transactionStarted = true;
        } catch (Throwable t) {
        }
        try {
            actions.run();
            if (transactionStarted) {
                myDatabase.setTransactionSuccessful();
            }
        } finally {
            if (transactionStarted) {
                try {
                    myDatabase.endTransaction();
                } catch (Throwable t) {
                }
            }
        }
    }

    public static boolean testDatabase(Context context) {
        Tracker t = ZLAndroidApplication.Instance().getTracker();
        boolean allOk = true;
        try {
            SQLiteDatabase myDatabase = context.openOrCreateDatabase("books.db", Context.MODE_PRIVATE, null);
            Cursor cursor = myDatabase.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = 'test_table'", null);
            if (cursor != null) {
                if (cursor.getCount() > 0) {
                    cursor.close();
                    myDatabase.execSQL("DROP TABLE IF EXISTS test_table");
                }
                cursor.close();
            }
            myDatabase.execSQL("CREATE TABLE test_table("
                    + "book_id INTEGER PRIMARY KEY,"
                    + "encoding TEXT,"
                    + "language TEXT,"
                    + "title TEXT NOT NULL,"
                    + "file_name TEXT UNIQUE NOT NULL)");
            myDatabase.execSQL("insert into test_table(book_id, language, title, file_name) values (1, 'english', 'title', 'file_name')");
            myDatabase.execSQL("DROP TABLE IF EXISTS test_table");
            myDatabase.close();
        } catch (Exception e) {
            t.send(new HitBuilders.EventBuilder()
                    .setCategory("SQLiteDatabase")
                    .setAction("check_error")
                    .setLabel(e.getMessage())
                    .setValue(1)
                    .build());
            Boolean deleted = null;
            try {
                if (!(deleted = context.deleteDatabase("books.db"))) {
                    t.send(new HitBuilders.EventBuilder()
                            .setCategory("SQLiteDatabase")
                            .setAction("cannot_delete_database")
                            .setLabel(e.getMessage())
                            .setValue(1)
                            .build());
                }
                SQLiteDatabase myDatabase = context.openOrCreateDatabase("books.db", Context.MODE_PRIVATE, null);
                Cursor cursor = myDatabase.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = 'test_table'", null);
                if (cursor != null) {
                    if (cursor.getCount() > 0) {
                        cursor.close();
                        myDatabase.execSQL("DROP TABLE IF EXISTS test_table");
                    }
                    cursor.close();
                }
                myDatabase.execSQL("CREATE TABLE test_table("
                        + "book_id INTEGER PRIMARY KEY,"
                        + "encoding TEXT,"
                        + "language TEXT,"
                        + "title TEXT NOT NULL,"
                        + "file_name TEXT UNIQUE NOT NULL)");
                myDatabase.execSQL("insert into test_table(book_id, language, title, file_name) values (1, 'english', 'title', 'file_name')");
                myDatabase.execSQL("DROP TABLE IF EXISTS test_table");
                myDatabase.close();
            } catch (Exception e2) {
                if (deleted == null) {
                    t.send(new HitBuilders.EventBuilder()
                            .setCategory("SQLiteDatabase")
                            .setAction("recreate_error")
                            .setLabel(e2.getMessage()).setValue(1).build());
                } else if (deleted) {
                    t.send(new HitBuilders.EventBuilder()
                            .setCategory("SQLiteDatabase")
                            .setAction("recreate_error_db_deleted")
                            .setLabel(e2.getMessage()).setValue(1).build());
                } else {
                    t.send(new HitBuilders.EventBuilder()
                            .setCategory("SQLiteDatabase")
                            .setAction("recreate_error_db_not_deleted")
                            .setLabel(e2.getMessage()).setValue(1).build());
                }
                allOk = false;
            }
        }

        return allOk;
    }

    private synchronized boolean migrate(Context context) {
        final int version = myDatabase.getVersion();
        final int currentVersion = 20;
        if (version >= currentVersion) {
            return true;
        }
        DebugLog.w("SQL", "migrate from " + version + " to " + currentVersion);
        /*UIUtil.wait((version == 0) ? "creatingBooksDatabase" : "updatingBooksDatabase", new Runnable() {
            public synchronized void run() {*/
        myDatabase.beginTransaction();

        try {

            switch (version) {
                case 0:
                    createTables();
                case 1:
                    updateTables1();
                case 2:
                    updateTables2();
                case 3:
                    updateTables3();
                case 4:
                    updateTables4();
                case 5:
                    updateTables5();
                case 6:
                    updateTables6();
                case 7:
                    updateTables7();
                case 8:
                    updateTables8();
                case 9:
                    updateTables9();
                case 10:
                    updateTables10();
                case 11:
                    updateTables11();
                case 12:
                    updateTables12();
                case 13:
                    updateTables13();
                case 14:
                    updateTables14();
                case 15:
                    updateTables15();
                case 16:
                    updateTables16();
                case 17:
                    updateTables17();
                case 18:
                    updateTables18();
                case 19:
                    updateTables19();
            }

            myDatabase.setTransactionSuccessful();
            myDatabase.endTransaction();

            myDatabase.execSQL("VACUUM");
            myDatabase.setVersion(currentVersion);
        } catch (Exception e) {

            Tracker t = ZLAndroidApplication.Instance().getTracker();
            t.send(new HitBuilders.EventBuilder()
                    .setCategory("SQLiteBooksDatabase")
                    .setAction("migrate")
                    .setLabel("exception, migrate from: " + version + ", to: " + currentVersion)
                    .setValue(1)
                    .build());

            StackTraceElement[] stack = e.getStackTrace();
            StringBuilder builder = new StringBuilder();
            for (StackTraceElement line : stack) {
                builder.append(line.toString());
                builder.append("\n" +
                        "");
            }

            t.send(new HitBuilders.ExceptionBuilder()
                    .setDescription(builder.toString())
                    .setFatal(false)
                    .build());

//            Log.w("SQLiteBooksDatabase", "migrate got exception(" + e.getLocalizedMessage() + "), try to cleanup database!");
            myDatabase.endTransaction();

            myDatabase.beginTransaction();

            myDatabase.execSQL("PRAGMA writable_schema = 1");
            myDatabase.execSQL("delete from sqlite_master where type = 'table'");
            myDatabase.execSQL("PRAGMA writable_schema = 0");

            myDatabase.setTransactionSuccessful();
            myDatabase.endTransaction();

            myDatabase.execSQL("VACUUM");
            myDatabase.setVersion(0);

            return false;
        } finally {
            if (myDatabase.inTransaction()) {
                myDatabase.endTransaction();
            }
        }
           /* }
        }, context);*/
        return true;
    }

    @Override
    public synchronized ZLFile loadZLFileByBookId(long bookId) {
        FileInfoSet infos = FileInfoSet.getInstance();

        final Cursor cursor = myDatabase.rawQuery(
                "SELECT file_id FROM Books WHERE book_id = " + bookId, null);
        try {
            if (cursor.moveToNext()) {
                long id = cursor.getLong(0);
                return infos.getFile(id);
            }
        } finally {
            cursor.close();
        }

        return null;
    }

    @Override
    public synchronized Book loadBook(long bookId) {
        Book book = null;
        final Cursor cursor = myDatabase.rawQuery(
                "SELECT file_id,title,encoding,language,added_time,isInArchive FROM Books WHERE book_id = "
                        + bookId, null);
        try {
            if (cursor.moveToNext()) {
                book = createBook(bookId, cursor.getLong(0), cursor.getString(1),
                        cursor.getString(2), cursor.getString(3),
                        SQLiteUtil.getDate(cursor, 4), cursor.getInt(5));
            }
        } finally {
            cursor.close();
        }

        return book;
    }

    @Override
    protected synchronized void reloadBook(Book book) {
        final Cursor cursor = myDatabase.rawQuery(
                "SELECT title,encoding,language FROM Books WHERE book_id = "
                        + book.getId(), null);
        try {
            if (cursor.moveToNext()) {
                book.setTitle(cursor.getString(0));
                book.setEncoding(cursor.getString(1));
                book.setLanguage(cursor.getString(2));
            }
        } finally {
            cursor.close();
        }

    }

    protected synchronized Book loadBookByFile(long fileId, ZLFile file) {
        if (fileId == -1) {
            return null;
        }
        Book book = null;
        final Cursor cursor = myDatabase.rawQuery(
                "SELECT book_id,title,encoding,language,added_time,isInArchive FROM Books WHERE file_id = "
                        + fileId, null);
        try {
            if (cursor.moveToNext()) {
                book = createBook(cursor.getLong(0), file, cursor.getString(1),
                        cursor.getString(2), cursor.getString(3),
                        SQLiteUtil.getDate(cursor, 4), cursor.getInt(5));
            }
        } catch (Exception ignored) {
        } finally {
            cursor.close();
        }
        return book;
    }

    private boolean myTagCacheIsInitialized;
    private final HashMap<Tag, Long> myIdByTag = new HashMap<Tag, Long>();
    private final HashMap<Long, Tag> myTagById = new HashMap<Long, Tag>();

    private synchronized void initTagCache() {
        if (myTagCacheIsInitialized) {
            return;
        }
        myTagCacheIsInitialized = true;

        Cursor cursor = myDatabase.rawQuery(
                "SELECT tag_id,parent_id,name FROM Tags ORDER BY tag_id", null);
        try {
            while (cursor.moveToNext()) {
                long id = cursor.getLong(0);
                if (myTagById.get(id) == null) {
                    final Tag tag = Tag.getTag(myTagById.get(cursor.getLong(1)),
                            cursor.getString(2));
                    myIdByTag.put(tag, id);
                    myTagById.put(id, tag);
                }
            }
        } finally {
            cursor.close();
        }

        cursor.close();
    }

    @Override
    public synchronized Map<Long, Book> loadBooks(FileInfoSet infos, List<Long> ids, boolean existing) {
        final HashMap<Long, Book> booksById = new HashMap<Long, Book>();
        final HashMap<Long, Book> booksByFileId = new HashMap<Long, Book>();
        final HashMap<Long, Author> authorById = new HashMap<Long, Author>();
        final HashMap<Long, String> seriesById = new HashMap<Long, String>();

        if (ids == null || ids.isEmpty()) {
            return booksByFileId;
        }

        StringBuilder questions = new StringBuilder("");
        String[] numbers = new String[ids.size()];
        questions.append("?");
        numbers[0] = ids.get(0).toString();
        for (int i = 1; i < ids.size(); i++) {
            questions.append(",?");
            numbers[i] = ids.get(i).toString();
        }

        Cursor cursor = null;
        //################################
        try {
            cursor = myDatabase.query("Books", new String[]{"book_id", "file_id", "title,encoding", "language", "added_time", "isInArchive"},
                    "`exists` = ? and book_id in (" + questions + ")",
                    Utils.concat(new String[]{String.valueOf((existing ? 1 : 0))}, numbers), null, null, null);
            while (cursor.moveToNext()) {
                final long id = cursor.getLong(0);
                final long fileId = cursor.getLong(1);

                final Book book = createBook(id, infos.getFile(fileId),
                        cursor.getString(2), cursor.getString(3),
                        cursor.getString(4), SQLiteUtil.getDate(cursor, 5), cursor.getInt(6));
                if (book != null) {
                    booksById.put(id, book);
                    booksByFileId.put(fileId, book);
                } else {
                    DebugLog.e("loadBooks", "cannot create book from existing book in DB, book_id: " + id + ", file_id: " + fileId);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        initTagCache();

        //################################
        try {
            cursor = myDatabase.query("Authors", new String[]{"author_id", "name", "sort_key"}, null, null, null, null, null);
            while (cursor.moveToNext()) {
                authorById.put(cursor.getLong(0), new Author(cursor.getString(1), cursor.getString(2)));
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        //################################
        try {
            cursor = myDatabase.query("BookAuthor", new String[]{"book_id", "author_id"}, "book_id in (" + questions + ")",
                    numbers, null, null, null);
            while (cursor.moveToNext()) {
                Book book = booksById.get(cursor.getLong(0));
                if (book != null) {
                    Author author = authorById.get(cursor.getLong(1));
                    if (author != null) {
                        addAuthor(book, author);
                    }
                }
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        //################################
        try {
            cursor = myDatabase.query("BookTag", new String[]{"book_id", "tag_id"}, "book_id in (" + questions + ")",
                    numbers, null, null, null);
            while (cursor.moveToNext()) {
                Book book = booksById.get(cursor.getLong(0));
                if (book != null) {
                    addTag(book, getTagById(cursor.getLong(1)));
                }
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        //################################
        try {
            cursor = myDatabase.query("Series", new String[]{"series_id", "name"}, null, null, null, null, null);
            while (cursor.moveToNext()) {
                seriesById.put(cursor.getLong(0), cursor.getString(1));
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        //################################
        try {
            cursor = myDatabase.query("BookSeries", new String[]{"book_id", "series_id", "book_index"}, "book_id in (" + questions + ")",
                    numbers, null, null, null);
            while (cursor.moveToNext()) {
                Book book = booksById.get(cursor.getLong(0));
                if (book != null) {
                    String series = seriesById.get(cursor.getLong(1));
                    if (series != null) {
                        setSeriesInfo(book, series, cursor.getFloat(2));
                    }
                }
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        //################################

        return booksByFileId;
    }

    private interface createBooksFromCursor {
        LinkedList<Book> createBooks(String beginingPath, Cursor cursor, final BookLoadedListener callback);
    }

    @Override
    public synchronized Map<Long, Book> loadBooks(final FileInfoSet infos, String beginingPath, List<Long> recent, boolean existing, final BookLoadedListener callback) {
        Cursor cursor = null;

        final HashMap<Long, Book> booksById = new HashMap<>();
        final HashMap<Long, Book> booksByFileId = new HashMap<>();

        final HashMap<Long, Author> authorById = new HashMap<>();
        final HashMap<Long, LinkedList<Long>> authorByBookId = new HashMap<>();
        final HashMap<Long, LinkedList<Long>> tagByBookId = new HashMap<>();
        final HashMap<Long, String> seriesById = new HashMap<>();
        final HashMap<Long, LinkedList<Pair<Long, Float>>> seriesByBookId = new HashMap<>();

        try {
            //####################################################
            cursor = myDatabase.rawQuery("SELECT author_id,name,sort_key FROM Authors", null);
            while (cursor.moveToNext()) {
                authorById.put(cursor.getLong(0), new Author(cursor.getString(1), cursor.getString(2)));
            }
            cursor.close();
            //####################################################
            cursor = myDatabase.rawQuery("SELECT book_id,author_id FROM BookAuthor", null);
            while (cursor.moveToNext()) {
                long book_id = cursor.getLong(0);
                long author_id = cursor.getLong(1);
                if (!authorByBookId.containsKey(book_id)) {
                    LinkedList<Long> list = new LinkedList<>();
                    list.add(author_id);
                    authorByBookId.put(book_id, list);
                } else {
                    authorByBookId.get(book_id).add(author_id);
                }
            }
            cursor.close();
            //####################################################
            cursor = myDatabase.rawQuery("SELECT parent_id, tag_id, name FROM Tags order by parent_id, tag_id", null);
            try {
                while (cursor.moveToNext()) {
                    final Tag parent = cursor.isNull(0) ? null : myTagById.get(cursor.getLong(0));
                    Tag tag = Tag.getTag(parent, cursor.getString(2));
                    myTagById.put(cursor.getLong(1), tag);
                }
            } finally {
                cursor.close();
            }
            for (Map.Entry<Long, Tag> entry : myTagById.entrySet()) {
                myIdByTag.put(entry.getValue(), entry.getKey());
            }

            /*cursor = myDatabase.rawQuery("SELECT parent_id, tag_id, name FROM Tags order by parent_id, tag_id", null);
            try {
                while (cursor.moveToNext()) {
                    Tag tag = myTagById.get(cursor.getLong(1));
                    if (cursor.isNull(0) && tag.Parent != null){
                        DebugLog.e("TAG", "ERROR parent is not null, tag_id = " + cursor.getLong(1));
                    }

                    if (!cursor.isNull(0) && tag.Parent == null){
                        DebugLog.e("TAG", "ERROR parent is null, tag_id = " + cursor.getLong(1));
                    }
                }
            } finally {
                cursor.close();
            }*/

            //####################################################
            cursor = myDatabase.rawQuery("SELECT book_id,tag_id FROM BookTag", null);
            while (cursor.moveToNext()) {
                long book_id = cursor.getLong(0);
                long tag_id = cursor.getLong(1);
                if (!tagByBookId.containsKey(book_id)) {
                    LinkedList<Long> list = new LinkedList<>();
                    list.add(tag_id);
                    tagByBookId.put(book_id, list);
                } else {
                    tagByBookId.get(book_id).add(tag_id);
                }
            }
            cursor.close();
            //####################################################
            cursor = myDatabase.rawQuery("SELECT series_id,name FROM Series", null);
            while (cursor.moveToNext()) {
                seriesById.put(cursor.getLong(0), cursor.getString(1));
            }
            cursor.close();

            //####################################################
            cursor = myDatabase.rawQuery("SELECT book_id, series_id, book_index FROM BookSeries", null);
            while (cursor.moveToNext()) {
                long book_id = cursor.getLong(0);
                long series_id = cursor.getLong(1);
                float book_index = cursor.getFloat(2);

                if (!seriesByBookId.containsKey(book_id)) {
                    LinkedList<Pair<Long, Float>> list = new LinkedList<>();
                    list.add(new Pair<>(series_id, book_index));
                    seriesByBookId.put(book_id, list);
                } else {
                    seriesByBookId.get(book_id).add(new Pair<>(series_id, book_index));
                }
            }
            cursor.close();
            //####################################################


            long base_file_id = infos.getId(ZLFile.createFileByPath(CollectionsManager.getInstance().getDefaultCollectionPath()));
            createBooksFromCursor processor = new createBooksFromCursor() {
                @Override
                public synchronized LinkedList<Book> createBooks(String beginingPath, Cursor cursor, BookLoadedListener callback) {
                    LinkedList<Book> books = new LinkedList<>();

                    while (cursor.moveToNext()) {
                        final long id = cursor.getLong(0);
                        final long fileId = cursor.getLong(1);

                        if (infos.getFile(fileId) == null) {
                            deleteAllBookData(id);
                            continue;
                        }
                        if (!infos.getFile(fileId).getPath().startsWith(beginingPath)) {
                            continue;
                        }

                        final Book book = createBook(id, infos.getFile(fileId),
                                cursor.getString(2), cursor.getString(3),
                                cursor.getString(4), SQLiteUtil.getDate(cursor, 5), cursor.getInt(6));
                        if (book != null) {
                            books.add(book);
                            booksById.put(id, book);
                            booksByFileId.put(fileId, book);

                            // ######################################
                            if (authorByBookId.containsKey(id)) {
                                for (Long author_id : authorByBookId.get(id)) {
                                    Author author = authorById.get(author_id);
                                    if (author != null) {
                                        addAuthor(book, author);
                                    }
                                }
                            }
                            // ######################################
                            if (tagByBookId.containsKey(id)) {
                                for (Long tag_id : tagByBookId.get(id)) {
                                    Tag tag = myTagById.get(tag_id);
                                    if (tag != null) {
                                        addTag(book, tag);
                                    }
                                }
                            }
                            // ######################################
                            if (seriesByBookId.containsKey(id)) {
                                for (Pair<Long, Float> pair : seriesByBookId.get(id)) {
                                    String series = seriesById.get(pair.first);
                                    if (series != null) {
                                        setSeriesInfo(book, series, pair.second);
                                    }
                                }
                            }
                            // ######################################
                            if (callback != null) {
                                callback.bookLoaded(book);
                            }
                        } else {
                            //DebugLog.e("loadBooks", "cannot create book from existing book in DB, book_id: " + id + ", file_id: " + fileId);
                        }
                    }
                    return books;
                }
            };

            StringBuilder questions = new StringBuilder("");
            String[] numbers = new String[recent.size()];
            // recent books
            if (!recent.isEmpty()) {
                questions.append("?");
                numbers[0] = recent.get(0).toString();
                for (int i = 1; i < recent.size(); i++) {
                    questions.append(",?");
                    numbers[i] = recent.get(i).toString();
                }

                DebugLog.e("loadBooks", "recentIds: " + Arrays.toString(numbers));

                cursor = myDatabase
                        .rawQuery(
                                "SELECT book_id,file_id,title,encoding,language,added_time,isInArchive FROM Books WHERE `exists` = "
                                        + (existing ? 1 : 0) + " and book_id in (" + questions + ")", numbers);
                processor.createBooks("", cursor, null);
                LinkedList<Book> _books = new LinkedList<>();
                for (Long id : recent) {
                    Book book = booksById.get(id);
                    //DebugLog.e("loadBooks", "add to recent: " + book.getTitle());
                    if (book != null) {
                        _books.add(book);
                    }
                }
                CollectionsManager.getInstance().getRecentCollection().replaceBooks(_books);
                cursor.close();
            }
            // from books folder
            /*if (base_file_id != -1) {
                if (!recent.isEmpty()) {
                    cursor = myDatabase
                            .rawQuery(
                                    "SELECT book_id,file_id,title,encoding,language,added_time,isInArchive FROM Books WHERE `exists` = "
                                            + (existing ? 1 : 0) +
                                            " and file_id in (select file_id from Files where parent_id = " + base_file_id + ")"
                                            + " and book_id not in (" + questions + ")", numbers);
                } else {
                    cursor = myDatabase
                            .rawQuery(
                                    "SELECT book_id,file_id,title,encoding,language,added_time,isInArchive FROM Books WHERE `exists` = "
                                            + (existing ? 1 : 0) +
                                            " and file_id in (select file_id from Files where parent_id = " + base_file_id + ")", null);
                }
                processor.createBooks(beginingPath, cursor, callback);
                cursor.close();
            }

            // all others
            if (!recent.isEmpty()) {
                cursor = myDatabase
                        .rawQuery(
                                "SELECT book_id,file_id,title,encoding,language,added_time,isInArchive FROM Books WHERE `exists` = "
                                        + (existing ? 1 : 0) +
                                        " and file_id not in (select file_id from Files where parent_id = " + base_file_id + ")"
                                        + " and book_id not in (" + questions + ")", numbers);
            } else {
                cursor = myDatabase
                        .rawQuery(
                                "SELECT book_id,file_id,title,encoding,language,added_time,isInArchive FROM Books WHERE `exists` = "
                                        + (existing ? 1 : 0) +
                                        " and file_id not in (select file_id from Files where parent_id = " + base_file_id + ")", null);
            }
            processor.createBooks(beginingPath, cursor, callback);
            cursor.close();*/

            //initTagCache();


        } catch (SQLiteException e) {
            org.geometerplus.android.AdobeSDKWrapper.DebugLog.e("DB ERROR", e.getLocalizedMessage());

            if (cursor != null && !cursor.isClosed()) {
                try {
                    cursor.close();
                } catch (Exception exception) {
                }
            }

            return new HashMap<Long, Book>();
        }

        return booksByFileId;
    }

    @Override
    public synchronized void setExistingFlag(Collection<Book> books, boolean flag) {
        if (books.isEmpty()) {
            return;
        }
        final StringBuilder bookSet = new StringBuilder("(");
        boolean first = true;
        for (Book b : books) {
            if (first) {
                first = false;
            } else {
                bookSet.append(",");
            }
            bookSet.append(b.getId());
        }
        bookSet.append(")");
        myDatabase.execSQL("UPDATE Books SET `exists` = " + (flag ? 1 : 0)
                + " WHERE book_id IN " + bookSet);
    }

    private SQLiteStatement myUpdateBookInfoStatement;

    @Override
    protected synchronized void updateBookInfo(long bookId, long fileId, String encoding,
                                               String language, String title, boolean isInArchive) {

        // TODO: check?
        //myDatabase.delete("Books", "file_id = ? and book_id != ?", new String[]{String.valueOf(fileId), String.valueOf(bookId)});

        if (myUpdateBookInfoStatement == null) {
            myUpdateBookInfoStatement = myDatabase
                    .compileStatement("UPDATE Books SET file_id = ?, encoding = ?, language = ?, title = ?, isInArchive = ? WHERE book_id = ?");
        }

        if (myUpdateBookInfoStatement != null) {
            myUpdateBookInfoStatement.bindLong(1, fileId);
            SQLiteUtil.bindString(myUpdateBookInfoStatement, 2, encoding);
            SQLiteUtil.bindString(myUpdateBookInfoStatement, 3, language);
            myUpdateBookInfoStatement.bindString(4, title);
            myUpdateBookInfoStatement.bindLong(5, isInArchive ? 1 : 0);
            myUpdateBookInfoStatement.bindLong(6, bookId);

            try {
                myUpdateBookInfoStatement.execute();
            } catch (SQLiteConstraintException e) {
                //e.printStackTrace();
                myDatabase.delete("Books", "file_id = ? and book_id != ?", new String[]{String.valueOf(fileId), String.valueOf(bookId)});
                myUpdateBookInfoStatement.execute();
            }

            if (!isInArchive) {
                DebugLog.w("updateBookInfo", "isInArchive = false, fileId = " + fileId);
            }
        }
    }

    private SQLiteStatement myInsertBookInfoStatement;

    @Override
    protected synchronized long insertBookInfo(ZLFile file, String encoding,
                                               String language, String title, Date addedTime, boolean mBookIsInArchive) {
        if (myInsertBookInfoStatement == null) {
            myInsertBookInfoStatement = myDatabase
                    .compileStatement("INSERT OR IGNORE INTO Books (encoding,language,title,file_id,added_time,isInArchive)" +
                            " VALUES (?,?,?,?,?,?)");
        }
        SQLiteUtil.bindString(myInsertBookInfoStatement, 1, encoding);
        SQLiteUtil.bindString(myInsertBookInfoStatement, 2, language);
        myInsertBookInfoStatement.bindString(3, title);
        final FileInfoSet infoSet = FileInfoSet.getInstance();
        myInsertBookInfoStatement.bindLong(4, infoSet.getId(file));
        SQLiteUtil.bindDate(myInsertBookInfoStatement, 5, addedTime);
        myInsertBookInfoStatement.bindLong(6, mBookIsInArchive ? 1 : 0);

        long lastInsertedRowID = myInsertBookInfoStatement.executeInsert();

        if (!mBookIsInArchive) {
            DebugLog.w("updateBookInfo", "isInArchive = false, file = " + file.getPath());
        }

        if (lastInsertedRowID == -1) {
            Collection<FileInfo> infos = loadFileInfos(file);

            if (infos != null && infos.size() > 0) {
                Iterator<FileInfo> iterator = infos.iterator();

                FileInfo info = iterator.next();
                while (info.hasChildren()) {
                    if (info.subTrees() != null) {
                        FileInfo _info = info.subTrees().iterator().next();
                        if (_info != null) {
                            info = _info;
                        } else {
                            break;
                        }
                    }
                }
                Book book = loadBookByFile(info.Id, file);
                if (book != null) {
                    return book.getId();
                }
            }
        }

        infoSet.save();

        DebugLog.d("SQL", "insertBookInfo " + lastInsertedRowID + " / " + file.getPath());

        return lastInsertedRowID;
    }

    private SQLiteStatement myDeleteBookAuthorsStatement;

    protected synchronized void deleteAllBookAuthors(long bookId) {
        try {
            if (myDeleteBookAuthorsStatement == null) {
                myDeleteBookAuthorsStatement = myDatabase
                        .compileStatement("DELETE FROM BookAuthor WHERE book_id = ?");
            }
            if (myDeleteBookAuthorsStatement != null) {
                myDeleteBookAuthorsStatement.bindLong(1, bookId);
                myDeleteBookAuthorsStatement.execute();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private SQLiteStatement myGetAuthorIdStatement;
    private SQLiteStatement myInsertAuthorStatement;
    private SQLiteStatement myInsertBookAuthorStatement;

    protected synchronized void saveBookAuthorInfo(long bookId, long index, Author author) {
        if (myGetAuthorIdStatement == null) {
            myGetAuthorIdStatement = myDatabase
                    .compileStatement("SELECT author_id FROM Authors WHERE name = ? AND sort_key = ?");
            myInsertAuthorStatement = myDatabase
                    .compileStatement("INSERT OR IGNORE INTO Authors (name,sort_key) VALUES (?,?)");
            myInsertBookAuthorStatement = myDatabase
                    .compileStatement("INSERT OR REPLACE INTO BookAuthor (book_id,author_id,author_index) VALUES (?,?,?)");
        }

        long authorId;
        try {
            myGetAuthorIdStatement.bindString(1, author.DisplayName);
            myGetAuthorIdStatement.bindString(2, author.SortKey);
            authorId = myGetAuthorIdStatement.simpleQueryForLong();
        } catch (SQLException e) {
            myInsertAuthorStatement.bindString(1, author.DisplayName);
            myInsertAuthorStatement.bindString(2, author.SortKey);
            authorId = myInsertAuthorStatement.executeInsert();
        }
        myInsertBookAuthorStatement.bindLong(1, bookId);
        myInsertBookAuthorStatement.bindLong(2, authorId);
        myInsertBookAuthorStatement.bindLong(3, index);
        myInsertBookAuthorStatement.execute();
    }

    protected synchronized List<Author> loadAuthors(long bookId) {
        final Cursor cursor = myDatabase
                .rawQuery(
                        "SELECT Authors.name,Authors.sort_key FROM BookAuthor INNER JOIN Authors ON Authors.author_id = BookAuthor.author_id WHERE BookAuthor.book_id = ?",
                        new String[]{"" + bookId});
        final ArrayList<Author> list = new ArrayList<Author>();
        try {
            if (!cursor.moveToNext()) {
                cursor.close();
                return null;
            }
            do {
                list.add(new Author(cursor.getString(0), cursor.getString(1)));
            } while (cursor.moveToNext());
        } finally {
            cursor.close();
        }
        return list;
    }

    public synchronized String getSyncTime(long bookId) {
        final Cursor cursor = myDatabase.rawQuery("SELECT syncTime FROM Synchronize WHERE book_id = ?", new String[]{"" + bookId});
        try {
            if (!cursor.moveToNext()) {
                cursor.close();
                return null;
            }
            String time = cursor.getString(cursor.getColumnIndex("syncTime"));
            return time;
        } finally {
            cursor.close();
        }
    }

//    public synchronized String getSyncModifyDate(long bookId) {
//        final Cursor cursor = myDatabase.rawQuery("SELECT modifyDate FROM Synchronize WHERE book_id = ?", new String[]{"" + bookId});
//        if (!cursor.moveToNext()) {
//            cursor.close();
//            return null;
//        }
//        return cursor.getString(cursor.getColumnIndex("modifyDate"));
//    }

    private SQLiteStatement mUpdateSyncTimeStatement;

    public synchronized void updateSyncTime(String version, long bookId) {
        if (mUpdateSyncTimeStatement == null) {
            mUpdateSyncTimeStatement = myDatabase
                    .compileStatement("UPDATE Synchronize SET syncTime = ? WHERE book_id = ?");
        }
        mUpdateSyncTimeStatement.bindString(1, version);
        mUpdateSyncTimeStatement.bindLong(2, bookId);
        mUpdateSyncTimeStatement.execute();

    }
//
//    private synchronized SQLiteStatement mUpdateSyncModifyStatement;
//
//    public synchronized void updateSyncModifyDate(String date, long bookId) {
//        if (mUpdateSyncModifyStatement == null) {
//            mUpdateSyncModifyStatement = myDatabase
//                    .compileStatement("UPDATE Synchronize SET modifyDate = ? WHERE book_id = ?");
//        }
//        mUpdateSyncModifyStatement.bindString(0, date);
//        mUpdatesyncTimeStatement.bindLong(1, bookId);
//        mUpdateSyncModifyStatement.execute();
//    }

    private SQLiteStatement myGetTagIdStatement;
    private SQLiteStatement myCreateTagIdStatement;

    private synchronized long getTagId(Tag tag) {
        if (myGetTagIdStatement == null) {
            myGetTagIdStatement = myDatabase
                    .compileStatement("SELECT tag_id FROM Tags WHERE parent_id = ? AND name = ?");
            myCreateTagIdStatement = myDatabase
                    .compileStatement("INSERT OR IGNORE INTO Tags (parent_id,name) VALUES (?,?)");
        }
        {
            final Long id = myIdByTag.get(tag);
            if (id != null) {
                return id;
            }
        }
        if (tag.Parent != null) {
            myGetTagIdStatement.bindLong(1, getTagId(tag.Parent));
        } else {
            myGetTagIdStatement.bindNull(1);
        }
        myGetTagIdStatement.bindString(2, tag.Name);
        long id;
        try {
            id = myGetTagIdStatement.simpleQueryForLong();
        } catch (SQLException e) {
            if (tag.Parent != null) {
                myCreateTagIdStatement.bindLong(1, getTagId(tag.Parent));
            } else {
                myCreateTagIdStatement.bindNull(1);
            }
            myCreateTagIdStatement.bindString(2, tag.Name);
            id = myCreateTagIdStatement.executeInsert();
        }
        myIdByTag.put(tag, id);
        myTagById.put(id, tag);
        return id;
    }

    private SQLiteStatement myDeleteBookTagsStatement;

    protected synchronized void deleteAllBookTags(long bookId) {
        try {
            if (myDeleteBookTagsStatement == null) {
                myDeleteBookTagsStatement = myDatabase
                        .compileStatement("DELETE FROM BookTag WHERE book_id = ?");
            }
            if (myDeleteBookTagsStatement != null) {
                myDeleteBookTagsStatement.bindLong(1, bookId);
                myDeleteBookTagsStatement.execute();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private SQLiteStatement myInsertBookTagStatement;

    protected synchronized void saveBookTagInfo(long bookId, Tag tag) {
        if (myInsertBookTagStatement == null) {
            myInsertBookTagStatement = myDatabase
                    .compileStatement("INSERT OR IGNORE INTO BookTag (book_id,tag_id) VALUES (?,?)");
        }
        if (myInsertBookTagStatement != null) {
            myInsertBookTagStatement.bindLong(1, bookId);
            myInsertBookTagStatement.bindLong(2, getTagId(tag));
            myInsertBookTagStatement.execute();
        }
    }

    private synchronized Tag getTagById(long id) {
        Tag tag = myTagById.get(id);
        if (tag == null) {
            final Cursor cursor = myDatabase.rawQuery(
                    "SELECT parent_id,name FROM Tags WHERE tag_id = ?",
                    new String[]{"" + id});
            try {
                if (cursor.moveToNext()) {
                    final Tag parent = cursor.isNull(0) ? null : getTagById(cursor
                            .getLong(0));
                    tag = Tag.getTag(parent, cursor.getString(1));
                    myIdByTag.put(tag, id);
                    myTagById.put(id, tag);
                }
            } finally {
                cursor.close();
            }
        }
        return tag;
    }

    protected synchronized List<Tag> loadTags(long bookId) {
        final Cursor cursor = myDatabase
                .rawQuery(
                        "SELECT Tags.tag_id FROM BookTag INNER JOIN Tags ON Tags.tag_id = BookTag.tag_id WHERE BookTag.book_id = ?",
                        new String[]{"" + bookId});
        ArrayList<Tag> list = new ArrayList<Tag>();
        try {
            if (!cursor.moveToNext()) {
                return null;
            }
            do {
                list.add(getTagById(cursor.getLong(0)));
            } while (cursor.moveToNext());
        } finally {
            cursor.close();
        }
        return list;
    }

    private SQLiteStatement myGetSeriesIdStatement;
    private SQLiteStatement myInsertSeriesStatement;
    private SQLiteStatement myInsertBookSeriesStatement;
    private SQLiteStatement myDeleteBookSeriesStatement;

    public synchronized boolean deleteBook(long bookId) {
        if (myDeleteBookSeriesStatement == null) {
            myDeleteBookSeriesStatement = myDatabase
                    .compileStatement("DELETE FROM Books WHERE book_id = ?");
        }
        DebugLog.w("SQL", "deleteBook " + bookId);

        try {
            myDeleteBookSeriesStatement.bindLong(1, bookId);
            myDeleteBookSeriesStatement.execute();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public synchronized void deleteAllBookData(final long bookId) {
        BooksDatabase.Instance().executeAsATransaction(new Runnable() {
            @Override
            public synchronized void run() {
                deleteBook(bookId);
                deleteAllBookAuthors(bookId);
                deleteAllBookTags(bookId);
                deleteFromBookList(bookId);
                removeFromFavorites(bookId);
                removeFromSynchronizable(bookId);
            }
        });
    }

    protected synchronized void saveBookSeriesInfo(long bookId, SeriesInfo seriesInfo) {
        if (myGetSeriesIdStatement == null) {
            myGetSeriesIdStatement = myDatabase
                    .compileStatement("SELECT series_id FROM Series WHERE name = ?");
            myInsertSeriesStatement = myDatabase
                    .compileStatement("INSERT OR IGNORE INTO Series (name) VALUES (?)");
            myInsertBookSeriesStatement = myDatabase
                    .compileStatement("INSERT OR REPLACE INTO BookSeries (book_id,series_id,book_index) VALUES (?,?,?)");
            myDeleteBookSeriesStatement = myDatabase
                    .compileStatement("DELETE FROM BookSeries WHERE book_id = ?");
        }

        if (seriesInfo == null) {
            /*if (myDeleteBookSeriesStatement != null) {
                myDeleteBookSeriesStatement.bindLong(1, bookId);
                myDeleteBookSeriesStatement.execute();
            }*/
        } else {
            long seriesId;
            try {
                myGetSeriesIdStatement.bindString(1, seriesInfo.Name);
                seriesId = myGetSeriesIdStatement.simpleQueryForLong();
            } catch (SQLException e) {
                myInsertSeriesStatement.bindString(1, seriesInfo.Name);
                seriesId = myInsertSeriesStatement.executeInsert();
            }
            myInsertBookSeriesStatement.bindLong(1, bookId);
            myInsertBookSeriesStatement.bindLong(2, seriesId);
            if (seriesInfo.Index != null) {
                myInsertBookSeriesStatement.bindDouble(3, seriesInfo.Index.doubleValue());
            } else {
                myInsertBookSeriesStatement.bindDouble(3, 0);
            }

            myInsertBookSeriesStatement.execute();
        }
    }

    protected synchronized SeriesInfo loadSeriesInfo(long bookId) {
        final Cursor cursor = myDatabase
                .rawQuery(
                        "SELECT Series.name,BookSeries.book_index FROM BookSeries INNER JOIN Series ON Series.series_id = BookSeries.series_id WHERE BookSeries.book_id = ?",
                        new String[]{"" + bookId});
        SeriesInfo info = null;
        if (cursor.moveToNext()) {
            info = new SeriesInfo(cursor.getString(0), new BigDecimal(cursor.getFloat(1)));
        }
        cursor.close();
        return info;
    }

    private SQLiteStatement myRemoveFileInfoStatement;

    protected synchronized void removeFileInfo(long fileId) {
        try {
            if (fileId == -1) {
                return;
            }
            if (myRemoveFileInfoStatement == null) {
                myRemoveFileInfoStatement = myDatabase
                        .compileStatement("DELETE FROM Files WHERE file_id = ?");
            }
            if (myRemoveFileInfoStatement != null) {
                myRemoveFileInfoStatement.bindLong(1, fileId);
                myRemoveFileInfoStatement.execute();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private SQLiteStatement myInsertFileInfoStatement;
    private SQLiteStatement myUpdateFileInfoStatement;

    protected synchronized void saveFileInfo(FileInfo fileInfo) {
        final long id = fileInfo.Id;
        SQLiteStatement statement;
        if (id == -1) {
            if (myInsertFileInfoStatement == null) {
                myInsertFileInfoStatement = myDatabase
                        .compileStatement("INSERT OR IGNORE INTO Files (name,parent_id,size) VALUES (?,?,?)");
            }
            statement = myInsertFileInfoStatement;
        } else {
            if (myUpdateFileInfoStatement == null) {
                myUpdateFileInfoStatement = myDatabase
                        .compileStatement("UPDATE Files SET name = ?, parent_id = ?, size = ? WHERE file_id = ?");
            }
            statement = myUpdateFileInfoStatement;
        }
        if (statement != null) {
            statement.bindString(1, fileInfo.Name);
            final FileInfo parent = fileInfo.Parent;
            if (parent != null) {
                statement.bindLong(2, parent.Id);
            } else {
                statement.bindNull(2);
            }
            final long size = fileInfo.FileSize;
            if (size != -1) {
                statement.bindLong(3, size);
            } else {
                statement.bindNull(3);
            }
            if (id == -1) {
                fileInfo.Id = statement.executeInsert();
            } else {
                statement.bindLong(4, id);
                statement.execute();
            }
        }

    }

    protected synchronized Collection<FileInfo> loadFileInfos() {
        HashMap<Long, FileInfo> infosById = new HashMap<Long, FileInfo>();

        Cursor cursor = null;
        try {
            cursor = myDatabase.rawQuery(
                    "SELECT file_id,name,parent_id,size FROM Files", null);

            while (cursor.moveToNext()) {
                final long id = cursor.getLong(0);
                final FileInfo info = createFileInfo(
                        id,
                        cursor.getString(1),
                        cursor.isNull(2) ? null : infosById.get(cursor
                                .getLong(2)));
                if (!cursor.isNull(3)) {
                    info.FileSize = cursor.getLong(3);
                }
                infosById.put(id, info);
            }

        } catch (SQLiteException e) {
            if (DebugLog.mLoggingEnabled) {
                e.printStackTrace();
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return infosById.values();
    }

    protected synchronized Collection<FileInfo> loadFileInfos(ZLFile file) {
        final LinkedList<ZLFile> fileStack = new LinkedList<ZLFile>();
        for (; file != null; file = file.getParent()) {
            fileStack.addFirst(file);
        }

        final ArrayList<FileInfo> infos = new ArrayList<FileInfo>(
                fileStack.size());

        // *wtf
        try {

            final String[] parameters = {null};
            FileInfo current = null;
            for (ZLFile f : fileStack) {
                parameters[0] = f.getLongName();
                final Cursor cursor = myDatabase
                        .rawQuery(
                                (current == null) ? "SELECT file_id,size FROM Files WHERE name = ?"
                                        : "SELECT file_id,size FROM Files WHERE parent_id = "
                                        + current.Id + " AND name = ?",
                                parameters);
                if (cursor.moveToNext()) {
                    current = createFileInfo(cursor.getLong(0), parameters[0],
                            current);
                    if (!cursor.isNull(1)) {
                        current.FileSize = cursor.getLong(1);
                    }
                    infos.add(current);
                    cursor.close();
                } else {
                    cursor.close();
                    break;
                }
            }

        } catch (Exception e) {
            DebugLog.e(e.getClass().toString(), e.getLocalizedMessage());
            // TODO: handle exception
        }
        return infos;
    }

    protected synchronized Collection<FileInfo> loadFileInfos(long fileId) {
        final ArrayList<FileInfo> infos = new ArrayList<FileInfo>();
        while (fileId != -1) {
            final Cursor cursor = myDatabase.rawQuery(
                    "SELECT name,size,parent_id FROM Files WHERE file_id = "
                            + fileId, null);
            if (cursor.moveToNext()) {
                FileInfo info = createFileInfo(fileId, cursor.getString(0),
                        null);
                if (!cursor.isNull(1)) {
                    info.FileSize = cursor.getLong(1);
                }
                infos.add(0, info);
                fileId = cursor.isNull(2) ? -1 : cursor.getLong(2);
            } else {
                fileId = -1;
            }
            cursor.close();
        }
        for (int i = 1; i < infos.size(); ++i) {
            final FileInfo oldInfo = infos.get(i);
            final FileInfo newInfo = createFileInfo(oldInfo.Id, oldInfo.Name,
                    infos.get(i - 1));
            newInfo.FileSize = oldInfo.FileSize;
            infos.set(i, newInfo);
        }
        return infos;
    }

    private SQLiteStatement mySaveRecentBookStatement;

    public synchronized void saveRecentBookIds(final List<Long> ids) {
        if (mySaveRecentBookStatement == null) {
            mySaveRecentBookStatement = myDatabase.compileStatement("INSERT OR IGNORE INTO RecentBooks (book_id) VALUES (?)");
        }
        executeAsATransaction(new Runnable() {
            public synchronized void run() {
                String _ids = "";
                myDatabase.delete("RecentBooks", null, null);
                for (long id : ids) {
                    _ids += id + ",";
                    mySaveRecentBookStatement.bindLong(1, id);
                    mySaveRecentBookStatement.execute();
                }
                DebugLog.w("SQL", "saveRecentBookIds: " + _ids);
                //DebugLog.stackTrace();
            }
        });
    }

    public synchronized List<Long> loadRecentBookIds() {
        final LinkedList<Long> ids = new LinkedList<Long>();

        Cursor cursor = null;
        try {
            cursor = myDatabase
                    .rawQuery(
                            "SELECT book_id FROM RecentBooks ORDER BY book_index",
                            null);

            while (cursor.moveToNext()) {
                ids.add(cursor.getLong(0));
            }
        } catch (SQLiteException e) {
            // TODO: handle exception

            DebugLog.e("SQL Error", e.getLocalizedMessage());

        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return ids;
    }

    private SQLiteStatement myAddToFavoritesStatement;

    public synchronized void addToFavorites(long bookId) {
        if (myAddToFavoritesStatement == null) {
            myAddToFavoritesStatement = myDatabase
                    .compileStatement("INSERT OR IGNORE INTO Favorites(book_id, added_time) VALUES (?, ?)");
        }
        if (myAddToFavoritesStatement != null) {
            myAddToFavoritesStatement.bindLong(1, bookId);
            myAddToFavoritesStatement.bindLong(2, System.currentTimeMillis());
            myAddToFavoritesStatement.execute();
        }

    }

    private SQLiteStatement myRemoveFromFavoritesStatement;

    public synchronized void removeFromFavorites(long bookId) {
        try {
            if (myRemoveFromFavoritesStatement == null) {
                myRemoveFromFavoritesStatement = myDatabase
                        .compileStatement("DELETE FROM Favorites WHERE book_id = ?");
            }
            if (myRemoveFromFavoritesStatement != null) {
                myRemoveFromFavoritesStatement.bindLong(1, bookId);
                myRemoveFromFavoritesStatement.execute();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized LinkedList<Long> loadFavoritesIds() {
        final Cursor cursor = myDatabase.rawQuery(
                "SELECT book_id FROM Favorites order by added_time desc", null);
        final LinkedList<Long> ids = new LinkedList<Long>();
        while (cursor.moveToNext()) {
            ids.add(cursor.getLong(0));
        }
        cursor.close();
        return ids;
    }

    private SQLiteStatement myAddToSynchronizableStatement;

    public synchronized void addToSynchronizable(long bookId, String time) {
        if (myAddToSynchronizableStatement == null) {
            myAddToSynchronizableStatement = myDatabase
                    .compileStatement("INSERT OR IGNORE INTO Synchronize(book_id, syncTime) VALUES (?,?)");
        }
        if (myAddToSynchronizableStatement != null) {
            myAddToSynchronizableStatement.bindLong(1, bookId);
            myAddToSynchronizableStatement.bindString(2, time);
            myAddToSynchronizableStatement.execute();
        }

    }

    private SQLiteStatement myRemoveFromSynchronizableStatement;

    public synchronized void removeFromSynchronizable(long bookId) {
        try {
            if (myRemoveFromSynchronizableStatement == null) {
                myRemoveFromSynchronizableStatement = myDatabase
                        .compileStatement("DELETE FROM Synchronize WHERE book_id = ?");
            }
            if (myRemoveFromSynchronizableStatement != null) {
                myRemoveFromSynchronizableStatement.bindLong(1, bookId);
                myRemoveFromSynchronizableStatement.execute();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static final String TAG = SQLiteBooksDatabase.class.getSimpleName();

    public synchronized List<Long> loadSynchronizableIds() {
        final Cursor cursor = myDatabase.rawQuery(
                "SELECT book_id FROM Synchronize", null);
        final LinkedList<Long> ids = new LinkedList<Long>();
        while (cursor.moveToNext()) {
            ids.add(cursor.getLong(0));
        }
        cursor.close();
        return ids;
    }

    @Override
    protected synchronized List<Bookmark> loadBookmarks(long bookId, boolean visible) {
        LinkedList<Bookmark> list = new LinkedList<Bookmark>();
        Cursor cursor = //myDatabase.query("Bookmarks", null, "book_id = " + bookId, null, null, null, null);
                myDatabase.rawQuery(
                        "SELECT Bookmarks.bookmark_id,Bookmarks.book_id,Books.title,Bookmarks.bookmark_text,Bookmarks.creation_time,Bookmarks.modification_time,Bookmarks.access_time,Bookmarks.access_counter,Bookmarks.model_id,Bookmarks.paragraph,Bookmarks.word,Bookmarks.char FROM Bookmarks INNER JOIN Books ON Books.book_id = Bookmarks.book_id WHERE Bookmarks.book_id = ? AND Bookmarks.visible = ?",
                        new String[]{"" + bookId, visible ? "1" : "0"});
        while (cursor.moveToNext()) {
            list.add(createBookmark(cursor.getLong(0), cursor.getLong(1),
                    cursor.getString(2), cursor.getString(3),
                    SQLiteUtil.getDate(cursor, 4),
                    SQLiteUtil.getDate(cursor, 5),
                    SQLiteUtil.getDate(cursor, 6), (int) cursor.getLong(7),
                    cursor.getString(8), (int) cursor.getLong(9),
                    (int) cursor.getLong(10), (int) cursor.getLong(11), visible));
        }
        cursor.close();
        return list;
    }

    @Override
    public synchronized List<DjVuBookmark> loadBookmarks(long bookId) {
        LinkedList<DjVuBookmark> list = new LinkedList<DjVuBookmark>();
        Cursor cursor = myDatabase.rawQuery(
                "SELECT Bookmarks.bookmark_id,Bookmarks.book_id,Books.title,Bookmarks.bookmark_text,Bookmarks.creation_time,Bookmarks.modification_time,Bookmarks.access_time,Bookmarks.access_counter,Bookmarks.model_id,Bookmarks.paragraph,Bookmarks.word,Bookmarks.char FROM Bookmarks INNER JOIN Books ON Books.book_id = Bookmarks.book_id WHERE Bookmarks.book_id = ?",
                new String[]{"" + bookId});
        while (cursor.moveToNext()) {
            list.add(new DjVuBookmark(cursor.getLong(0), cursor.getLong(1), cursor.getInt(9), cursor.getString(3), SQLiteUtil.getDate(cursor, 4)));
        }
        cursor.close();

        return list;
    }

    @Override
    protected synchronized List<Bookmark> loadAllVisibleBookmarks() {
        LinkedList<Bookmark> list = new LinkedList<Bookmark>();
        myDatabase.execSQL("DELETE FROM Bookmarks WHERE book_id = -1");
        Cursor cursor = myDatabase
                .rawQuery(
                        "SELECT Bookmarks.bookmark_id,Bookmarks.book_id,Books.title,Bookmarks.bookmark_text,Bookmarks.creation_time,Bookmarks.modification_time,Bookmarks.access_time,Bookmarks.access_counter,Bookmarks.model_id,Bookmarks.paragraph,Bookmarks.word,Bookmarks.char FROM Bookmarks INNER JOIN Books ON Books.book_id = Bookmarks.book_id WHERE Bookmarks.visible = 1",
                        null);
        while (cursor.moveToNext()) {
            list.add(createBookmark(cursor.getLong(0), cursor.getLong(1),
                    cursor.getString(2), cursor.getString(3),
                    SQLiteUtil.getDate(cursor, 4),
                    SQLiteUtil.getDate(cursor, 5),
                    SQLiteUtil.getDate(cursor, 6), (int) cursor.getLong(7),
                    cursor.getString(8), (int) cursor.getLong(9),
                    (int) cursor.getLong(10), (int) cursor.getLong(11), true));
        }
        cursor.close();
        return list;
    }


    private SQLiteStatement myInsertBookmarkStatement;
    private SQLiteStatement myUpdateBookmarkStatement;

    @Override
    protected synchronized long saveBookmark(Bookmark bookmark) {
        SQLiteStatement statement;
        if (bookmark.getId() == -1) {
            if (myInsertBookmarkStatement == null) {
                myInsertBookmarkStatement = myDatabase
                        .compileStatement("INSERT OR IGNORE INTO Bookmarks (book_id,bookmark_text,creation_time,modification_time,access_time,access_counter,model_id,paragraph,word,char,visible) VALUES (?,?,?,?,?,?,?,?,?,?,?)");
            }
            statement = myInsertBookmarkStatement;
        } else {
            if (myUpdateBookmarkStatement == null) {
                myUpdateBookmarkStatement = myDatabase
                        .compileStatement("UPDATE Bookmarks SET book_id = ?, bookmark_text = ?, creation_time =?, modification_time = ?,access_time = ?, access_counter = ?, model_id = ?, paragraph = ?, word = ?, char = ?, visible = ? WHERE bookmark_id = ?");
            }
            statement = myUpdateBookmarkStatement;
        }

        if (statement != null) {
            statement.bindLong(1, bookmark.getBookId());
            statement.bindString(2, bookmark.getText());
            SQLiteUtil.bindDate(statement, 3, bookmark.getTime(Bookmark.CREATION));
            SQLiteUtil.bindDate(statement, 4,
                    bookmark.getTime(Bookmark.MODIFICATION));
            SQLiteUtil.bindDate(statement, 5, bookmark.getTime(Bookmark.ACCESS));
            statement.bindLong(6, bookmark.getAccessCount());
            SQLiteUtil.bindString(statement, 7, bookmark.ModelId);
            statement.bindLong(8, bookmark.ParagraphIndex);
            statement.bindLong(9, bookmark.ElementIndex);
            statement.bindLong(10, bookmark.CharIndex);
            statement.bindLong(11, bookmark.IsVisible ? 1 : 0);

            if (statement == myInsertBookmarkStatement) {
                return statement.executeInsert();
            } else {
                final long id = bookmark.getId();
                statement.bindLong(12, id);
                statement.execute();
                return id;
            }
        }

        return -1;
    }

    private SQLiteStatement myDeleteBookmarkStatement;

    @Override
    protected synchronized void deleteBookmark(Bookmark bookmark) {
        if (myDeleteBookmarkStatement == null) {
            myDeleteBookmarkStatement = myDatabase
                    .compileStatement("DELETE FROM Bookmarks WHERE bookmark_id = ?");
        }
        myDeleteBookmarkStatement.bindLong(1, bookmark.getId());
        myDeleteBookmarkStatement.execute();
    }

    @Override
    public synchronized long saveBookmark(DjVuBookmark djVuBookmark) {
        SQLiteStatement statement;
        if (djVuBookmark.getId() == -1) {
            if (myInsertBookmarkStatement == null) {
                myInsertBookmarkStatement = myDatabase
                        .compileStatement("INSERT OR IGNORE INTO Bookmarks (book_id,bookmark_text,creation_time,modification_time,access_time,access_counter,model_id,paragraph,word,char,visible) VALUES (?,?,?,?,?,?,?,?,?,?,?)");
            }
            statement = myInsertBookmarkStatement;
        } else {
            if (myUpdateBookmarkStatement == null) {
                myUpdateBookmarkStatement = myDatabase
                        .compileStatement("UPDATE Bookmarks SET book_id = ?, bookmark_text = ?, creation_time =?, modification_time = ?,access_time = ?, access_counter = ?, model_id = ?, paragraph = ?, word = ?, char = ?, visible = ? WHERE bookmark_id = ?");
            }
            statement = myUpdateBookmarkStatement;
        }

        if (statement != null) {
            statement.bindLong(1, djVuBookmark.getBookId());
            statement.bindString(2, djVuBookmark.getText());
            SQLiteUtil.bindDate(statement, 3, djVuBookmark.getTime(Bookmark.CREATION));
            SQLiteUtil.bindDate(statement, 4, djVuBookmark.getTime(Bookmark.MODIFICATION));
            SQLiteUtil.bindDate(statement, 5, djVuBookmark.getTime(Bookmark.ACCESS));
            statement.bindLong(6, 1);
            SQLiteUtil.bindString(statement, 7, "");
            statement.bindLong(8, djVuBookmark.getPageNumber());
            statement.bindLong(9, 0);
            statement.bindLong(10, 0);
            statement.bindLong(11, 1);

            if (statement == myInsertBookmarkStatement) {
                return statement.executeInsert();
            } else {
                final long id = djVuBookmark.getId();
                statement.bindLong(6, id);
                statement.execute();
                return id;
            }
        }

        return -1;
    }

    @Override
    public synchronized void deleteBookmark(DjVuBookmark djVuBookmark) {
        if (myDeleteBookmarkStatement == null) {
            myDeleteBookmarkStatement = myDatabase
                    .compileStatement("DELETE FROM Bookmarks WHERE bookmark_id = ?");
        }
        myDeleteBookmarkStatement.bindLong(1, djVuBookmark.getId());
        myDeleteBookmarkStatement.execute();
    }

    protected synchronized ZLTextPosition getStoredPosition(long bookId) {
        ZLTextPosition position = null;
        Cursor cursor = myDatabase.rawQuery(
                "SELECT paragraph,word,char FROM BookState WHERE book_id = "
                        + bookId, null);
        if (cursor.moveToNext()) {
            position = new ZLTextFixedPosition((int) cursor.getLong(0),
                    (int) cursor.getLong(1), (int) cursor.getLong(2));
        }
        cursor.close();

        if (position != null) {
            org.geometerplus.android.AdobeSDKWrapper.DebugLog.d("BooksDatabase", "restore >>> bookId: " + bookId + ", " + "paragraph: " + position.getParagraphIndex() +
                    ", element: " + position.getElementIndex() + ", char: " + position.getCharIndex());
        } else {
            org.geometerplus.android.AdobeSDKWrapper.DebugLog.d("BooksDatabase", "restore >>> bookId: " + bookId + " NULL data");
        }

        return position;
    }

    private SQLiteStatement myStorePositionStatement;

    protected synchronized void storePosition(long bookId, ZLTextPosition position) {
        if (myStorePositionStatement == null) {
            myStorePositionStatement = myDatabase
                    .compileStatement("INSERT OR REPLACE INTO BookState (book_id,paragraph,word,char) VALUES (?,?,?,?)");
        }
        if (myStorePositionStatement != null) {
            myStorePositionStatement.bindLong(1, bookId);
            myStorePositionStatement.bindLong(2, position.getParagraphIndex());
            myStorePositionStatement.bindLong(3, position.getElementIndex());
            myStorePositionStatement.bindLong(4, position.getCharIndex());
            try {
                myStorePositionStatement.execute();
            } catch (SQLiteException ignored) {
            }
        }

        org.geometerplus.android.AdobeSDKWrapper.DebugLog.d("BooksDatabase", "store >>> bookId: " + bookId + ", " + "paragraph: " + position.getParagraphIndex() +
                ", element: " + position.getElementIndex() + ", char: " + position.getCharIndex());
    }

    private SQLiteStatement myInsertIntoBookListStatement;

    protected synchronized boolean insertIntoBookList(long bookId) {
        if (myInsertIntoBookListStatement == null) {
            myInsertIntoBookListStatement = myDatabase
                    .compileStatement("INSERT OR IGNORE INTO BookList(book_id) VALUES (?)");
        }
        if (myInsertIntoBookListStatement != null) {
            myInsertIntoBookListStatement.bindLong(1, bookId);
            myInsertIntoBookListStatement.execute();
        }

        return true;
    }

    private SQLiteStatement myDeleteFromBookListStatement;

    public synchronized boolean deleteFromBookList(long bookId) {
        try {
            if (myDeleteFromBookListStatement == null) {
                myDeleteFromBookListStatement = myDatabase
                        .compileStatement("DELETE FROM BookList WHERE book_id = ?");
            }
            if (myDeleteFromBookListStatement != null) {
                myDeleteFromBookListStatement.bindLong(1, bookId);
                myDeleteFromBookListStatement.execute();
            }

            deleteVisitedHyperlinks(bookId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    private SQLiteStatement myCheckBookListStatement;

    protected synchronized boolean checkBookList(long bookId) {
        if (myCheckBookListStatement == null) {
            myCheckBookListStatement = myDatabase
                    .compileStatement("SELECT COUNT(*) FROM BookList WHERE book_id = ?");
        }
        if (myCheckBookListStatement != null) {
            myCheckBookListStatement.bindLong(1, bookId);
            return myCheckBookListStatement.simpleQueryForLong() > 0;
        }
        return false;
    }

    private SQLiteStatement myDeleteVisitedHyperlinksStatement;

    private synchronized void deleteVisitedHyperlinks(long bookId) {
        if (myDeleteVisitedHyperlinksStatement == null) {
            myDeleteVisitedHyperlinksStatement = myDatabase
                    .compileStatement("DELETE FROM VisitedHyperlinks WHERE book_id = ?");
        }

        if (myDeleteVisitedHyperlinksStatement != null) {
            myDeleteVisitedHyperlinksStatement.bindLong(1, bookId);
            myDeleteVisitedHyperlinksStatement.execute();
        }

    }

    private SQLiteStatement myStoreVisitedHyperlinksStatement;

    protected synchronized void addVisitedHyperlink(long bookId, String hyperlinkId) {
        if (myStoreVisitedHyperlinksStatement == null) {
            myStoreVisitedHyperlinksStatement = myDatabase
                    .compileStatement("INSERT OR IGNORE INTO VisitedHyperlinks(book_id,hyperlink_id) VALUES (?,?)");
        }

        if (myStoreVisitedHyperlinksStatement != null) {
            myStoreVisitedHyperlinksStatement.bindLong(1, bookId);
            myStoreVisitedHyperlinksStatement.bindString(2, hyperlinkId);
            myStoreVisitedHyperlinksStatement.execute();
        }

    }

    protected synchronized Collection<String> loadVisitedHyperlinks(long bookId) {
        final TreeSet<String> links = new TreeSet<String>();
        final Cursor cursor = myDatabase.rawQuery(
                "SELECT hyperlink_id FROM VisitedHyperlinks WHERE book_id = ?",
                new String[]{"" + bookId});
        while (cursor.moveToNext()) {
            links.add(cursor.getString(0));
        }
        cursor.close();
        return links;
    }

    private synchronized void createTables() {

        // *wtf
        try {

            myDatabase.execSQL("CREATE TABLE Books("
                    + "book_id INTEGER PRIMARY KEY," + "encoding TEXT,"
                    + "language TEXT," + "title TEXT NOT NULL,"
                    + "file_name TEXT UNIQUE NOT NULL)");
        } catch (Exception e) {

            DebugLog.e(e.getClass().toString(), e.getLocalizedMessage()
                    + "Books");
            // TODO: handle exception
        }
        try {
            myDatabase.execSQL("CREATE TABLE Authors("
                    + "author_id INTEGER PRIMARY KEY," + "name TEXT NOT NULL,"
                    + "sort_key TEXT NOT NULL,"
                    + "CONSTRAINT Authors_Unique UNIQUE (name, sort_key))");
        } catch (Exception e) {

            DebugLog.e(e.getClass().toString(), e.getLocalizedMessage()
                    + "Authours");

            // TODO: handle exception
        }
        try {
            myDatabase
                    .execSQL("CREATE TABLE BookAuthor("
                            + "author_id INTEGER NOT NULL REFERENCES Authors(author_id),"
                            + "book_id INTEGER NOT NULL REFERENCES Books(book_id),"
                            + "author_index INTEGER NOT NULL,"
                            + "CONSTRAINT BookAuthor_Unique0 UNIQUE (author_id, book_id),"
                            + "CONSTRAINT BookAuthor_Unique1 UNIQUE (book_id, author_index))");
        } catch (Exception e) {

            DebugLog.e(e.getClass().toString(), e.getLocalizedMessage()
                    + "BookAuthor");

            // TODO: handle exception
        }
        try {
            myDatabase.execSQL("CREATE TABLE Series("
                    + "series_id INTEGER PRIMARY KEY,"
                    + "name TEXT UNIQUE NOT NULL)");
        } catch (Exception e) {

            DebugLog.e(e.getClass().toString(), e.getLocalizedMessage()
                    + "Series");

            // TODO: handle exception
        }
        try {
            myDatabase
                    .execSQL("CREATE TABLE BookSeries("
                            + "series_id INTEGER NOT NULL REFERENCES Series(series_id),"
                            + "book_id INTEGER NOT NULL UNIQUE REFERENCES Books(book_id),"
                            + "book_index INTEGER)");
        } catch (Exception e) {

            DebugLog.e(e.getClass().toString(), e.getLocalizedMessage()
                    + "BookSeries");

            // TODO: handle exception
        }
        try {
            myDatabase.execSQL("CREATE TABLE Tags("
                    + "tag_id INTEGER PRIMARY KEY," + "name TEXT NOT NULL,"
                    + "parent INTEGER REFERENCES Tags(tag_id),"
                    + "CONSTRAINT Tags_Unique UNIQUE (name, parent))");
        } catch (Exception e) {

            DebugLog.e(e.getClass().toString(), e.getLocalizedMessage()
                    + "Tags");

            // TODO: handle exception
        }
        try {
            myDatabase.execSQL("CREATE TABLE BookTag("
                    + "tag_id INTEGER REFERENCES Tags(tag_id),"
                    + "book_id INTEGER REFERENCES Books(book_id),"
                    + "CONSTRAINT BookTag_Unique UNIQUE (tag_id, book_id))");

        } catch (Exception e) {
            DebugLog.e(e.getClass().toString(), e.getLocalizedMessage() + "BookTag");
            // TODO: handle exception
        }
    }

    private static AtomicBoolean mTest = new AtomicBoolean(false);

    private synchronized void updateTables1() {

        String alterTable = "ALTER TABLE Tags RENAME TO Tags_Obsolete";
        String createTable = "CREATE TABLE Tags(" + "tag_id INTEGER PRIMARY KEY,"
                + "name TEXT NOT NULL,"
                + "parent_id INTEGER REFERENCES Tags(tag_id),"
                + "CONSTRAINT Tags_Unique UNIQUE (name, parent_id))";

        /*boolean containsName = false;
        try{
            Cursor ti = myDatabase.rawQuery("PRAGMA table_info(Tags)", null);

            if ( ti.moveToFirst() ) {
                do {
                    if (ti.getString(1).equals("name")){
                        containsName = true;
                    }
                } while (ti.moveToNext());
            }
        } catch (Exception e){
        }

        if (containsName){
            try {
                myDatabase.execSQL(createTable);
            } catch (Exception ignored){}
        } else {*/
        myDatabase.execSQL(alterTable);
        myDatabase.execSQL(createTable);

        /*if (!mTest.get()){
            myDatabase.execSQL("DROP TABLE Tags_Obsolete");
            myDatabase.execSQL(alterTable);
            myDatabase.execSQL(createTable);
            mTest.set(true);
        }*/

        //}

        //for(int i = 0; i < 10; i++){
        myDatabase.execSQL("INSERT INTO Tags (tag_id,name,parent_id) SELECT tag_id,name,parent FROM Tags_Obsolete");
        //}

        myDatabase.execSQL("DROP TABLE Tags_Obsolete");

        myDatabase.execSQL("ALTER TABLE BookTag RENAME TO BookTag_Obsolete");
        myDatabase.execSQL("CREATE TABLE BookTag("
                + "tag_id INTEGER NOT NULL REFERENCES Tags(tag_id),"
                + "book_id INTEGER NOT NULL REFERENCES Books(book_id),"
                + "CONSTRAINT BookTag_Unique UNIQUE (tag_id, book_id))");
        myDatabase
                .execSQL("INSERT INTO BookTag (tag_id,book_id) SELECT tag_id,book_id FROM BookTag_Obsolete");
        myDatabase.execSQL("DROP TABLE BookTag_Obsolete");


    }

    private synchronized void updateTables2() {
        myDatabase
                .execSQL("CREATE INDEX BookAuthor_BookIndex ON BookAuthor (book_id)");
        myDatabase
                .execSQL("CREATE INDEX BookTag_BookIndex ON BookTag (book_id)");
        myDatabase
                .execSQL("CREATE INDEX BookSeries_BookIndex ON BookSeries (book_id)");
    }

    private synchronized void updateTables3() {
        myDatabase.execSQL("CREATE TABLE Files("
                + "file_id INTEGER PRIMARY KEY," + "name TEXT NOT NULL,"
                + "parent_id INTEGER REFERENCES Files(file_id),"
                + "size INTEGER,"
                + "CONSTRAINT Files_Unique UNIQUE (name, parent_id))");
    }

    private synchronized void updateTables4() {
        final FileInfoSet fileInfos = FileInfoSet.getInstanceNonBlocking(this);
        final Cursor cursor = myDatabase.rawQuery(
                "SELECT file_name FROM Books", null);
        while (cursor.moveToNext()) {
            fileInfos.check(ZLFile.createFileByPath(cursor.getString(0))
                    .getPhysicalFile(), false);
        }
        cursor.close();
        fileInfos.save();

        myDatabase.execSQL("CREATE TABLE RecentBooks("
                + "book_index INTEGER PRIMARY KEY,"
                + "book_id INTEGER REFERENCES Books(book_id))");
        final ArrayList<Long> ids = new ArrayList<Long>();

        final SQLiteStatement statement = myDatabase
                .compileStatement("SELECT book_id FROM Books WHERE file_name = ?");

        for (int i = 0; i < 20; ++i) {
            final ZLStringOption option = new ZLStringOption("LastOpenedBooks",
                    "FB2Parser" + i, "");
            final String fileName = option.getValue();
            option.setValue("");
            try {
                if (statement != null) {
                    statement.bindString(1, fileName);
                    final long bookId = statement.simpleQueryForLong();
                    if (bookId != -1) {
                        ids.add(bookId);
                    }
                }

            } catch (SQLException e) {
            }
        }
        saveRecentBookIds(ids);
    }

    private synchronized void updateTables5() {
        myDatabase.execSQL("CREATE TABLE Bookmarks("
                + "bookmark_id INTEGER PRIMARY KEY,"
                + "book_id INTEGER NOT NULL REFERENCES Books(book_id),"
                + "bookmark_text TEXT NOT NULL,"
                + "creation_time INTEGER NOT NULL,"
                + "modification_time INTEGER," + "access_time INTEGER,"
                + "access_counter INTEGER NOT NULL,"
                + "paragraph INTEGER NOT NULL," + "word INTEGER NOT NULL,"
                + "char INTEGER NOT NULL)");

        myDatabase.execSQL("CREATE TABLE BookState("
                + "book_id INTEGER UNIQUE NOT NULL REFERENCES Books(book_id),"
                + "paragraph INTEGER NOT NULL," + "word INTEGER NOT NULL,"
                + "char INTEGER NOT NULL)");
        Cursor cursor = myDatabase.rawQuery(
                "SELECT book_id,file_name FROM Books", null);
        final SQLiteStatement statement = myDatabase
                .compileStatement("INSERT INTO BookState (book_id,paragraph,word,char) VALUES (?,?,?,?)");
        while (cursor.moveToNext()) {
            final long bookId = cursor.getLong(0);
            final String fileName = cursor.getString(1);
            final int position = new ZLIntegerOption(fileName,
                    "PositionInBuffer", 0).getValue();
            final int paragraph = new ZLIntegerOption(fileName, "Paragraph_"
                    + position, 0).getValue();
            final int word = new ZLIntegerOption(fileName, "Word_" + position,
                    0).getValue();
            final int chr = new ZLIntegerOption(fileName, "Char_" + position, 0)
                    .getValue();
            if ((paragraph != 0) || (word != 0) || (chr != 0)) {
                if (statement != null) {
                    statement.bindLong(1, bookId);
                    statement.bindLong(2, paragraph);
                    statement.bindLong(3, word);
                    statement.bindLong(4, chr);
                    statement.execute();
                }

            }
            ZLConfig.Instance().removeGroup(fileName);
        }
        cursor.close();
    }

    private synchronized void updateTables6() {
        myDatabase.execSQL("ALTER TABLE Bookmarks ADD COLUMN model_id TEXT");

        myDatabase.execSQL("ALTER TABLE Books ADD COLUMN file_id INTEGER");

        myDatabase.execSQL("DELETE FROM Files");
        final FileInfoSet infoSet = FileInfoSet.getInstanceNonBlocking(this);
        Cursor cursor = myDatabase
                .rawQuery("SELECT file_name FROM Books", null);
        while (cursor.moveToNext()) {
            infoSet.check(ZLFile.createFileByPath(cursor.getString(0))
                    .getPhysicalFile(), false);
        }
        cursor.close();
        infoSet.save();

        cursor = myDatabase.rawQuery("SELECT book_id,file_name FROM Books",
                null);
        final SQLiteStatement deleteStatement = myDatabase
                .compileStatement("DELETE FROM Books WHERE book_id = ?");
        final SQLiteStatement updateStatement = myDatabase
                .compileStatement("UPDATE Books SET file_id = ? WHERE book_id = ?");
        while (cursor.moveToNext()) {
            final long bookId = cursor.getLong(0);
            final long fileId = infoSet.getId(ZLFile.createFileByPath(cursor
                    .getString(1)));

            if (fileId == -1) {
                if (deleteStatement != null) {
                    deleteStatement.bindLong(1, bookId);
                    deleteStatement.execute();
                }

            } else {
                if (updateStatement != null) {
                    updateStatement.bindLong(1, fileId);
                    updateStatement.bindLong(2, bookId);
                    updateStatement.execute();
                }

            }
        }
        cursor.close();

        myDatabase.execSQL("ALTER TABLE Books RENAME TO Books_Obsolete");
        myDatabase.execSQL("CREATE TABLE Books("
                + "book_id INTEGER PRIMARY KEY," + "encoding TEXT,"
                + "language TEXT," + "title TEXT NOT NULL,"
                + "file_id INTEGER UNIQUE NOT NULL REFERENCES Files(file_id))");
        myDatabase
                .execSQL("INSERT INTO Books (book_id,encoding,language,title,file_id) SELECT book_id,encoding,language,title,file_id FROM Books_Obsolete");
        myDatabase.execSQL("DROP TABLE Books_Obsolete");
    }

    private synchronized void updateTables7() {
        final ArrayList<Long> seriesIDs = new ArrayList<Long>();
        Cursor cursor = myDatabase.rawQuery(
                "SELECT series_id,name FROM Series", null);
        while (cursor.moveToNext()) {
            String str = cursor.getString(1);
            if (str != null && str.length() > 200) {
                seriesIDs.add(cursor.getLong(0));
            }
        }
        cursor.close();
        if (seriesIDs.isEmpty()) {
            return;
        }

        final ArrayList<Long> bookIDs = new ArrayList<Long>();
        for (Long id : seriesIDs) {
            cursor = myDatabase.rawQuery(
                    "SELECT book_id FROM BookSeries WHERE series_id=" + id,
                    null);
            while (cursor.moveToNext()) {
                bookIDs.add(cursor.getLong(0));
            }
            cursor.close();
            myDatabase.execSQL("DELETE FROM BookSeries WHERE series_id=" + id);
            myDatabase.execSQL("DELETE FROM Series WHERE series_id=" + id);
        }

        for (Long id : bookIDs) {
            myDatabase.execSQL("DELETE FROM Books WHERE book_id=" + id);
            myDatabase.execSQL("DELETE FROM BookAuthor WHERE book_id=" + id);
            myDatabase.execSQL("DELETE FROM BookTag WHERE book_id=" + id);
        }
    }

    private synchronized void updateTables8() {
        myDatabase
                .execSQL("CREATE TABLE IF NOT EXISTS BookList ( "
                        + "book_id INTEGER UNIQUE NOT NULL REFERENCES Books (book_id))");
    }

    private synchronized void updateTables9() {
        myDatabase
                .execSQL("CREATE INDEX BookList_BookIndex ON BookList (book_id)");
    }

    private synchronized void updateTables10() {
        myDatabase.execSQL("CREATE TABLE IF NOT EXISTS Favorites("
                + "book_id INTEGER UNIQUE NOT NULL REFERENCES Books(book_id))");
    }

    private synchronized void updateTables11() {
        myDatabase.execSQL("UPDATE Files SET size = size + 1");
    }

    private synchronized void updateTables12() {
        myDatabase
                .execSQL("DELETE FROM Files WHERE parent_id IN (SELECT file_id FROM Files WHERE name LIKE '%.epub')");
    }

    private synchronized void updateTables13() {
        myDatabase
                .execSQL("ALTER TABLE Bookmarks ADD COLUMN visible INTEGER DEFAULT 1");
    }

    private synchronized void updateTables14() {
        myDatabase
                .execSQL("ALTER TABLE BookSeries RENAME TO BookSeries_Obsolete");
        myDatabase.execSQL("CREATE TABLE BookSeries("
                + "series_id INTEGER NOT NULL REFERENCES Series(series_id),"
                + "book_id INTEGER NOT NULL UNIQUE REFERENCES Books(book_id),"
                + "book_index REAL)");
        myDatabase
                .execSQL("INSERT INTO BookSeries (series_id,book_id,book_index) SELECT series_id,book_id,book_index FROM BookSeries_Obsolete");
        myDatabase.execSQL("DROP TABLE BookSeries_Obsolete");
    }

    private synchronized void updateTables15() {
        myDatabase
                .execSQL("CREATE TABLE IF NOT EXISTS VisitedHyperlinks("
                        + "book_id INTEGER NOT NULL REFERENCES Books(book_id),"
                        + "hyperlink_id TEXT NOT NULL,"
                        + "CONSTRAINT VisitedHyperlinks_Unique UNIQUE (book_id, hyperlink_id))");
    }

    private synchronized void updateTables16() {
        myDatabase
                .execSQL("ALTER TABLE Books ADD COLUMN `exists` INTEGER DEFAULT 1");
        myDatabase
                .execSQL("ALTER TABLE Books ADD COLUMN added_time INTEGER DEFAULT "
                        + (new Date().getTime()));
    }

    private synchronized void updateTables17() {
        myDatabase
                .execSQL("CREATE TABLE IF NOT EXISTS Synchronize("
                        + "book_id INTEGER NOT NULL REFERENCES Books(book_id), syncTime TEXT NOT NULL)"); //, modifyDate TEXT NOT NULL)");
    }

//    private synchronized void updateTable19() {
//        myDatabase.execSQL("CREATE TABLE IF NOT EXISTS Marks(book_id INTEGER UNIQUE NOT NULL REFERENCES Books(book_id), " +
//                "paragraph INTEGER, offset INTEGER, length INTEGER, color INTEGER)");
//    }

    private synchronized void updateTables18() {
        try {
            myDatabase.execSQL("ALTER TABLE Books ADD COLUMN `isInArchive` INTEGER DEFAULT 0");
        } catch (Exception ignore) {
        }
        Map<Long, Book> books = this.loadBooks(FileInfoSet.getInstanceNonBlocking(this),
                CollectionsManager.getInstance().getDefaultCollectionPath(), Collections.<Long>emptyList(), true, null);
        for (Map.Entry<Long, Book> entry : books.entrySet()) {
            Book book = entry.getValue();
            if (book.File.exists() && book.File.isArchive(true)) {
                book.setBookIsInArchive(true);
                book.save(this, FileInfoSet.getInstanceNonBlocking(this));
            }
        }
        File dummyMyBooksFolder = new File(CollectionsManager.getInstance().getDefaultCollectionPath(), "My Books");
        if (dummyMyBooksFolder.exists()) {
            File[] files = dummyMyBooksFolder.listFiles();
            if (files == null || files.length == 0) {
                dummyMyBooksFolder.delete();
            }
        }
    }

    private synchronized void updateTables19() {
        myDatabase.execSQL("ALTER TABLE Favorites ADD COLUMN added_time INTEGER DEFAULT 0");
    }
}