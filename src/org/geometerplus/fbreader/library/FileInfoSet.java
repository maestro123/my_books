/*
 * Copyright (C) 2009-2011 Geometer Plus <contact@geometerplus.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

package org.geometerplus.fbreader.library;

import org.geometerplus.android.fbreader.library.SQLiteBooksDatabase;
import org.geometerplus.zlibrary.core.filesystem.ZLArchiveEntryFile;
import org.geometerplus.zlibrary.core.filesystem.ZLFile;
import org.geometerplus.zlibrary.core.filesystem.ZLPhysicalFile;
import org.geometerplus.zlibrary.core.util.ZLMiscUtil;

import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;

public final class FileInfoSet {
    private static final class Pair {
        private final String myName;
        private final FileInfo myParent;

        Pair(String name, FileInfo parent) {
            myName = name;
            myParent = parent;
        }

        @Override
        public int hashCode() {
            return (myParent == null) ? myName.hashCode() : myParent.hashCode() + myName.hashCode();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (!(o instanceof Pair)) {
                return false;
            }
            Pair p = (Pair) o;
            return (myName.equals(p.myName)) && ZLMiscUtil.equals(myParent, p.myParent);
        }
    }

    private final HashMap<ZLFile, FileInfo> myInfosByFile = new HashMap<ZLFile, FileInfo>();
    private final HashMap<FileInfo, ZLFile> myFilesByInfo = new HashMap<FileInfo, ZLFile>();

    private final HashMap<Pair, FileInfo> myInfosByPair = new HashMap<Pair, FileInfo>();
    private final HashMap<Long, FileInfo> myInfosById = new HashMap<Long, FileInfo>();

    private final LinkedHashSet<FileInfo> myInfosToSave = new LinkedHashSet<FileInfo>();
    private final LinkedHashSet<FileInfo> myInfosToRemove = new LinkedHashSet<FileInfo>();

    private static FileInfoSet mInstance = null;
    private static FileInfoSet mInstanceNonBlocking = null;
    private static BooksDatabase mDB = null;
    private static CountDownLatch initGate = new CountDownLatch(1);
    private static AtomicBoolean mInitStarted = new AtomicBoolean(false);

    public static FileInfoSet getInstance() {
        try {
            SQLiteBooksDatabase.Instance();
            synchronized (FileInfoSet.class) {
                if (!mInitStarted.get()) {
                    throw new RuntimeException("you need to call FileInfoSet.init(BooksDatabase db) first, before you can use getInstance()");
                }
            }
            initGate.await();
        } catch (InterruptedException e) {
            return null;
        }
        return mInstance;
    }

    public static synchronized FileInfoSet getInstanceNonBlocking(BooksDatabase db) {
        if (mInstanceNonBlocking == null) {
            mInstanceNonBlocking = new FileInfoSet(db);
            mDB = db;
        }
        return mInstanceNonBlocking;
    }

    public static synchronized FileInfoSet init(BooksDatabase db) {
        mInitStarted.set(true);
        if (mInstance == null) {
            mInstance = new FileInfoSet(db);
            mDB = db;
        }
        initGate.countDown();
        return mInstance;
    }

    private FileInfoSet(BooksDatabase database) {
        load(database.loadFileInfos());
    }

    private FileInfoSet(long fileId) {
        load(BooksDatabase.Instance().loadFileInfos(fileId));
    }

    private void load(Collection<FileInfo> infos) {
        for (FileInfo info : infos) {
            myInfosByPair.put(new Pair(info.Name, info.Parent), info);
            myInfosById.put(info.Id, info);
        }
    }

    public void save() {
        final BooksDatabase database = mDB == null ? BooksDatabase.Instance() : mDB;
        database.executeAsATransaction(new Runnable() {
            public void run() {
                synchronized (myInfosToRemove) {
                    for (FileInfo info : myInfosToRemove) {
                        //database.removeFileInfo(info.Id);
                        myInfosByPair.remove(new Pair(info.Name, info.Parent));
                    }
                    myInfosToRemove.clear();
                }
                synchronized (myInfosToSave) {
                    for (FileInfo info : myInfosToSave) {
                        database.saveFileInfo(info);
                    }
                    myInfosToSave.clear();
                }
            }
        });
    }

    public boolean check(ZLPhysicalFile file, boolean processChildren) {
        if (file == null) {
            return true;
        }
        final long fileSize = file.size();
        FileInfo info = get(file);
        if (info.FileSize == fileSize) {
            return true;
        } else {
            info.FileSize = fileSize;
            if (processChildren && !"epub".equals(file.getExtension())) {
                removeChildren(info);
                synchronized (myInfosToSave) {
                    myInfosToSave.add(info);
                }
                //addChildren(file);
            } else {
                synchronized (myInfosToSave) {
                    myInfosToSave.add(info);
                }
            }
            return false;
        }
    }

    public List<ZLFile> archiveEntries(ZLFile file) {
        final LinkedList<ZLFile> entries = new LinkedList<ZLFile>();
        List<ZLFile> files_inside = file.children();
        for (ZLFile file_inside : files_inside) {
            ZLArchiveEntryFile entry = ZLArchiveEntryFile.createArchiveEntryFile(file, file_inside.getLongName());
            check(entry.getPhysicalFile(), true);
            entries.add(entry);
        }
        return entries;
    }

    private FileInfo get(String name, FileInfo parent) {
        final Pair pair = new Pair(name, parent);
        FileInfo info = myInfosByPair.get(pair);
        if (info == null) {
            info = new FileInfo(name, parent);
            myInfosByPair.put(pair, info);
            synchronized (myInfosToSave) {
                myInfosToSave.add(info);
            }
        }
        return info;
    }


    private FileInfo get(ZLFile file) {
        if (file == null) {
            return null;
        }
        FileInfo info = myInfosByFile.get(file);
        if (info == null) {
            info = get(file.getLongName(), get(file.getParent()));
            myInfosByFile.put(file, info);
        }
        return info;
    }

    public long getId(ZLFile file) {
        final FileInfo info = get(file);
        if (info == null) {
            return -1;
        }
        if (info.Id == -1) {
            save();
        }
        return info.Id;
    }

    public long getIdSmart(ZLFile file) {
        FileInfo info = get(file);
        if (info == null) {
            return -1;
        }
        if (file.getPath().toLowerCase().endsWith(".zip") && info.mySubTrees != null && info.mySubTrees.size() == 1) {
            info = info.mySubTrees.get(0);
        }
        if (info.Id == -1) {
            save();
        }
        return info.Id;
    }

    private ZLFile getFile(FileInfo info) {
        if (info == null) {
            return null;
        }
        ZLFile file = myFilesByInfo.get(info);
        if (file == null) {
            file = ZLFile.createFile(getFile(info.Parent), info.Name);
            myFilesByInfo.put(info, file);
        }
        return file;
    }

    public ZLFile getFile(long id) {
        return getFile(myInfosById.get(id));
    }

    private void removeChildren(FileInfo info) {
        for (FileInfo child : info.subTrees()) {
            boolean contains;
            synchronized (myInfosToSave) {
                contains = myInfosToSave.contains(child);
            }
            if (contains) {
                synchronized (myInfosToSave) {
                    myInfosToSave.remove(child);
                }
            } else {
                synchronized (myInfosToRemove) {
                    myInfosToRemove.add(child);
                }
            }
            removeChildren(child);
        }
    }

    private void addChildren(ZLFile file) {
        for (ZLFile child : file.children()) {
            final FileInfo info = get(child);
            boolean contains;
            synchronized (myInfosToRemove) {
                contains = myInfosToRemove.contains(info);
            }
            if (contains) {
                synchronized (myInfosToRemove) {
                    myInfosToRemove.remove(info);
                }
            } else {
                synchronized (myInfosToSave) {
                    myInfosToSave.add(info);
                }
            }
            addChildren(child);
        }
    }

}
