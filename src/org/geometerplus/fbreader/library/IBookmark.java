package org.geometerplus.fbreader.library;

import org.json.JSONObject;

import java.util.Comparator;
import java.util.Date;

public interface IBookmark {

    int CREATION = 0;
    int MODIFICATION = 1;
    int ACCESS = 2;
    int LATEST = 3;

    void delete();

    void onOpen();

    Date getTime(int timeStamp);

    String getText();

    String getBookTitle();

    String getTOCTitle();

    void save();

    class ByTimeComparator implements Comparator<IBookmark> {
        public int compare(IBookmark bm0, IBookmark bm1) {
            return bm1.getTime(LATEST).compareTo(bm0.getTime(LATEST));
        }
    }
}
