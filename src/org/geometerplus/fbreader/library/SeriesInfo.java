/*
 * Copyright (C) 2009-2011 Geometer Plus <contact@geometerplus.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

package org.geometerplus.fbreader.library;

import com.prestigio.ereader.book.BooksLibrary2;

import java.math.BigDecimal;
import java.util.ArrayList;

public final class SeriesInfo {
    public static SeriesInfo createSeriesInfo(String title, String index) {
        if (title == null) {
            return null;
        }
        return new SeriesInfo(title, createIndex(index));
    }

    public static BigDecimal createIndex(String index) {
        try {
            return index != null ? new BigDecimal(index).stripTrailingZeros() : null;
        } catch (NumberFormatException e) {
            return null;
        }
    }

    public String Name;
    public BigDecimal Index;
    public ArrayList<Book> Books = new ArrayList<Book>() {
        @Override
        public boolean add(Book object) {
            if (!contains(object))
                return super.add(object);
            return false;
        }

        @Override
        public boolean remove(Object object) {
            boolean removed = super.remove(object);
            if (removed) {
                BooksLibrary2.getInstance().notifyCatalogChange(BooksLibrary2.CATALOG.SERIES,
                        (Book) object, size() == 0);
            }
            return removed;
        }
    };

    public SeriesInfo(String name, BigDecimal index) {
        Name = name;
        Index = index;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || !(o instanceof SeriesInfo)) {
            return false;
        } else {
            SeriesInfo _o = (SeriesInfo) o;
            if (_o.Name.equals(Name)) {
                return true;
            }
        }
        return false;
    }
}
