/*
 * Copyright (C) 2007-2011 Geometer Plus <contact@geometerplus.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

package org.geometerplus.fbreader.library;

import android.graphics.Color;
import android.text.TextUtils;
import com.prestigio.android.ereader.read.maestro.AudioBookRenderer;
import com.prestigio.android.ereader.read.maestro.TTSHelper;
import com.prestigio.android.ereader.utils.UID;
import com.prestigio.android.ereader.utils.Utils;
import com.prestigio.ereader.Sql.table.BooksProgress;
import com.prestigio.ereader.book.BooksCollection;
import com.prestigio.ereader.book.CollectionsManager;
import org.geometerplus.android.AdobeSDKWrapper.DebugLog;
import org.geometerplus.android.fbreader.library.SQLiteBooksDatabase;
import org.geometerplus.fbreader.Paths;
import org.geometerplus.fbreader.bookmodel.BookReadingException;
import org.geometerplus.fbreader.formats.FormatPlugin;
import org.geometerplus.fbreader.formats.PluginCollection;
import org.geometerplus.zlibrary.core.filesystem.ZLFile;
import org.geometerplus.zlibrary.core.filesystem.ZLPhysicalFile;
import org.geometerplus.zlibrary.core.image.ZLImage;
import org.geometerplus.zlibrary.core.util.ZLMiscUtil;
import org.geometerplus.zlibrary.text.view.ZLTextPosition;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class Book {

    public class RestrictedAccessToFile extends Exception {

    }

    public static final String TAG = Book.class.getSimpleName();

    public static Book getById(long bookId) {

        Book book;
        book = CollectionsManager.getInstance().findBookById(bookId);
        if (book != null) {
            return book;
        }

        book = BooksDatabase.Instance().loadBook(bookId);
        if (book == null) {
            return null;
        }
        book.loadLists();

        final ZLFile bookFile = book.File;
        final ZLPhysicalFile physicalFile = bookFile.getPhysicalFile();
        if (physicalFile == null) {
            return book;
        }
        if (!physicalFile.exists()) {
            return null;
        }

        FileInfoSet fileInfos = FileInfoSet.getInstance();
        if (fileInfos.check(physicalFile, physicalFile != bookFile)) {
            return book;
        }
        fileInfos.save();

        return book.readMetaInfo() ? book : null;
    }

    public String getEncodingNoDetection() {
        return myEncoding;
    }

    public static Book getByFile(ZLFile bookFile) {
        if (bookFile == null) {
            return null;
        }

        if (bookFile.isArchive(true)) {
            ZLFile single = bookFile.getSingleBookFile();
            if (single != null && !single.equals(bookFile)) {
                bookFile = single;
            }
        }

        if (bookFile.isArchive(true) && !bookFile.singleBook()) {
            return null;
        }

        Book book = CollectionsManager.getInstance().findBookByPath(bookFile.getPath());

        if (book != null) {
            return book;
        }

        final ZLPhysicalFile physicalFile = bookFile.getPhysicalFile();
        if (physicalFile != null && !physicalFile.exists()) {
            return null;
        }

        final FileInfoSet fileInfos = FileInfoSet.getInstance();// new

        book = BooksDatabase.Instance().loadBookByFile(fileInfos.getId(bookFile), bookFile);
        if (book != null) {
            book.loadLists();
        } else {
            book = BooksDatabase.Instance().loadBookByFile(fileInfos.getId(bookFile.getSingleBookFile()), bookFile);
            if (book != null) {
                book.loadLists();
            } else {
                book = BooksDatabase.Instance().loadBookByFile(fileInfos.getIdSmart(bookFile), bookFile);
                if (book != null) {
                    book.loadLists();
                }
            }
        }

        if (book != null && fileInfos.check(physicalFile, physicalFile != bookFile)) {
            return book;
        }
        fileInfos.save();

        if (book == null) {
            single:
            {
                if (bookFile.singleBook()) {
                    ZLFile book_file = bookFile.getSingleBookFile();
                    if (book_file != null) {
                        boolean isInArchive = false;
                        if (bookFile.getParent().isArchive()) {
                            isInArchive = !bookFile.getParent().singleBook();
                        } else {
                            isInArchive = bookFile.isArchive(true) && !bookFile.isSingleBookInArchive();
                        }
                        book = new Book(book_file, isInArchive);
                        break single;
                    }
                }
                book = new Book(bookFile, bookFile.isArchive(true) && !bookFile.isSingleBookInArchive());
            }
        }
        if (book.readMetaInfo()) {
            book.save();

            fileInfos.check(physicalFile, physicalFile != bookFile);
            fileInfos.save();

            return book;
        }
        return null;
    }

    public volatile ZLFile File;

    private volatile int mColor = -1;

    private long myId;
    private Date myAddedTime;

    private volatile String myEncoding;
    private volatile String myLanguage;
    private volatile String myTitle;
    private volatile List<Author> myAuthors;
    private volatile List<Tag> myTags;
    private List<UID> mUids = new ArrayList<UID>() {
        @Override
        public boolean add(UID object) {
            if (object != null && !contains(object))
                return super.add(object);
            else
                return false;
        }
    };
    private volatile SeriesInfo mySeriesInfo;

    private boolean myIsSaved;
    private boolean requiredRemoveFromSync;

    private boolean selected_to_collection;

    private static final int ADDED_TIME_GRANULARITY = 1000 * 60 * 10;
    private volatile int mHasCover = 0;

    private volatile int mCurrentPage = 0;
    private volatile int mPagesCount = 0;

    private volatile boolean mBookIsInArchive = false;

    public boolean isBookIsInArchive() {
        return mBookIsInArchive;
    }

    public void setBookIsInArchive(boolean mBookIsInArchive) {
        this.mBookIsInArchive = mBookIsInArchive;
    }

    public void setHasCover(boolean hasCover) {
        mHasCover = hasCover ? 1 : -1;
    }

    public int getHasCover() {
        return mHasCover;
    }

    public boolean hasCover() {
        return mHasCover == 1;
    }

    public Book(long id, ZLFile file, String title, String encoding, String language,
                Date addedTime, boolean isInArchive) {
        myId = id;
        File = file;
        myTitle = title.trim();
        myEncoding = encoding;
        myLanguage = language;
        myIsSaved = true;
        myAddedTime = addedTime;

        setCurrentPage(BooksProgress.getInstance().getCurrentPosition(getId(), true));
        setPagesCount(BooksProgress.getInstance().getPagesCount(getId(), true));
        mBookIsInArchive = isInArchive;
    }

    public static Book makeDummyBook() {
        return new Book(0, ZLFile.createFileByPath(Paths.BooksDirectoryOption().getValue()), "", "", "", new Date(0), false);
    }

    public static Book makeDummyBook(ZLFile file) {
        return new Book(file);
    }

    // WARNING! dummy book, do not use if you are not sure!
    private Book(ZLFile file) {
        myId = -1;
        File = file;
    }

    private Book(ZLFile file, boolean isInArchive) {
        myId = -1;
        File = file;
        myAddedTime = new Date();
        myAddedTime.setTime((myAddedTime.getTime() / ADDED_TIME_GRANULARITY)
                * ADDED_TIME_GRANULARITY);

        setCurrentPage(BooksProgress.getInstance().getCurrentPosition(getId(), true));
        setPagesCount(BooksProgress.getInstance().getPagesCount(getId(), true));
        mBookIsInArchive = isInArchive;

        DebugLog.e("Book", "2 create book from " + file.getPath() + ", mBookIsInArchive = " + mBookIsInArchive);
    }

    public void updateSyncTime() {
        long syncTime = Long.valueOf(getSyncTime());
        SQLiteBooksDatabase.Instance().updateSyncTime(String.valueOf(syncTime + 1), myId);
    }

    public void updateSyncTime(String value) {
        SQLiteBooksDatabase.Instance().updateSyncTime(value, myId);
    }

    public String getSyncTime() {
        String version = SQLiteBooksDatabase.Instance().getSyncTime(myId);
        return version != null && !TextUtils.isEmpty(version) ? version : "-1";
    }

    public void reloadBookProgress() {
        setCurrentPage(BooksProgress.getInstance().getCurrentPosition(getId(), false));
        setPagesCount(BooksProgress.getInstance().getPagesCount(getId(), false));
    }

    public void setCurrentPage(Integer pageNumber) {
        DebugLog.e("BookProgressTest", File.getPath() + ", setCurrentPage: " + pageNumber);
        if (pageNumber != null) {
            //DebugLog.stackTrace();
            mCurrentPage = pageNumber;
        } else {
            //DebugLog.stackTrace();
        }
    }

    public void setPagesCount(Integer pagesCount) {
        if (pagesCount != null && pagesCount > 0) {
            mPagesCount = pagesCount;
        }
    }

    public int getCurrentPage() {
        return mCurrentPage;
    }

    public int getPagesCount() {
        return mPagesCount;
    }

    public float getProgress() {
        if (mPagesCount > 0 && mCurrentPage > 0) {
            return ((float) mCurrentPage * 100) / (float) mPagesCount;
        } else {
            return 0;
        }
        //return mPagesCount > 0 && mCurrentPage > 0 ? ((float) mCurrentPage / mPagesCount * 100) : 0;
    }

    private static final int[] colors = {Color.parseColor("#ffd40000"),
            Color.parseColor("#ff61658c"), Color.parseColor("#ffeecc2f"),
            Color.parseColor("#ff1ca186")};

    private static final AtomicInteger rand = new AtomicInteger(0);

    public int getColor() {
        synchronized (rand) {
            if (mColor == -1) {
                mColor = getRandomColor();
            }
            return mColor;
        }
    }

    private final Random random = new Random();

    private int getRandomColor() {
        return colors[random.nextInt(colors.length)];
    }

    public void delete(boolean deletePhysicalFile) throws RestrictedAccessToFile {
        BooksCollection currentCollection = getBookCollection();
        if (deletePhysicalFile && !Utils.isWritableSDCardFolder2(File.getPhysicalFile().getPath())) {
            throw new RestrictedAccessToFile();
        }
        if (currentCollection != null) {
            currentCollection.removeBook(this, false);
        }
        CollectionsManager.getInstance().getRecentCollection().removeBook(this, true, true);
        CollectionsManager.getInstance().getFavoritesCollection().removeBook(this, true, true);
        CollectionsManager.getInstance().getSyncronizableCollection().removeBook(this, true, true);

        if (deletePhysicalFile) {
            BooksDatabase.Instance().deleteAllBookData(getId());
            BooksDatabase.Instance().removeFileInfo(FileInfoSet.getInstance().getId(File));
            deleteFile(File);
        }
    }

    private boolean deleteFile(ZLFile zlFile) {
        final ZLPhysicalFile file = zlFile.getPhysicalFile();
        boolean delete = false;
        if (file != null) {
            if (zlFile.singleBook()) {
                delete = file.delete();
            }
        }
        return delete;
    }

    public BooksCollection getBookCollection() {
        return CollectionsManager.getInstance().findCollectionByBook(this);
    }

    public void reloadInfoFromDatabase() {
        final BooksDatabase database = BooksDatabase.Instance();
        database.reloadBook(this);
        if (myAuthors == null) {
            myAuthors = database.loadAuthors(myId);
        }
        if (myTags == null) {
            myTags = database.loadTags(myId);
        }
        if (mySeriesInfo == null) {
            mySeriesInfo = database.loadSeriesInfo(myId);
        }
        myIsSaved = true;
    }

    private static final Object syncReadMetaInfo = new Object();

    public boolean readMetaInfo() {
        myEncoding = null;
        myLanguage = null;
        //myTitle = null;
        myAuthors = null;
        myTags = null;
        mySeriesInfo = null;

        myIsSaved = false;

        try {

            synchronized (syncReadMetaInfo) {
                final FormatPlugin plugin = PluginCollection.Instance().getPlugin(File);

                if (DebugLog.mLoggingEnabled) {

					/*
                     * System.out.println(this.File.getPath());
					 * System.out.println(this.getTitle());
					 *
					 * final StackTraceElement[] trace =
					 * Thread.currentThread().getStackTrace();
					 * for(StackTraceElement elem: trace){
					 * System.out.println(elem.toString()); }
					 */
                }
                // crash fix
                if (File.getPhysicalFile() == null || File.getPhysicalFile().getPath().contains(":")) {
                    return false;
                }
                if (Utils.isAcsmFile(File.getPath())) {
                    return false;
                }
                if (!Utils.getBookExtension(File.getPath()).endsWith("pdf") && !Utils.getBookExtension(File.getPath()).endsWith("djvu")) {
                    try {
                        if (plugin != null) {
                            plugin.readMetainfo(this);
                            return true;
                        }
                    } catch (BookReadingException e) {
                        e.printStackTrace();
                    }

                    return false;
                }
            }
            // DebugLog.wtf("readMetaInfo", "myTitle = " + myTitle);
            if (myTitle == null || myTitle.length() == 0) {
                final String fileName = File.getShortName();
                final int index = fileName.lastIndexOf('.');
                setTitle(index > 0 ? fileName.substring(0, index) : fileName);
            }
            final String demoPathPrefix = Paths.BooksDirectoryOption()
                    .getValue()
                    + java.io.File.separator
                    + "Demos"
                    + java.io.File.separator;
            if (File.getPath().startsWith(demoPathPrefix)) {
//                final String demoTag = Library.resource().getResource("demo") .getValue();
//                setTitle(getTitle() + " (" + demoTag + ")");
//                addTag(demoTag);
            }
        } catch (Exception e) {
            e.printStackTrace();
            // DebugLog.e(e.getClass().toString(), e.getMessage());
            // TODO: handle exception
        }

        // DebugLog.wtf("readMetaInfo", "myTitle == " + myTitle);

        return true;
    }

    private void loadLists() {
        final BooksDatabase database = BooksDatabase.Instance();
        if (myAuthors == null) {
            myAuthors = database.loadAuthors(myId);
        }
        if (myTags == null) {
            myTags = database.loadTags(myId);
        }
        if (mySeriesInfo == null) {
            mySeriesInfo = database.loadSeriesInfo(myId);
        }
        myIsSaved = true;
    }

    public List<Author> authors() {
        return (myAuthors != null) ? Collections.unmodifiableList(myAuthors)
                : Collections.<Author>emptyList();
    }

    void addAuthorWithNoCheck(Author author) {
        if (myAuthors == null) {
            myAuthors = new ArrayList<Author>();
        }
        myAuthors.add(author);
    }

    private void addAuthor(Author author) {
        if (author == null) {
            return;
        }
        if (myAuthors == null) {
            myAuthors = new ArrayList<Author>();
            myAuthors.add(author);
            myIsSaved = false;
        } else if (!myAuthors.contains(author)) {
            myAuthors.add(author);
            myIsSaved = false;
        }
    }

    public void addAuthor(String name) {
        addAuthor(name, "");
    }

    public void addAuthor(String name, String sortKey) {
        String strippedName = name;
        strippedName = strippedName.trim();
        if (strippedName.length() == 0) {
            return;
        }

        String strippedKey = sortKey;
        strippedKey = strippedKey.trim();
        if (strippedKey.length() == 0) {
            int index = strippedName.lastIndexOf(' ');
            if (index == -1) {
                strippedKey = strippedName;
            } else {
                strippedKey = strippedName.substring(index + 1);
                while ((index >= 0) && (strippedName.charAt(index) == ' ')) {
                    --index;
                }
                strippedName = strippedName.substring(0, index + 1) + ' '
                        + strippedKey;
            }
        }

        addAuthor(new Author(strippedName, strippedKey));
    }

    public long getId() {
        return myId;
    }

    public Date getAddedTime() {
        return new Date(myAddedTime.getTime());
    }

    public String getTitle() {
        return myTitle != null ? myTitle : this.File.getLongName().trim();
    }

    public String getNormalizeTitle() {
        String title = getTitle();
        if (title.startsWith(" "))
            title = title.substring(1, title.length());
        return normalizeDotIndex(title, 0);
    }

    private String normalizeDotIndex(String in, int offset) {
        int index = in.indexOf('.', offset);
        if (index != -1) {
            if (index < in.length() - 1) {
                if (in.charAt(index + 1) != ' ') {
                    StringBuilder builder = new StringBuilder();
                    builder.append(in.substring(0, index + 1));
                    builder.append(' ').append(in.substring(index + 1, in.length()));
                    return normalizeDotIndex(builder.toString(), index + 1);
                }
                return normalizeDotIndex(in, index + 1);
            }
        }
        return in;
    }

    public void setTitle(String title, boolean force) {
        if (force) {
            myTitle = title.trim();
            myIsSaved = false;
            ArrayList<BooksCollection> collections = CollectionsManager.getInstance().findCollectionsByBook(this);
            for (BooksCollection collection : collections) {
                collection.notifyCollectionUpdated(BooksCollection.EventStatus.BOOK_ADD);
            }
        } else {
            setTitle(title);
        }
    }

    public void setTitle(String title) {
        if (title != null) {
            if (myTitle == null /*|| !ZLMiscUtil.equals(myTitle, title)*/) {
                myTitle = title.trim();
                myIsSaved = false;
                ArrayList<BooksCollection> collections = CollectionsManager.getInstance().findCollectionsByBook(this);
                for (BooksCollection collection : collections) {
                    collection.notifyCollectionUpdated(BooksCollection.EventStatus.BOOK_ADD);
                }
            }
        }
    }

    public SeriesInfo getSeriesInfo() {
        return mySeriesInfo;
    }

    void setSeriesInfoWithNoCheck(String name, String index) {
        mySeriesInfo = SeriesInfo.createSeriesInfo(name, index);
    }

    public void setSeriesInfo(SeriesInfo info) {
        mySeriesInfo = info;
    }

    public void setSeriesInfo(String name, String index) {
        setSeriesInfo(name, SeriesInfo.createIndex(index));
    }

    public void setSeriesInfo(String name, BigDecimal index) {
        if (mySeriesInfo == null) {
            if (name != null) {
                mySeriesInfo = new SeriesInfo(name, index);
                myIsSaved = false;
            }
        } else if (name == null) {
            mySeriesInfo = null;
            myIsSaved = false;
        } else if (!name.equals(mySeriesInfo.Name) || mySeriesInfo.Index != index) {
            mySeriesInfo = new SeriesInfo(name, index);
            myIsSaved = false;
        }
    }

    public String getLanguage() {
        return myLanguage;
    }

    public void setLanguage(String language) {
        if (!ZLMiscUtil.equals(myLanguage, language)) {
            myLanguage = language;
            myIsSaved = false;
        }
    }

    public String getEncoding() {
        return myEncoding;
    }

    public void setEncoding(String encoding) {
        if (!ZLMiscUtil.equals(myEncoding, encoding)) {
            myEncoding = encoding;
            myIsSaved = false;
        }
    }

    public List<Tag> tags() {
        return (myTags != null) ? Collections.unmodifiableList(myTags)
                : Collections.<Tag>emptyList();
    }

    void addTagWithNoCheck(Tag tag) {
        if (myTags == null) {
            myTags = new ArrayList<Tag>();
        }
        myTags.add(tag);
    }

    public void addTag(Tag tag) {
        if (tag != null) {
            if (myTags == null) {
                myTags = new ArrayList<Tag>();
            }
            if (!myTags.contains(tag)) {
                myTags.add(tag);
                myIsSaved = false;
            }
        }
    }

    public void removeTag(Tag tag) {
        if (tag != null) {
            if (myTags == null) {
                return;
            }
            if (myTags.contains(tag)) {
                myTags.remove(tag);
                myIsSaved = false;
            }
        }
    }

    public void addTag(String tagName) {
        addTag(Tag.getTag(null, tagName));
    }

    public boolean matches(String pattern) {
        if (myTitle != null
                && ZLMiscUtil.matchesIgnoreCase(myTitle != null ? myTitle
                : this.File.getLongName(), pattern)) {
            return true;
        }
        if (mySeriesInfo != null
                && ZLMiscUtil.matchesIgnoreCase(mySeriesInfo.Name, pattern)) {
            return true;
        }
        if (myAuthors != null) {
            for (Author author : myAuthors) {
                if (ZLMiscUtil.matchesIgnoreCase(author.DisplayName, pattern)) {
                    return true;
                }
            }
        }
        if (myTags != null) {
            for (Tag tag : myTags) {
                if (ZLMiscUtil.matchesIgnoreCase(tag.Name, pattern)) {
                    return true;
                }
            }
        }
        return ZLMiscUtil.matchesIgnoreCase(File.getLongName(), pattern);
    }

    public boolean saveUpdatedPath() {
        final Book thisBook = this;
        final BooksDatabase database = BooksDatabase.Instance();
        database.executeAsATransaction(new Runnable() {
            public void run() {
                if (myId >= 0) {
                    final FileInfoSet fileInfos = FileInfoSet.getInstance();// new
                    database.updateBookInfo(
                            myId,
                            fileInfos.getId(File),
                            myEncoding,
                            myLanguage,
                            myTitle != null ? myTitle : thisBook.File.getLongName(),
                            mBookIsInArchive
                    );
                }
            }
        });

        myIsSaved = true;
        return true;
    }

    public boolean save(BooksDatabase _database, FileInfoSet _fileInfos) {
        try {
            final Book thisBook = this;
            final BooksDatabase database = _database == null ? BooksDatabase.Instance() : _database;
            final FileInfoSet fileInfos = _fileInfos == null ? FileInfoSet.getInstance() : _fileInfos;// new

            database.executeAsATransaction(new Runnable() {
                public void run() {
                    if (myId >= 0) {
                        database.updateBookInfo(myId, fileInfos.getId(File),
                                myEncoding, myLanguage, myTitle != null ? myTitle
                                        : thisBook.File.getLongName(), mBookIsInArchive
                        );
                    } else {
                        myId = database.insertBookInfo(File, myEncoding,
                                myLanguage, myTitle != null ? myTitle
                                        : thisBook.File.getLongName(), myAddedTime, mBookIsInArchive
                        );
                        storeAllVisitedHyperinks();
                    }

                    long index = 0;
                    database.deleteAllBookAuthors(myId);
                    for (Author author : authors()) {
                        database.saveBookAuthorInfo(myId, index++, author);
                    }
                    database.deleteAllBookTags(myId);
                    for (Tag tag : tags()) {
                        database.saveBookTagInfo(myId, tag);
                    }
                    database.saveBookSeriesInfo(myId, mySeriesInfo);
                    BooksProgress.getInstance().savePosition(getId(), mCurrentPage, mPagesCount);
                }
            });

            myIsSaved = true;
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            //ignore
        }
        return false;
    }

    public boolean save() {
        updateSyncTime();
        return save(null, null);
    }

    public ZLTextPosition getStoredPosition() {
        ZLTextPosition position = BooksDatabase.Instance().getStoredPosition(myId);
        if (position == null) {
            DebugLog.w("getStoredPosition", "no stored position: " + File.getPath());
        }
        return position;
    }

    public void storePosition(ZLTextPosition position) {
        if (myId != -1) {
            BooksDatabase.Instance().storePosition(myId, position);
        }
    }

    public void savePosition(int currentPage, int pagesCount) {
        if (pagesCount > 0) {
            updateSyncTime();
            mCurrentPage = currentPage;
            mPagesCount = pagesCount;
            BooksProgress.getInstance().savePosition(getId(), mCurrentPage, mPagesCount);
        }
    }

    private String syncLocation;

    public void setSyncLocation(String location) {
        syncLocation = location;
    }

    public String getSyncLocation() {
        return syncLocation;
    }

    private Set<String> myVisitedHyperlinks;

    private void initHyperlinkSet() {
        if (myVisitedHyperlinks == null) {
            myVisitedHyperlinks = new TreeSet<String>();
            if (myId != -1) {
                myVisitedHyperlinks.addAll(BooksDatabase.Instance()
                        .loadVisitedHyperlinks(myId));
            }
        }
    }

    public boolean isHyperlinkVisited(String linkId) {
        initHyperlinkSet();
        return myVisitedHyperlinks.contains(linkId);
    }

    public void markHyperlinkAsVisited(String linkId) {
        initHyperlinkSet();
        if (!myVisitedHyperlinks.contains(linkId)) {
            myVisitedHyperlinks.add(linkId);
            if (myId != -1) {
                BooksDatabase.Instance().addVisitedHyperlink(myId, linkId);
            }
        }
    }

    private void storeAllVisitedHyperinks() {
        if (myId != -1 && myVisitedHyperlinks != null) {
            for (String linkId : myVisitedHyperlinks) {
                BooksDatabase.Instance().addVisitedHyperlink(myId, linkId);
            }
        }
    }

    public void insertIntoBookList() {
        if (myId != -1) {
            BooksDatabase.Instance().insertIntoBookList(myId);
        }
    }

    public ZLImage getCover() {
        if (File.getPhysicalFile() != null && !File.getPhysicalFile().exists()) {
            return null;
        }
        ZLImage image = null;
        try {
            final FormatPlugin plugin = PluginCollection.Instance().getPlugin(
                    File);
            if (plugin != null) {
                image = plugin.readCover(File);
            }
            return image;
        } catch (Exception e) {
            DebugLog.e(e.getClass().toString(), e.getLocalizedMessage());
        }
        return null;
    }

    @Override
    public int hashCode() {
        return File.hashCode();
    }

    @Override
    public String toString() {
        return "FB2Parser{ID: " + myId + "; Title: " + myTitle != null ? myTitle
                : this.File.getLongName() + "; Author: " + myAuthors + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Book)) {
            return false;
        }
        return File.equals(((Book) o).File);
    }

    public String getAuthors() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < authors().size(); i++) {
            Author a = authors().get(i);
            builder.append(a.DisplayName);
            if (i != authors().size() - 1) {
                builder.append(", ");
            }
        }
        return builder.toString();
    }

    public void setRequiredRemoveFromSync(boolean requiredRemoveFromSync) {
        this.requiredRemoveFromSync = requiredRemoveFromSync;
    }

    public boolean isRequiredRemoveFromSync() {
        return requiredRemoveFromSync;
    }

    // ============== COLLECTIONS =============

    public boolean isSelectedToCollection() {
        return selected_to_collection;
    }

    public void setSelectedToCollection(boolean sel) {
        selected_to_collection = sel;
    }

    public void setFile(java.io.File f) {
        this.File = new ZLPhysicalFile(f);
    }

    public int getHash() {
        return getTitle().hashCode() + File.getExtension().hashCode() + File.getSubShortName().hashCode();
    }

    public int getHashWithOrigin() {
        return getTitle().hashCode() + File.getExtension().hashCode() + File.getSubShortName().hashCode() + hashCode();
    }

    public String getKey() {
        return String.valueOf(getHash());
    }

    public void onDelete() {
        if (myTags != null)
            for (Tag tag : myTags) {
                tag.Books.remove(this);

            }
        if (mySeriesInfo != null) {
            mySeriesInfo.Books.remove(this);
        }
        if (myAuthors != null) {
            for (Author author : myAuthors) {
                author.AuthorBooks.remove(this);
            }
        }
        if (AudioBookRenderer.getInstance().isCurrentBook(this)) {
            AudioBookRenderer.getInstance().stop();
        }else if (TTSHelper.getInstance().isCurrentBook(this)){
            TTSHelper.getInstance().release();
        }
    }

    public List<UID> getUids() {
        return mUids;
    }

    public void addUid(String type, String id) {
        mUids.add(new UID(type, id));
    }
}
