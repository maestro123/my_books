/*
 * Copyright (C) 2009-2011 Geometer Plus <contact@geometerplus.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

package org.geometerplus.fbreader.library;

import android.support.annotation.Nullable;
import com.prestigio.android.ereader.read.djvu.DjVuBookmark;
import org.geometerplus.android.fbreader.library.SQLiteBooksDatabase;
import org.geometerplus.zlibrary.core.filesystem.ZLFile;
import org.geometerplus.zlibrary.text.view.ZLTextPosition;

import java.util.*;

public abstract class BooksDatabase {
    //private static BooksDatabase ourInstance = null;

    public interface InitializedCallback{
        void run();
    }

    public static void migrate() {
        SQLiteBooksDatabase.migrate();
    }

    public static boolean initialized(InitializedCallback callback) {
        return SQLiteBooksDatabase.initialized(callback);
    }

    public static BooksDatabase Instance() {
        return SQLiteBooksDatabase.Instance();
    }

    public interface BookLoadedListener {
        void bookLoaded(Book book);
    }
    /*private BooksDatabase() {
        //ourInstance = this;
    }*/

    protected Book createBook(long id, long fileId, String title,
                              String encoding, String language, Date addedTime, int isInArchive) {
        final FileInfoSet infos = FileInfoSet.getInstance();
        return createBook(id, infos.getFile(fileId), title, encoding, language,
                addedTime, isInArchive);
    }

    protected Book createBook(long id, ZLFile file, String title,
                              String encoding, String language, Date addedTime, int isInArchive) {
        return (file != null && file.getPhysicalFile() != null && file.getPhysicalFile().exists()) ? new Book(id, file, title, encoding, language,
                addedTime, isInArchive == 1) : null;
    }

    protected void addAuthor(Book book, Author author) {
        book.addAuthorWithNoCheck(author);
    }

    protected void addTag(Book book, Tag tag) {
        book.addTagWithNoCheck(tag);
    }

    protected void setSeriesInfo(Book book, String series, float index) {
        book.setSeriesInfoWithNoCheck(series, String.valueOf(index));
    }

    public abstract void executeAsATransaction(Runnable actions);

    // returns map fileId -> book
    public abstract Map<Long, Book> loadBooks(FileInfoSet infos, List<Long> ids, boolean existing);

    public abstract Map<Long, Book> loadBooks(FileInfoSet infos, String beginingPath, List<Long> recent, boolean existing, BookLoadedListener callback);

    public abstract void setExistingFlag(Collection<Book> books, boolean flag);

    @Nullable
    public abstract ZLFile loadZLFileByBookId(long bookId);

    public abstract Book loadBook(long bookId);

    protected abstract void reloadBook(Book book);

    protected abstract Book loadBookByFile(long fileId, ZLFile file);

    protected abstract List<Author> loadAuthors(long bookId);

    protected abstract List<Tag> loadTags(long bookId);

    protected abstract SeriesInfo loadSeriesInfo(long bookId);

    protected abstract void updateBookInfo(long bookId, long fileId,
                                           String encoding, String language, String title, boolean isInArchive);

    protected abstract long insertBookInfo(ZLFile file, String encoding,
                                           String language, String title, Date addedTime, boolean mBookIsInArchive);

    protected abstract void deleteAllBookAuthors(long bookId);

    protected abstract void saveBookAuthorInfo(long bookId, long index,
                                               Author author);

    protected abstract void deleteAllBookTags(long bookId);

    protected abstract void saveBookTagInfo(long bookId, Tag tag);

    public abstract boolean deleteBook(long bookId);

    public abstract void deleteAllBookData(long bookId);

    protected abstract void saveBookSeriesInfo(long bookId,
                                               SeriesInfo seriesInfo);

    protected FileInfo createFileInfo(long id, String name, FileInfo parent) {
        return new FileInfo(name, parent, id);
    }

    protected abstract Collection<FileInfo> loadFileInfos();

    protected abstract Collection<FileInfo> loadFileInfos(ZLFile file);

    protected abstract Collection<FileInfo> loadFileInfos(long fileId);

    protected abstract void removeFileInfo(long fileId);

    protected abstract void saveFileInfo(FileInfo fileInfo);

    public abstract List<Long> loadRecentBookIds();

    public abstract void saveRecentBookIds(final List<Long> ids);

    public abstract LinkedList<Long> loadFavoritesIds();

    public abstract void addToFavorites(long bookId);

    public abstract void removeFromFavorites(long bookId);

    public abstract List<Long> loadSynchronizableIds();

    public abstract void addToSynchronizable(long bookId, String time);

    public abstract void removeFromSynchronizable(long bookId);

    protected Bookmark createBookmark(long id, long bookId, String bookTitle,
                                      String text, Date creationDate, Date modificationDate,
                                      Date accessDate, int accessCounter, String modelId,
                                      int paragraphIndex, int wordIndex, int charIndex, boolean isVisible) {
        return new Bookmark(id, bookId, bookTitle, text, creationDate,
                modificationDate, accessDate, accessCounter, modelId,
                paragraphIndex, wordIndex, charIndex, isVisible);
    }

    protected abstract List<Bookmark> loadBookmarks(long bookId, boolean isVisible);

    public abstract List<DjVuBookmark> loadBookmarks(long bookId);

    protected abstract List<Bookmark> loadAllVisibleBookmarks();

    protected abstract long saveBookmark(Bookmark bookmark);

    protected abstract void deleteBookmark(Bookmark bookmark);

    public abstract long saveBookmark(DjVuBookmark djVuBookmark);

    public abstract void deleteBookmark(DjVuBookmark djVuBookmark);

//    protected ZLTextHighlightMark

    protected abstract ZLTextPosition getStoredPosition(long bookId);

    protected abstract void storePosition(long bookId, ZLTextPosition position);

    protected abstract boolean insertIntoBookList(long bookId);

    public abstract boolean deleteFromBookList(long bookId);

    protected abstract boolean checkBookList(long bookId);

    protected abstract Collection<String> loadVisitedHyperlinks(long bookId);

    protected abstract void addVisitedHyperlink(long bookId, String hyperlinkId);

    public abstract String getSyncTime(long id);

//    public abstract String getSyncModifyDate(long id);

    public abstract void updateSyncTime(String version, long id);

//    public abstract void updateSyncModifyDate(String date, long id);

}
