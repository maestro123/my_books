/*
 * Copyright (C) 2007-2011 Geometer Plus <contact@geometerplus.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

package org.geometerplus.fbreader;

import android.os.Environment;
import org.geometerplus.zlibrary.core.options.ZLStringOption;
import org.geometerplus.zlibrary.ui.android.library.ZLAndroidApplication;

import java.io.File;

public abstract class Paths {

    public static final String PRESTIGIO_DEVICE_WITH_STRANGE_MOUNT = "PMP3084";
    private static final String PRESTIGIO_DEVICE_WITH_STRANGE_MOUNT_STORAGE_PATH = "/flash";

    public static String cardDirectory() {
        String model = android.os.Build.MODEL;
        if (!model.contains(PRESTIGIO_DEVICE_WITH_STRANGE_MOUNT)) {
            return Environment.getExternalStorageDirectory().getPath();
        } else {
            return PRESTIGIO_DEVICE_WITH_STRANGE_MOUNT_STORAGE_PATH;
        }
    }

    public static ZLStringOption BooksDirectoryOption() {
        return new ZLStringOption("Files", "BooksDirectory", cardDirectory() + "/Books");
    }

    public static ZLStringOption FontsDirectoryOption() {
        return new ZLStringOption("Files", "FontsDirectory", cardDirectory() + "/Fonts");
    }

    public static ZLStringOption WallpapersDirectoryOption() {
        return new ZLStringOption("Files", "WallpapersDirectory", cardDirectory() + "/Wallpapers");
    }

    public static ZLStringOption TempDirectoryOption =
            new ZLStringOption("Files", "TemporaryDirectory", "");

    public static String cacheDirectory() {
        String cachePath;

        try {
            cachePath = ZLAndroidApplication.Instance().getExternalCacheDir().getPath() + "/.PrestigioReader";
        } catch (Exception e){
            cachePath = ZLAndroidApplication.Instance().getCacheDir().getPath() + "/.PrestigioReader";
        }

        new File(cachePath).mkdirs();

        if (new File(cachePath).canWrite())
            return cachePath;
        else {
            cachePath = ZLAndroidApplication.Instance().getCacheDir().getPath() + "/.PrestigioReader";
            new File(cachePath).mkdirs();
            return cachePath;
        }
    }

    public static String networkCacheDirectory() {
        return cacheDirectory() + "/cache";
    }

    public static String tempDirectory() {
        return cacheDirectory();
    }

}
