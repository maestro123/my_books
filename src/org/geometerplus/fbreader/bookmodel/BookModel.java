package org.geometerplus.fbreader.bookmodel;

import org.geometerplus.fbreader.formats.BuiltinFormatPlugin;
import org.geometerplus.fbreader.formats.FormatPlugin;
import org.geometerplus.fbreader.formats.PluginCollection;
import org.geometerplus.fbreader.library.Book;
import org.geometerplus.zlibrary.core.fonts.FileInfo;
import org.geometerplus.zlibrary.core.fonts.FontEntry;
import org.geometerplus.zlibrary.core.fonts.FontManager;
import org.geometerplus.zlibrary.text.model.ZLTextModel;

import java.util.Arrays;
import java.util.List;

public abstract class BookModel {

    public final Book Book;
    public final TOCTree TOCTree = new TOCTree();
    public final FontManager FontManager = new FontManager();

    public static BookModel createModel(Book book) throws BookReadingException {
        final FormatPlugin plugin = PluginCollection.getInstance().getPlugin(book.File);
        if (plugin instanceof BuiltinFormatPlugin) {
            final BookModel model = new NativeBookModel(book);
            ((BuiltinFormatPlugin) plugin).readModel(model);
            return model;
        }
        if (plugin == null) {
            throw new BookReadingException("unknownPluginType");
        }
        throw new BookReadingException("unknownPluginType", null, new String[]{plugin.type().toString()});
    }

    public static final class Label {
        public final String ModelId;
        public final int ParagraphIndex;

        public Label(String modelId, int paragraphIndex) {
            ModelId = modelId;
            ParagraphIndex = paragraphIndex;
        }
    }

    protected BookModel(Book book) {
        Book = book;
    }

    public abstract ZLTextModel getTextModel();

    public abstract ZLTextModel getFootnoteModel(String id);

    protected abstract Label getLabelInternal(String id);

    public interface LabelResolver {
        List<String> getCandidates(String id);
    }

    private LabelResolver myResolver;

    public void setLabelResolver(LabelResolver resolver) {
        myResolver = resolver;
    }

    public Label getLabel(String id) {
        Label label = getLabelInternal(id);
        if (label == null && myResolver != null) {
            for (String candidate : myResolver.getCandidates(id)) {
                label = getLabelInternal(candidate);
                if (label != null) {
                    break;
                }
            }
        }
        return label;
    }

    public void registerFontFamilyList(String[] families) {
        FontManager.index(Arrays.asList(families));
    }

    public void registerFontEntry(String family, FontEntry entry) {
        FontManager.Entries.put(family, entry);
    }

    public void registerFontEntry(String family, FileInfo normal, FileInfo bold, FileInfo italic, FileInfo boldItalic) {
        registerFontEntry(family, new FontEntry(family, normal, bold, italic, boldItalic));
    }
}
