package org.geometerplus.fbreader.formats;

import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.text.TextUtils;
import org.geometerplus.fbreader.bookmodel.BookReadingException;
import org.geometerplus.fbreader.library.Book;
import org.geometerplus.zlibrary.core.encodings.EncodingCollection;
import org.geometerplus.zlibrary.core.filesystem.ZLFile;
import org.geometerplus.zlibrary.core.image.ZLImage;
import org.geometerplus.zlibrary.ui.android.image.ZLBitmapImage;

/**
 * Created by Artyom on 5/28/2015.
 */
public class AudioBookFormatPlugin extends FormatPlugin {

    protected AudioBookFormatPlugin() {
        super("AudioBook");
    }

    @Override
    public void readMetainfo(Book book) throws BookReadingException {
        MediaMetadataRetriever retriever = getRetriever(book.File);
        try {
            String author = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_AUTHOR);
            if (TextUtils.isEmpty(author)) {
                author = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
                if (TextUtils.isEmpty(author)) {
                    author = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUMARTIST);
                    if (TextUtils.isEmpty(author)) {
                        author = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_COMPOSER);
                        if (TextUtils.isEmpty(author)) {
                            author = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_WRITER);
                        }
                    }
                }
            }
            book.addAuthor(author);
            book.addTag(retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_GENRE));
            String title = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
            if (TextUtils.isEmpty(title)) {
                title = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM);
            }
            book.setTitle(title);
        } finally {
            retriever.release();
        }
    }

    @Override
    public void readUids(Book book) throws BookReadingException {

    }

    @Override
    public void detectLanguageAndEncoding(Book book) throws BookReadingException {

    }

    @Override
    public ZLImage readCover(ZLFile file) {
        MediaMetadataRetriever retriever = getRetriever(file);
        try {
            byte[] rawBytes = retriever.getEmbeddedPicture();
            if (rawBytes != null && rawBytes.length > 0) {
                return new ZLBitmapImage(BitmapFactory.decodeByteArray(rawBytes, 0, rawBytes.length));
            }
            return null;
        } finally {
            retriever.release();
        }
    }

    @Override
    public String readAnnotation(ZLFile file) {
        return null;
    }

    @Override
    public Type type() {
        return Type.BUILTIN;
    }

    @Override
    public EncodingCollection supportedEncodings() {
        return null;
    }

    private final MediaMetadataRetriever getRetriever(ZLFile file) {
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(file.getPath());
        return retriever;
    }
}
