package org.geometerplus.fbreader.formats;

import org.geometerplus.zlibrary.core.filesystem.ZLFile;
import org.geometerplus.zlibrary.core.filetypes.FileType;
import org.geometerplus.zlibrary.core.filetypes.FileTypeCollection;

import java.util.LinkedList;
import java.util.List;

public class PluginCollection {

    public static final String TAG = PluginCollection.class.getSimpleName();

    private static PluginCollection ourInstance;

    private final List<BuiltinFormatPlugin> myBuiltinPlugins = new LinkedList<>();
    private final List<FormatPlugin> mAdditionalPlugins = new LinkedList<>();

    public synchronized static PluginCollection getInstance() {
        if (ourInstance == null) {
            ourInstance = new PluginCollection();
            for (NativeFormatPlugin p : ourInstance.nativePlugins()) {
                ourInstance.myBuiltinPlugins.add(p);
            }
            ourInstance.mAdditionalPlugins.add(new AudioBookFormatPlugin());
        }
        return ourInstance;
    }

    private PluginCollection() {
    }

    public FormatPlugin getPlugin(ZLFile file) {
        final FileType fileType = FileTypeCollection.Instance.typeForFile(file);
        final FormatPlugin plugin = getPlugin(fileType);
        return plugin;
    }

    public FormatPlugin getPlugin(FileType fileType) {
        if (fileType == null) {
            return null;
        }

        for (FormatPlugin p : myBuiltinPlugins) {
            if (fileType.Id.equalsIgnoreCase(p.supportedFileType())) {
                return p;
            }
        }

        for (FormatPlugin p : mAdditionalPlugins) {
            if (fileType.Id.equalsIgnoreCase(p.supportedFileType())) {
                return p;
            }
        }

        return null;
    }

    private synchronized native NativeFormatPlugin[] nativePlugins();

    private synchronized native void free();

    protected void finalize() throws Throwable {
        free();
        super.finalize();
    }

    static {
        System.loadLibrary("NativeFormats-v4");
    }

}