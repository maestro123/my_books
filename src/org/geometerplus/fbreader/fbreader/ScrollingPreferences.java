/*
 * Copyright (C) 2007-2011 Geometer Plus <contact@geometerplus.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

package org.geometerplus.fbreader.fbreader;

import com.prestigio.android.ereader.read.ShelfReadSettingsHolder;
import com.prestigio.android.ereader.read.maestro.MTextOptions;
import org.geometerplus.zlibrary.core.options.ZLEnumOption;

public class ScrollingPreferences {

    private static ScrollingPreferences ourInstance;

    public static ScrollingPreferences Instance() {
        return (ourInstance != null) ? ourInstance : new ScrollingPreferences();
    }

    public enum FingerScrolling {
        byTap, byFlick, byTapAndFlick
    }

    public enum AnimationSpeed {
        FAST, NORMAL, SLOW
    }

    public final ZLEnumOption<FingerScrolling> FingerScrollingOption = new ZLEnumOption<>("Scrolling", "Finger", FingerScrolling.byTapAndFlick);

    public final ZLEnumOption<AnimationSpeed> AnimationSpeedOption = new ZLEnumOption<AnimationSpeed>("Animation", "Speed", AnimationSpeed.NORMAL);

    private ScrollingPreferences() {
        ourInstance = this;
    }

    public boolean canScroll() {
        return FingerScrollingOption.getValue() == FingerScrolling.byTapAndFlick
                || FingerScrollingOption.getValue() == FingerScrolling.byFlick;
    }

    public boolean canScrollByClick() {
        return FingerScrollingOption.getValue() == FingerScrolling.byTap
                || FingerScrollingOption.getValue() == FingerScrolling.byTapAndFlick;
    }

    public boolean canAnimateSwap(boolean drm) {
        return !(MTextOptions.getInstance().isPowerSaveMode()
                || (drm ? ShelfReadSettingsHolder.getInstance().getBookAnimationSimpleDrm() == ShelfReadSettingsHolder.BOOK_ANIMATION_SIMPLE_DRM.NONE
                : ShelfReadSettingsHolder.getInstance().getBookAnimationSimple() == ShelfReadSettingsHolder.BOOK_ANIMATION_SIMPLE.NONE));
    }

    public float getAnimationSpeed(float duration) {
        switch (AnimationSpeedOption.getValue()) {
            case FAST:
                return duration * .5f;
            case SLOW:
                return duration * 1.5f;
            default:
                return duration;
        }
    }

}
