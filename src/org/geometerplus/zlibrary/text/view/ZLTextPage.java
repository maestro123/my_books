/*
 * Copyright (C) 2007-2011 Geometer Plus <contact@geometerplus.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

package org.geometerplus.zlibrary.text.view;

import android.graphics.Bitmap;
import android.graphics.Picture;
import com.prestigio.android.ereader.read.maestro.MDrawBitmapManager;
import com.prestigio.android.ereader.read.maestro.MTextView;

import java.util.ArrayList;
import java.util.HashMap;

public class ZLTextPage {

    public final ZLTextWordCursor StartCursor = new ZLTextWordCursor();
    public final ZLTextWordCursor EndCursor = new ZLTextWordCursor();
    public final ArrayList<ZLTextLineInfo> LineInfos = new ArrayList<ZLTextLineInfo>();
    public int PaintState = PaintStateEnum.NOTHING_TO_PAINT;

    public final ZLTextElementAreaVector TextElementMap = new ZLTextElementAreaVector();

    private ZLTextRegion mSelectedRegion;
    public int Column0Height;
    boolean isTwoColumnView;
    private int myColumnWidth;
    private int myHeight;

    private HashMap<MTextView.PAINT_LAYER, Picture> mDrawLayers = new HashMap<MTextView.PAINT_LAYER, Picture>();
    private MDrawBitmapManager.MDrawBitmapDrawable finalBitmapDrawable, backBitmapDrawable;
    public int[] Labels;
    private MTextView.PagePosition mPagePosition;
    private boolean isNull;

    public void setup(int columnWidth, int height, boolean twoColumnView) {
        myColumnWidth = columnWidth;
        myHeight = height;
        isTwoColumnView = twoColumnView;
    }

    public void reset() {
        StartCursor.reset();
        EndCursor.reset();
        LineInfos.clear();
        PaintState = PaintStateEnum.NOTHING_TO_PAINT;
        mPagePosition = null;
    }

    public void moveStartCursor(ZLTextParagraphCursor cursor) {
        StartCursor.setCursor(cursor);
        EndCursor.reset();
        LineInfos.clear();
        PaintState = PaintStateEnum.START_IS_KNOWN;
    }

    public void moveStartCursor(int paragraphIndex, int wordIndex, int charIndex) {
        if (StartCursor.isNull()) {
            StartCursor.setCursor(EndCursor);
        }
        StartCursor.moveToParagraph(paragraphIndex);
        StartCursor.moveTo(wordIndex, charIndex);
        EndCursor.reset();
        LineInfos.clear();
        PaintState = PaintStateEnum.START_IS_KNOWN;
    }

    public void moveEndCursor(ZLTextParagraphCursor cursor) {
        EndCursor.setCursor(cursor);
        StartCursor.reset();
        LineInfos.clear();
        PaintState = PaintStateEnum.END_IS_KNOWN;
    }

    public void moveEndCursor(int paragraphIndex, int wordIndex, int charIndex) {
        if (EndCursor.isNull()) {
            EndCursor.setCursor(StartCursor);
        }
        EndCursor.moveToParagraph(paragraphIndex);
        if ((paragraphIndex > 0) && (wordIndex == 0) && (charIndex == 0)) {
            EndCursor.previousParagraph();
            EndCursor.moveToParagraphEnd();
        } else {
            EndCursor.moveTo(wordIndex, charIndex);
        }
        StartCursor.reset();
        LineInfos.clear();
        PaintState = PaintStateEnum.END_IS_KNOWN;
    }

    public boolean isEmptyPage() {
        for (ZLTextLineInfo info : LineInfos) {
            if (info.IsVisible) {
                return false;
            }
        }
        return true;
    }

    public void findLineFromStart(ZLTextWordCursor cursor, int overlappingValue) {
        if (LineInfos.isEmpty() || (overlappingValue == 0)) {
            cursor.reset();
            return;
        }
        ZLTextLineInfo info = null;
        for (ZLTextLineInfo i : LineInfos) {
            info = i;
            if (info.IsVisible) {
                --overlappingValue;
                if (overlappingValue == 0) {
                    break;
                }
            }
        }
        cursor.setCursor(info.ParagraphCursor);
        cursor.moveTo(info.EndElementIndex, info.EndCharIndex);
    }

    public void findLineFromEnd(ZLTextWordCursor cursor, int overlappingValue) {
        if (LineInfos.isEmpty() || (overlappingValue == 0)) {
            cursor.reset();
            return;
        }
        final ArrayList<ZLTextLineInfo> infos = LineInfos;
        final int size = infos.size();
        ZLTextLineInfo info = null;
        for (int i = size - 1; i >= 0; --i) {
            info = infos.get(i);
            if (info.IsVisible) {
                --overlappingValue;
                if (overlappingValue == 0) {
                    break;
                }
            }
        }
        cursor.setCursor(info.ParagraphCursor);
        cursor.moveTo(info.StartElementIndex, info.StartCharIndex);
    }

    public void findPercentFromStart(ZLTextWordCursor cursor, int areaHeight, int percent) {
        if (LineInfos.isEmpty()) {
            cursor.reset();
            return;
        }
        int height = areaHeight * percent / 100;
        boolean visibleLineOccurred = false;
        ZLTextLineInfo info = null;
        for (ZLTextLineInfo i : LineInfos) {
            info = i;
            if (info.IsVisible) {
                visibleLineOccurred = true;
            }
            height -= info.Height + info.Descent + info.VSpaceAfter;
            if (visibleLineOccurred && (height <= 0)) {
                break;
            }
        }
        cursor.setCursor(info.ParagraphCursor);
        cursor.moveTo(info.EndElementIndex, info.EndCharIndex);
    }

    void findPercentFromStart(ZLTextWordCursor cursor, int percent) {
        if (LineInfos.isEmpty()) {
            cursor.reset();
            return;
        }
        int height = myHeight * percent / 100;
        boolean visibleLineOccured = false;
        ZLTextLineInfo info = null;
        for (ZLTextLineInfo i : LineInfos) {
            info = i;
            if (info.IsVisible) {
                visibleLineOccured = true;
            }
            height -= info.Height + info.Descent + info.VSpaceAfter;
            if (visibleLineOccured && (height <= 0)) {
                break;
            }
        }
        cursor.setCursor(info.ParagraphCursor);
        cursor.moveTo(info.EndElementIndex, info.EndCharIndex);
    }

    public void setSelectedRegion(ZLTextRegion region) {
        mSelectedRegion = region;
    }

    public ZLTextRegion getSelectedRegion() {
        return mSelectedRegion;
    }

    public void addPictureLayer(MTextView.PAINT_LAYER layer, Picture picture) {
        mDrawLayers.remove(layer);
        mDrawLayers.put(layer, picture);
    }

    public Picture getPictureLayer(MTextView.PAINT_LAYER layer) {
        return mDrawLayers.get(layer);
    }

    public void removePictureLayer(MTextView.PAINT_LAYER layer) {
        mDrawLayers.remove(layer);
    }

    public boolean isReady() {
        return finalBitmapDrawable != null || mDrawLayers.containsKey(MTextView.PAINT_LAYER.FINAL);
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof ZLTextPage) {
            ZLTextPage page = (ZLTextPage) o;
            return page.StartCursor.equals(StartCursor) && page.EndCursor.equals(EndCursor);
        }
        return super.equals(o);
    }

    @Override
    public String toString() {
        return new StringBuilder("[").append(ZLTextPage.class.getName()).append("]:")
                .append(StartCursor.toString()).append(", ").append(EndCursor.toString()).toString();
    }

    public Bitmap getFinalBitmap() {
        return finalBitmapDrawable != null ? finalBitmapDrawable.getBitmap() : null;
    }

    public MDrawBitmapManager.MDrawBitmapDrawable getFinalBitmapDrawable() {
        return finalBitmapDrawable;
    }

    public void setFinalBitmap(MDrawBitmapManager.MDrawBitmapDrawable bitmapDrawable) {
        finalBitmapDrawable = bitmapDrawable;
        if (finalBitmapDrawable != null)
            finalBitmapDrawable.setIsUsed(true);
    }

    public void setBackBitmapDrawable(MDrawBitmapManager.MDrawBitmapDrawable bitmapDrawable) {
        backBitmapDrawable = bitmapDrawable;
        if (backBitmapDrawable != null)
            backBitmapDrawable.setIsUsed(true);
    }

    public Bitmap getBackBitmap() {
        return backBitmapDrawable != null ? backBitmapDrawable.getBitmap() : null;
    }

    public MDrawBitmapManager.MDrawBitmapDrawable getBackBitmapDrawable() {
        return backBitmapDrawable;
    }

    public void release() {
        isNull = true;
        mSelectedRegion = null;
        clearDrawCache();
    }

    public void clearDrawCache() {
        mDrawLayers.clear();
        if (finalBitmapDrawable != null) {
            finalBitmapDrawable.setIsUsed(false);
            finalBitmapDrawable = null;
        }
        if (backBitmapDrawable != null) {
            backBitmapDrawable.setIsUsed(false);
            backBitmapDrawable = null;
        }
    }

    public boolean hasValidBitmaps() {
        return (finalBitmapDrawable == null || finalBitmapDrawable.hasValidBitmap()) && (backBitmapDrawable == null || backBitmapDrawable.hasValidBitmap());
    }

    int getTextWidth() {
        return myColumnWidth;
    }

    public int getTextHeight() {
        return myHeight;
    }

    public boolean isTwoColumnView() {
        return isTwoColumnView;
    }

    public void setPagePosition(MTextView.PagePosition pagePosition) {
        this.mPagePosition = pagePosition;
    }

    public MTextView.PagePosition getPagePosition() {
        return mPagePosition;
    }

    public void setIsNull(boolean value) {
        isNull = value;
    }

    public boolean isNull() {
        return isNull;
    }

}
