/*
 * Copyright (C) 2007-2011 Geometer Plus <contact@geometerplus.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

package org.geometerplus.zlibrary.text.view;

public final class ZLTextLineInfo {
    public final ZLTextParagraphCursor ParagraphCursor;
    public final int ParagraphCursorLength;

    public int StartElementIndex;
    public int StartCharIndex;
    public int RealStartElementIndex;
    public int RealStartCharIndex;
    public int EndElementIndex;
    public int EndCharIndex;

    public boolean IsVisible;
    public int LeftIndent;
    public int Width;
    public int Height;
    public int Descent;
    public int VSpaceAfter;
    public int VSpaceBefore;
    public boolean PreviousInfoUsed;
    public int SpaceCounter;
    public ZLTextStyle StartStyle;

    public ZLTextLineInfo(ZLTextParagraphCursor paragraphCursor, int elementIndex, int charIndex, ZLTextStyle style) {
        ParagraphCursor = paragraphCursor;
        ParagraphCursorLength = paragraphCursor.getParagraphLength();
        StartElementIndex = elementIndex;
        StartCharIndex = charIndex;
        RealStartElementIndex = elementIndex;
        RealStartCharIndex = charIndex;
        EndElementIndex = elementIndex;
        EndCharIndex = charIndex;
        StartStyle = style;
    }

    public boolean isEndOfParagraph() {
        return EndElementIndex == ParagraphCursorLength;
    }

    public void adjust(ZLTextLineInfo previous) {
        if (!PreviousInfoUsed && previous != null) {
            Height -= Math.min(previous.VSpaceAfter, VSpaceBefore);
            PreviousInfoUsed = true;
        }
    }

    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (!(o instanceof ZLTextLineInfo)) {
            return false;
        }
        ZLTextLineInfo info = (ZLTextLineInfo) o;
        return (ParagraphCursor == info.ParagraphCursor) &&
                (StartElementIndex == info.StartElementIndex) &&
                (StartCharIndex == info.StartCharIndex);
    }

    public int hashCode() {
        return ParagraphCursor.hashCode() + StartElementIndex + 239 * StartCharIndex;
    }
}
