/*
 * Copyright (C) 2007-2014 Geometer Plus <contact@geometerplus.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

package org.geometerplus.zlibrary.text.view.style;

import org.geometerplus.zlibrary.core.options.ZLStringOption;
import org.geometerplus.zlibrary.core.util.ZLBoolean3;
import org.geometerplus.zlibrary.text.model.ZLTextAlignmentType;
import org.geometerplus.zlibrary.text.model.ZLTextMetrics;
import org.geometerplus.zlibrary.text.model.ZLTextStyleEntry;

import java.util.HashMap;
import java.util.Map;

public class ZLTextNGStyleDescription {

    public static final String ITALIC = "italic";
    public static final String OBLIQUE = "oblique";
    public static final String BOLD = "bold";
    public static final String NORMAL = "normal";
    public static final String UNDERLINE = "underline";
    public static final String LINE_THROUGH = "line-through";

    public static final String AUTO = "auto";
    public static final String NONE = "none";
    public static final String UNDEFINED = "undefined";

    public static final String INHERIT = "inherit";

    public static final String ALIGNMENT_CENTER = "center";
    public static final String ALIGNMENT_LEFT = "left";
    public static final String ALIGNMENT_RIGHT = "right";
    public static final String ALIGNMENT_JUSTIFY = "justify";

    public final String Name;

    public final ZLStringOption FontFamilyOption;
    public final ZLStringOption FontSizeOption;
    public final ZLStringOption FontWeightOption;
    public final ZLStringOption FontStyleOption;
    public final ZLStringOption TextDecorationOption;
    public final ZLStringOption AlignmentOption;
    public final ZLStringOption HyphenationOption;

    public final ZLStringOption MarginTopOption;
    public final ZLStringOption MarginBottomOption;
    public final ZLStringOption MarginLeftOption;
    public final ZLStringOption MarginRightOption;

    //FIRST LINE INDENT
    public final ZLStringOption TextIndentOption;
    //LINE SPACE (what a fu**ing names!!!???)
    public final ZLStringOption LineHeightOption;

    public final ZLStringOption VerticalAlignOption;

    private static ZLStringOption createOption(String selector, String name, Map<String, String> valueMap) {
        return new ZLStringOption("Style", selector + "::" + name, valueMap.get(name));
    }

    ZLTextNGStyleDescription(String selector, Map<String, String> valueMap) {
        Name = valueMap.get("fbreader-name");

        //?????????
        FontFamilyOption = createOption(selector, "font-family", valueMap);

        //?????????
        FontSizeOption = createOption(selector, "font-size", valueMap);
        FontSizeOption.setup(0.1, 0.1, 4.0, "em");

        //bold (boolean)
        FontWeightOption = createOption(selector, "font-weight", valueMap);

        //italic (boolean)
        FontStyleOption = createOption(selector, "font-style", valueMap);

        //line-through, underline
        TextDecorationOption = createOption(selector, "text-decoration", valueMap);

        //boolean
        HyphenationOption = createOption(selector, "hyphens", valueMap);

        //????????? (em)
        MarginTopOption = createOption(selector, "margin-top", valueMap);
        MarginTopOption.setup(0.1, 0.0, 4.0, "em");
        //????????? (em)
        MarginBottomOption = createOption(selector, "margin-bottom", valueMap);
        MarginBottomOption.setup(0.1, 0.0, 4.0, "em");
        //????????? (em)
        MarginLeftOption = createOption(selector, "margin-left", valueMap);
        MarginLeftOption.setup(0.1, 0.0, 4.0, "em");
        //????????? (em)
        MarginRightOption = createOption(selector, "margin-right", valueMap);
        //????????? (pt) (first line indent?)
        TextIndentOption = createOption(selector, "text-indent", valueMap);
        // alignment
        AlignmentOption = createOption(selector, "text-align", valueMap);

        //????????? (em)
        VerticalAlignOption = createOption(selector, "vertical-align", valueMap);
        //?????????
        LineHeightOption = createOption(selector, "line-height", valueMap);
    }

    public int getFontSize(ZLTextMetrics metrics, int parentFontSize) {
        final ZLTextStyleEntry.Length length = parseLength(FontSizeOption.getValue());
        if (length == null) {
            return parentFontSize;
        }
        return ZLTextStyleEntry.compute(
                length, metrics, parentFontSize, ZLTextStyleEntry.Feature.LENGTH_FONT_SIZE
        );
    }

    public int getVerticalAlign(ZLTextMetrics metrics, int base, int fontSize) {
        final ZLTextStyleEntry.Length length = parseLength(VerticalAlignOption.getValue());
        if (length == null) {
            return base;
        }
        return ZLTextStyleEntry.compute(
                // TODO: add new length for vertical alignment
                length, metrics, fontSize, ZLTextStyleEntry.Feature.LENGTH_FONT_SIZE
        );
    }

    public int getLeftIndent(ZLTextMetrics metrics, int base, int fontSize) {
        final ZLTextStyleEntry.Length length = parseLength(MarginLeftOption.getValue());
        if (length == null) {
            return base;
        }
        return ZLTextStyleEntry.compute(
                length, metrics, fontSize, ZLTextStyleEntry.Feature.LENGTH_LEFT_INDENT
        );
    }

    public int getRightIndent(ZLTextMetrics metrics, int base, int fontSize) {
        final ZLTextStyleEntry.Length length = parseLength(MarginRightOption.getValue());
        if (length == null) {
            return base;
        }
        return ZLTextStyleEntry.compute(
                length, metrics, fontSize, ZLTextStyleEntry.Feature.LENGTH_RIGHT_INDENT
        );
    }

    public int getFirstLineIndent(ZLTextMetrics metrics, int base, int fontSize) {
        final ZLTextStyleEntry.Length length = parseLength(TextIndentOption.getValue());
        if (length == null) {
            return base;
        }
        return ZLTextStyleEntry.compute(
                length, metrics, fontSize, ZLTextStyleEntry.Feature.LENGTH_FIRST_LINE_INDENT
        );
    }

    public int getSpaceBefore(ZLTextMetrics metrics, int base, int fontSize) {
        final ZLTextStyleEntry.Length length = parseLength(MarginTopOption.getValue());
        if (length == null) {
            return base;
        }
        return ZLTextStyleEntry.compute(
                length, metrics, fontSize, ZLTextStyleEntry.Feature.LENGTH_SPACE_BEFORE
        );
    }

    public int getSpaceAfter(ZLTextMetrics metrics, int base, int fontSize) {
        final ZLTextStyleEntry.Length length = parseLength(MarginBottomOption.getValue());
        if (length == null) {
            return base;
        }
        return ZLTextStyleEntry.compute(
                length, metrics, fontSize, ZLTextStyleEntry.Feature.LENGTH_SPACE_AFTER
        );
    }

    public ZLBoolean3 isBold() {
        final String fontWeight = FontWeightOption.getValue();
        if (BOLD.equals(fontWeight)) {
            return ZLBoolean3.B3_TRUE;
        } else if (NORMAL.equals(fontWeight)) {
            return ZLBoolean3.B3_FALSE;
        } else {
            return ZLBoolean3.B3_UNDEFINED;
        }
    }

    public ZLBoolean3 isItalic() {
        final String fontStyle = FontStyleOption.getValue();
        if (ITALIC.equals(fontStyle) || OBLIQUE.equals(fontStyle)) {
            return ZLBoolean3.B3_TRUE;
        } else if (NORMAL.equals(fontStyle)) {
            return ZLBoolean3.B3_FALSE;
        } else {
            return ZLBoolean3.B3_UNDEFINED;
        }
    }

    public ZLBoolean3 isUnderlined() {
        final String textDecoration = TextDecorationOption.getValue();
        if (UNDERLINE.equals(textDecoration)) {
            return ZLBoolean3.B3_TRUE;
        } else if ("".equals(textDecoration) || INHERIT.equals(textDecoration) || UNDEFINED.equals(textDecoration)) {
            return ZLBoolean3.B3_UNDEFINED;
        } else {
            return ZLBoolean3.B3_FALSE;
        }
    }

    public ZLBoolean3 isStrikedThrough() {
        final String textDecoration = TextDecorationOption.getValue();
        if (LINE_THROUGH.equals(textDecoration)) {
            return ZLBoolean3.B3_TRUE;
        } else if ("".equals(textDecoration) || INHERIT.equals(textDecoration) || UNDEFINED.equals(textDecoration)) {
            return ZLBoolean3.B3_UNDEFINED;
        } else {
            return ZLBoolean3.B3_FALSE;
        }
    }

    public byte getAlignment() {
        final String alignment = AlignmentOption.getValue();
        if (alignment.length() == 0) {
            return ZLTextAlignmentType.ALIGN_UNDEFINED;
        } else if (ALIGNMENT_CENTER.equals(alignment)) {
            return ZLTextAlignmentType.ALIGN_CENTER;
        } else if (ALIGNMENT_LEFT.equals(alignment)) {
            return ZLTextAlignmentType.ALIGN_LEFT;
        } else if (ALIGNMENT_RIGHT.equals(alignment)) {
            return ZLTextAlignmentType.ALIGN_RIGHT;
        } else if (ALIGNMENT_JUSTIFY.equals(alignment)) {
            return ZLTextAlignmentType.ALIGN_JUSTIFY;
        } else {
            return ZLTextAlignmentType.ALIGN_UNDEFINED;
        }
    }

    public String getAlignment(int alignment) {
        switch (alignment) {
            case 1:
                return ALIGNMENT_LEFT;
            case 2:
                return ALIGNMENT_RIGHT;
            case 3:
                return ALIGNMENT_CENTER;
            case 4:
                return ALIGNMENT_JUSTIFY;
            default:
                return "";
        }
    }

    public ZLBoolean3 allowHyphenations() {
        final String hyphen = HyphenationOption.getValue();
        if (AUTO.equals(hyphen)) {
            return ZLBoolean3.B3_TRUE;
        } else if (NONE.equals(hyphen)) {
            return ZLBoolean3.B3_FALSE;
        } else {
            return ZLBoolean3.B3_UNDEFINED;
        }
    }

    private static final Map<String, Object> ourCache = new HashMap<String, Object>();
    private static final Object ourNullObject = new Object();

    private static ZLTextStyleEntry.Length parseLength(String value) {
        if (value.length() == 0) {
            return null;
        }

        final Object cached = ourCache.get(value);
        if (cached != null && cached != ourNullObject) {
            return cached == ourNullObject ? null : (ZLTextStyleEntry.Length) cached;
        }

        ZLTextStyleEntry.Length length = null;
        try {
            if (value.endsWith("%")) {
                length = new ZLTextStyleEntry.Length(
                        Short.valueOf(value.substring(0, value.length() - 1)),
                        ZLTextStyleEntry.SizeUnit.PERCENT
                );
            } else if (value.endsWith("rem")) {
                length = new ZLTextStyleEntry.Length(
                        (short) (100 * Double.valueOf(value.substring(0, value.length() - 3))),
                        ZLTextStyleEntry.SizeUnit.REM_100
                );
            } else if (value.endsWith("em")) {
                length = new ZLTextStyleEntry.Length(
                        (short) (100 * Double.valueOf(value.substring(0, value.length() - 2))),
                        ZLTextStyleEntry.SizeUnit.EM_100
                );
            } else if (value.endsWith("ex")) {
                length = new ZLTextStyleEntry.Length(
                        (short) (100 * Double.valueOf(value.substring(0, value.length() - 2))),
                        ZLTextStyleEntry.SizeUnit.EX_100
                );
            } else if (value.endsWith("px")) {
                length = new ZLTextStyleEntry.Length(
                        Short.valueOf(value.substring(0, value.length() - 2)),
                        ZLTextStyleEntry.SizeUnit.PIXEL
                );
            } else if (value.endsWith("pt")) {
                length = new ZLTextStyleEntry.Length(
                        Short.valueOf(value.substring(0, value.length() - 2)),
                        ZLTextStyleEntry.SizeUnit.POINT
                );
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        ourCache.put(value, length != null ? length : ourNullObject);
        return length;
    }
}
