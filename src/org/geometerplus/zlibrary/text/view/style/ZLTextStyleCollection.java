package org.geometerplus.zlibrary.text.view.style;

import com.prestigio.android.ereader.read.maestro.MTextOptions;
import org.geometerplus.zlibrary.core.filesystem.ZLResourceFile;
import org.geometerplus.zlibrary.core.xml.ZLStringMap;
import org.geometerplus.zlibrary.core.xml.ZLXMLReaderAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class ZLTextStyleCollection {

    private static volatile ZLTextStyleCollection instance;

    public static ZLTextStyleCollection Instance() {
        return instance == null ? instance = new ZLTextStyleCollection("Base") : instance;
    }

    public final String Screen;

    private final List<ZLTextNGStyleDescription> myDescriptionList;
    private final ZLTextNGStyleDescription[] myDescriptionMap = new ZLTextNGStyleDescription[256];

    private ZLTextBaseStyle myBaseStyle;

    public ZLTextStyleCollection(String screen) {
        Screen = screen;
        final Map<Integer, ZLTextNGStyleDescription> descriptions =
                new SimpleCSSReader().read(ZLResourceFile.createResourceFile("default/styles.css"));
        myDescriptionList = Collections.unmodifiableList(
                new ArrayList<ZLTextNGStyleDescription>(descriptions.values()));
        for (Map.Entry<Integer, ZLTextNGStyleDescription> entry : descriptions.entrySet()) {
            myDescriptionMap[entry.getKey() & 0xFF] = entry.getValue();
        }
        myBaseStyle = new ZLTextBaseStyle("Base", "Droid", MTextOptions.getInstance().getDefaultFontSize());
        new TextStyleReader().readQuietly(ZLResourceFile.createResourceFile("default/styles.xml"));
    }

    public ZLTextBaseStyle getBaseStyle() {
        return myBaseStyle;
    }

    public List<ZLTextNGStyleDescription> getDescriptionList() {
        return myDescriptionList;
    }

    public ZLTextNGStyleDescription getDescription(byte kind) {
        return myDescriptionMap[kind & 0xFF];
    }

    public ZLTextNGStyleDescription getDescriptionByNameHash(int hashCode) {
        for (ZLTextNGStyleDescription description : myDescriptionList) {
            if (description.Name.hashCode() == hashCode) {
                return description;
            }
        }
        return null;
    }

    private class TextStyleReader extends ZLXMLReaderAdapter {
        @Override
        public boolean dontCacheAttributeValues() {
            return true;
        }

        private int intValue(ZLStringMap attributes, String name, int defaultValue) {
            final String value = attributes.getValue(name);
            if (value != null) {
                try {
                    return Integer.parseInt(value);
                } catch (NumberFormatException e) {
                }
            }
            return defaultValue;
        }

        @Override
        public boolean startElementHandler(String tag, ZLStringMap attributes) {
            if ("base".equals(tag) && Screen.equals(attributes.getValue("screen"))) {
                myBaseStyle = new ZLTextBaseStyle(
                        Screen,
                        attributes.getValue("family"),
                        intValue(attributes, "fontSize", MTextOptions.getInstance().getDefaultFontSize())
                );
            }
            return false;
        }
    }
}
