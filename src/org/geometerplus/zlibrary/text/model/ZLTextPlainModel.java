/*
 * Copyright (C) 2007-2014 Geometer Plus <contact@geometerplus.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

package org.geometerplus.zlibrary.text.model;

import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import com.prestigio.android.ereader.read.maestro.MTextBuilder;
import com.prestigio.android.ereader.shelf.views.TypefaceSpan;
import com.prestigio.android.ereader.utils.Colors;
import com.prestigio.android.ereader.utils.UpdatableAsyncTaskLoader;
import org.geometerplus.zlibrary.core.fonts.FontManager;
import org.geometerplus.zlibrary.core.image.ZLImage;
import org.geometerplus.zlibrary.core.util.ZLSearchPattern;
import org.geometerplus.zlibrary.core.util.ZLSearchUtil;
import org.geometerplus.zlibrary.text.view.ZLTextFixedPosition;
import org.geometerplus.zlibrary.text.view.ZLTextParagraphCursor;
import org.geometerplus.zlibrary.text.view.ZLTextWord;
import org.geometerplus.zlibrary.text.view.ZLTextWordCursor;

import java.util.*;

public class ZLTextPlainModel implements ZLTextModel, ZLTextStyleEntry.Feature {

    public static final String TAG = ZLTextPlainModel.class.getSimpleName();

    private final String myId;
    private final String myLanguage;

    protected int[] myStartEntryIndices;
    protected int[] myStartEntryOffsets;
    protected int[] myParagraphLengths;
    protected int[] myTextSizes;
    protected byte[] myParagraphKinds;

    protected int myParagraphsNumber;

    protected final CharStorage myStorage;
    protected final Map<String, ZLImage> myImageMap;

    private ArrayList<ZLTextMark> myMarks, myPrevMarks;
    private String myPreviousSearch;
    private boolean showSearchResults = false;

    private final FontManager myFontManager;

    final class EntryIteratorImpl implements ZLTextParagraph.EntryIterator {
        private int myCounter;
        private int myLength;
        private byte myType;

        int myDataIndex;
        int myDataOffset;

        // TextEntry data
        private char[] myTextData;
        private int myTextOffset;
        private int myTextLength;

        // ControlEntry data
        private byte myControlKind;
        private boolean myControlIsStart;
        // HyperlinkControlEntry data
        private byte myHyperlinkType;
        private String myHyperlinkId;

        // ImageEntry
        private ZLImageEntry myImageEntry;

        // VideoEntry
        private ZLVideoEntry myVideoEntry;

        //AudioEntry
        private ZLAudioEntry myAudioEntry;

        // StyleEntry
        private ZLTextStyleEntry myStyleEntry;

        // FixedHSpaceEntry data
        private short myFixedHSpaceLength;

        EntryIteratorImpl(int index) {
            reset(index);
        }

        void reset(int index) {
            myCounter = 0;
            myLength = myParagraphLengths[index];
            myDataIndex = myStartEntryIndices[index];
            myDataOffset = myStartEntryOffsets[index];
        }

        public byte getType() {
            return myType;
        }

        public char[] getTextData() {
            return myTextData;
        }

        public int getTextOffset() {
            return myTextOffset;
        }

        public int getTextLength() {
            return myTextLength;
        }

        public byte getControlKind() {
            return myControlKind;
        }

        public boolean getControlIsStart() {
            return myControlIsStart;
        }

        public byte getHyperlinkType() {
            return myHyperlinkType;
        }

        public String getHyperlinkId() {
            return myHyperlinkId;
        }

        public ZLImageEntry getImageEntry() {
            return myImageEntry;
        }

        public ZLVideoEntry getVideoEntry() {
            return myVideoEntry;
        }

        @Override
        public ZLAudioEntry getAudioEntry() {
            return myAudioEntry;
        }

        public ZLTextStyleEntry getStyleEntry() {
            return myStyleEntry;
        }

        public short getFixedHSpaceLength() {
            return myFixedHSpaceLength;
        }

        public boolean next() {
            if (myCounter >= myLength) {
                return false;
            }

            int dataOffset = myDataOffset;
            char[] data = null;
            try {
                data = myStorage.block(myDataIndex);
            } catch (CachedCharStorageException e) {
                e.printStackTrace();
                return false;
            }
            if (data == null) {
                return false;
            }
            if (dataOffset >= data.length) {
                data = myStorage.block(++myDataIndex);
                if (data == null) {
                    return false;
                }
                dataOffset = 0;
            }
            if (dataOffset > data.length){
                return false;
            }
            byte type = (byte) data[dataOffset];
            if (type == 0) {
                data = myStorage.block(++myDataIndex);
                if (data == null || data.length == 0) {
                    return false;
                }
                dataOffset = 0;
                type = (byte) data[0];
            }
            myType = type;
            ++dataOffset;
            switch (type) {
                case ZLTextParagraph.Entry.TEXT: {
                    int textLength = (int) data[dataOffset++];
                    textLength += (((int) data[dataOffset++]) << 16);
                    textLength = Math.min(textLength, data.length - dataOffset);
                    myTextLength = textLength;
                    myTextData = data;
                    myTextOffset = dataOffset;
                    dataOffset += textLength;
                    break;
                }
                case ZLTextParagraph.Entry.CONTROL: {
                    short kind = (short) data[dataOffset++];
                    myControlKind = (byte) kind;
                    myControlIsStart = (kind & 0x0100) == 0x0100;
                    myHyperlinkType = 0;
                    break;
                }
                case ZLTextParagraph.Entry.HYPERLINK_CONTROL: {
                    final short kind = (short) data[dataOffset++];
                    myControlKind = (byte) kind;
                    myControlIsStart = true;
                    myHyperlinkType = (byte) (kind >> 8);
                    final short labelLength = (short) data[dataOffset++];
                    myHyperlinkId = new String(data, dataOffset, labelLength);
                    dataOffset += labelLength;
                    break;
                }
                case ZLTextParagraph.Entry.IMAGE: {
                    final short vOffset = (short) data[dataOffset++];
                    final short len = (short) data[dataOffset++];
                    final String id = new String(data, dataOffset, len);
                    dataOffset += len;
                    final boolean isCover = data[dataOffset++] != 0;
                    myImageEntry = new ZLImageEntry(myImageMap, id, vOffset, isCover);
                    break;
                }
                case ZLTextParagraph.Entry.FIXED_HSPACE:
                    myFixedHSpaceLength = (short) data[dataOffset++];
                    break;
                case ZLTextParagraph.Entry.STYLE_CSS:
                case ZLTextParagraph.Entry.STYLE_OTHER: {
                    final ZLTextStyleEntry entry =
                            type == ZLTextParagraph.Entry.STYLE_CSS
                                    ? new ZLTextCSSStyleEntry()
                                    : new ZLTextOtherStyleEntry();

                    final short mask = (short) data[dataOffset++];
                    for (int i = 0; i < NUMBER_OF_LENGTHS; ++i) {
                        if (ZLTextStyleEntry.isFeatureSupported(mask, i)) {
                            final short size = (short) data[dataOffset++];
                            final byte unit = (byte) data[dataOffset++];
                            entry.setLength(i, size, unit);
                        }
                    }
                    if (ZLTextStyleEntry.isFeatureSupported(mask, ALIGNMENT_TYPE)) {
                        final short value = (short) data[dataOffset++];
                        entry.setAlignmentType((byte) (value & 0xFF));
                    }
                    if (ZLTextStyleEntry.isFeatureSupported(mask, FONT_FAMILY)) {
                        entry.setFontFamilies(myFontManager, (short) data[dataOffset++]);
                    }
                    if (ZLTextStyleEntry.isFeatureSupported(mask, FONT_STYLE_MODIFIER)) {
                        final short value = (short) data[dataOffset++];
                        entry.setFontModifiers((byte) (value & 0xFF), (byte) ((value >> 8) & 0xFF));
                    }

                    myStyleEntry = entry;
                }
                case ZLTextParagraph.Entry.STYLE_CLOSE:
                    // No data
                    break;
                case ZLTextParagraph.Entry.RESET_BIDI:
                    // No data
                    break;
                case ZLTextParagraph.Entry.AUDIO:
                    myAudioEntry = new ZLAudioEntry();
                    final short audioMapSize = (short) data[dataOffset++];
                    for (short i = 0; i < audioMapSize; ++i) {
                        try {
                            short len = (short) data[dataOffset++];
                            final String mime = new String(data, dataOffset, len);
                            dataOffset += len;
                            len = (short) data[dataOffset++];
                            final String src = new String(data, dataOffset, len);
                            dataOffset += len;
                            myAudioEntry.addSource(mime, src);
                        } catch (StringIndexOutOfBoundsException e){
                            e.printStackTrace();
                        }
                    }
                    break;
                case ZLTextParagraph.Entry.VIDEO: {
                    myVideoEntry = new ZLVideoEntry();
                    final short mapSize = (short) data[dataOffset++];
                    for (short i = 0; i < mapSize; ++i) {
                        try {
                            short len = (short) data[dataOffset++];
                            final String mime = new String(data, dataOffset, len);
                            dataOffset += len;
                            len = (short) data[dataOffset++];
                            final String src = new String(data, dataOffset, len);
                            dataOffset += len;
                            myVideoEntry.addSource(mime, src);
                        } catch (StringIndexOutOfBoundsException e){
                            e.printStackTrace();
                        }
                    }
                    break;
                }
            }
            ++myCounter;
            myDataOffset = dataOffset;
            return true;
        }
    }

    protected ZLTextPlainModel(
            String id,
            String language,
            int[] entryIndices,
            int[] entryOffsets,
            int[] paragraphLenghts,
            int[] textSizes,
            byte[] paragraphKinds,
            CharStorage storage,
            Map<String, ZLImage> imageMap,
            FontManager fontManager
    ) {
        myId = id;
        myLanguage = language;
        myStartEntryIndices = entryIndices;
        myStartEntryOffsets = entryOffsets;
        myParagraphLengths = paragraphLenghts;
        myTextSizes = textSizes;
        myParagraphKinds = paragraphKinds;
        myStorage = storage;
        myImageMap = imageMap;
        myFontManager = fontManager;
    }

    public final String getId() {
        return myId;
    }

    public final String getLanguage() {
        return myLanguage;
    }

    public final ZLTextMark getFirstMark() {
        return (myMarks == null || myMarks.isEmpty()) ? null : myMarks.get(0);
    }

    public final ZLTextMark getLastMark() {
        return (myMarks == null || myMarks.isEmpty()) ? null : myMarks.get(myMarks.size() - 1);
    }

    public final ZLTextMark getNextMark(ZLTextMark position) {
        if (position == null || myMarks == null) {
            return null;
        }

        ZLTextMark mark = null;
        for (ZLTextMark current : myMarks) {
            if (current.compareTo(position) >= 0) {
                if ((mark == null) || (mark.compareTo(current) > 0)) {
                    mark = current;
                }
            }
        }
        return mark;
    }

    public final ZLTextMark getPreviousMark(ZLTextMark position) {
        if ((position == null) || (myMarks == null)) {
            return null;
        }

        ZLTextMark mark = null;
        for (ZLTextMark current : myMarks) {
            if (current.compareTo(position) < 0) {
                if ((mark == null) || (mark.compareTo(current) < 0)) {
                    mark = current;
                }
            }
        }
        return mark;
    }

    public final int search(final String text, int startIndex, int endIndex, boolean ignoreCase) {
        int count = 0;
        ZLSearchPattern pattern = new ZLSearchPattern(text, ignoreCase);
        myMarks = new ArrayList<ZLTextMark>();
        if (startIndex > myParagraphsNumber) {
            startIndex = myParagraphsNumber;
        }
        if (endIndex > myParagraphsNumber) {
            endIndex = myParagraphsNumber;
        }
        int index = startIndex;
        final EntryIteratorImpl it = new EntryIteratorImpl(index);
        while (true) {
            int offset = 0;
            while (it.next()) {
                if (it.getType() == ZLTextParagraph.Entry.TEXT) {
                    char[] textData = it.getTextData();
                    int textOffset = it.getTextOffset();
                    int textLength = it.getTextLength();
                    for (int pos = ZLSearchUtil.find(textData, textOffset, textLength, pattern); pos != -1;
                         pos = ZLSearchUtil.find(textData, textOffset, textLength, pattern, pos + 1)) {
                        myMarks.add(new ZLTextMark(index, offset + pos, pattern.getLength()));
                        ++count;
                    }
                    offset += textLength;
                }
            }
            if (++index >= endIndex) {
                break;
            }
            it.reset(index);
        }
        return count;
    }

    private HashMap<Integer, ArrayList<ZLTextMark>> mSearchResults = new HashMap<Integer, ArrayList<ZLTextMark>>();

    public final ArrayList<ZLTextMark> search(final String text, Typeface boldTypeface, UpdatableAsyncTaskLoader updateListener) {
        ZLSearchPattern pattern = new ZLSearchPattern(text, true);
        myPrevMarks = new ArrayList<ZLTextMark>();
        mSearchResults.clear();
        myPreviousSearch = text;
        final int startIndex = 0;
        final int endIndex = myParagraphsNumber;
        int index = startIndex;
        final EntryIteratorImpl it = new EntryIteratorImpl(index);
        try {

            MTextBuilder textBuilder = new MTextBuilder(this);

            while (!updateListener.isCanceledInternal()) {
                int offset = 0;
                while (it.next() && !updateListener.isCanceledInternal()) {
                    ZLTextParagraphCursor paragraphCursor = ZLTextParagraphCursor.cursor(this, index);

                    if (it.getType() == ZLTextParagraph.Entry.TEXT) {
                        char[] textData = it.getTextData();
                        int textOffset = it.getTextOffset();
                        int textLength = it.getTextLength();
                        for (int pos = ZLSearchUtil.find(textData, textOffset, textLength, pattern); pos != -1;
                             pos = ZLSearchUtil.find(textData, textOffset, textLength, pattern, pos + 1)) {

                            if (updateListener.isCanceledInternal())
                                return myPrevMarks;

                            int matchElement = paragraphCursor.getElementIndexByOffset(pos);

                            int maxChars = 100 - pattern.getLength();
                            int leftChars = maxChars / 2;
                            int rightChars = maxChars / 2;
                            int startElementIndex = 0;
                            int endElementIndex = matchElement;

                            if (pos - leftChars > 0) {
                                startElementIndex = paragraphCursor.getNearestElementIndexByOffset(pos - leftChars);
                            } else {
                                startElementIndex = paragraphCursor.getNearestElementIndexByOffset(0);
                                rightChars += leftChars - pos;
                            }

                            if (startElementIndex == -1) {
                                startElementIndex = matchElement;
                                rightChars = 100 - pattern.getLength();
                                leftChars = -1;
                            }

                            if (rightChars + pos > textLength) {
                                endElementIndex = paragraphCursor.getNearestElementIndexByOffset(rightChars + pos);
                            } else {
                                endElementIndex = paragraphCursor.getParagraphLength();
                            }

                            if (endElementIndex == -1) {
                                endElementIndex = paragraphCursor.getParagraphLength();
                            }

                            textBuilder.clear();
                            textBuilder.traverse(new ZLTextFixedPosition(index, startElementIndex, 0), new ZLTextFixedPosition(index, endElementIndex, 0), 100);

                            SpannableStringBuilder builder = new SpannableStringBuilder();
                            builder.append("...").append(textBuilder.getText()).append("...");
                            int spanStart = pos - ((ZLTextWord) paragraphCursor.getElement(matchElement)).getParagraphOffset() + 3 + (leftChars != -1 ? ((ZLTextWord) paragraphCursor.getElement(matchElement)).getParagraphOffset() - ((ZLTextWord) paragraphCursor.getElement(startElementIndex)).getParagraphOffset() : 0);
                            builder.setSpan(new TypefaceSpan(boldTypeface), spanStart, spanStart + pattern.getLength(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                            builder.setSpan(new ForegroundColorSpan(Colors.SEARCH_MATCH_TEXT_COLOR), spanStart, spanStart + pattern.getLength(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                            ZLTextMark mark = new ZLTextMark(index, offset + pos, pattern.getLength(), builder);
                            myPrevMarks.add(mark);
                            if (!mSearchResults.containsKey(index)) {
                                mSearchResults.put(index, new ArrayList<ZLTextMark>());
                            }
                            synchronized (mSearchResults) {
                                mSearchResults.get(index).add(mark);
                            }
                            if (updateListener != null && myPrevMarks.size() % 100 == 0) {
                                updateListener.update(myPrevMarks);
                            }
                        }
                        offset += textLength;
                    }
                }

                if (++index >= endIndex) {
                    break;
                }
                it.reset(index);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return myPrevMarks;
    }

    @Override
    public String getPreviousSearchText() {
        return myPreviousSearch;
    }

    @Override
    public boolean canShowSearchResults() {
        return showSearchResults;
    }

    public final List<ZLTextMark> getMarks() {
//        Log.e(TAG, "get marks called, return = " + (myMarks != null && showSearchResults ? myMarks.size() : "null"));
//        return myMarks != null && showSearchResults ? myMarks : Collections.<ZLTextMark>emptyList();
        return Collections.emptyList();
    }

    @Override
    public final List<ZLTextMark> getPreviousMarks() {
        return myPrevMarks != null ? myPrevMarks : Collections.<ZLTextMark>emptyList();
    }

    @Override
    public void setCanShowSearchResults(boolean value) {
        showSearchResults = value;
        if (value)
            myMarks = new ArrayList<ZLTextMark>(myPrevMarks);
    }

    @Override
    public ArrayList<ZLTextMark> getSearchMarks(ZLTextWordCursor startCursor, ZLTextWordCursor endCursor) {
        ArrayList<ZLTextMark> marks = new ArrayList<ZLTextMark>();
        int startParagraph = startCursor.getParagraphIndex();
        int endParagraph = endCursor.getParagraphIndex();
        synchronized (mSearchResults) {
            for (int i = startParagraph; i < endParagraph + 1; i++) {
                if (mSearchResults.containsKey(i)) {
                    ArrayList<ZLTextMark> paragraphMarks = mSearchResults.get(i);
                    if (paragraphMarks != null && paragraphMarks.size() > 0) {
                        marks.addAll(paragraphMarks);
                    }
                }
            }
        }
        return marks;
    }

    public final void removeAllMarks() {
        myMarks = null;
    }

    public final int getParagraphsNumber() {
        return myParagraphsNumber;
    }

    public final ZLTextParagraph getParagraph(int index) {
        final byte kind = myParagraphKinds[index];
        return (kind == ZLTextParagraph.Kind.TEXT_PARAGRAPH) ?
                new ZLTextParagraphImpl(this, index) :
                new ZLTextSpecialParagraphImpl(kind, this, index);
    }

    public final int getTextLength(int index) {
        if (myTextSizes.length == 0) {
            return 0;
        }
        return myTextSizes[Math.max(Math.min(index, myParagraphsNumber - 1), 0)];
    }

    private static int binarySearch(int[] array, int length, int value) {
        int lowIndex = 0;
        int highIndex = length - 1;

        while (lowIndex <= highIndex) {
            int midIndex = (lowIndex + highIndex) >>> 1;
            int midValue = array[midIndex];
            if (midValue > value) {
                highIndex = midIndex - 1;
            } else if (midValue < value) {
                lowIndex = midIndex + 1;
            } else {
                return midIndex;
            }
        }
        return -lowIndex - 1;
    }

    public final int findParagraphByTextLength(int length) {
        int index = binarySearch(myTextSizes, myParagraphsNumber, length);
        if (index >= 0) {
            return index;
        }
        return Math.min(-index - 1, myParagraphsNumber - 1);
    }
}
