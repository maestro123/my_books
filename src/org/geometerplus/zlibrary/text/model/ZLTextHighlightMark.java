package org.geometerplus.zlibrary.text.model;

/**
 * Created by artyom on 9/4/14.
 */
public class ZLTextHighlightMark extends ZLTextMark {

    public static final String TAG = ZLTextHighlightMark.class.getSimpleName();

    private int color;

    public ZLTextHighlightMark(int paragraphIndex, int offset, int length, int color) {
        super(paragraphIndex, offset, length);
    }

    public ZLTextHighlightMark(ZLTextMark mark) {
        super(mark);
    }

    public void save() {
//        BooksDatabase.Instance().
//        Log.e(TAG, "save");
    }

}
