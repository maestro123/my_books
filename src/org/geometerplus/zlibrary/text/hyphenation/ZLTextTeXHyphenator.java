package org.geometerplus.zlibrary.text.hyphenation;

import org.geometerplus.zlibrary.core.filesystem.ZLFile;
import org.geometerplus.zlibrary.core.filesystem.ZLResourceFile;
import org.geometerplus.zlibrary.core.util.ZLMiscUtil;

import java.util.*;

final class ZLTextTeXHyphenator extends ZLTextHyphenator {

    public static final String TAG = ZLTextTeXHyphenator.class.getSimpleName();

    private final HashMap<ZLTextTeXHyphenationPattern, ZLTextTeXHyphenationPattern> myPatternTable =
            new HashMap<ZLTextTeXHyphenationPattern, ZLTextTeXHyphenationPattern>();
    private String myLanguage;

    void addPattern(ZLTextTeXHyphenationPattern pattern) {
        myPatternTable.put(pattern, pattern);
    }

    private List<String> myLanguageCodes;

    public List<String> languageCodes() {
        if (myLanguageCodes == null) {
            final TreeSet<String> codes = new TreeSet<String>();
            final ZLFile patternsFile = ZLResourceFile.createResourceFile("hyphenationPatterns");
            for (ZLFile file : patternsFile.children()) {
                final String name = file.getShortName();
                if (name.endsWith(".pattern")) {
                    codes.add(name.substring(0, name.length() - ".pattern".length()));
                }
            }

            codes.add("zh");
            myLanguageCodes = new ArrayList<String>(codes);
        }

        return Collections.unmodifiableList(myLanguageCodes);
    }

    public void load(final String language) {
        if (ZLMiscUtil.equals(language, myLanguage)) {
            return;
        }
        myLanguage = language;
        unload();

        if (language != null) {
            new ZLTextHyphenationReader(this).read(ZLResourceFile.createResourceFile(
                    "hyphenationPatterns/" + language + ".pattern"
            ));
        }
    }

    public void unload() {
        myPatternTable.clear();
    }

    public void hyphenate(char[] stringToHyphenate, boolean[] mask, int length) {
        if (myPatternTable.isEmpty()) {
            for (int i = 0; i < length - 1; i++) {
                mask[i] = false;
            }
            return;
        }

        byte[] values = new byte[length + 1];

        final HashMap<ZLTextTeXHyphenationPattern, ZLTextTeXHyphenationPattern> table = myPatternTable;
        ZLTextTeXHyphenationPattern pattern =
                new ZLTextTeXHyphenationPattern(stringToHyphenate, 0, length, false);
        for (int offset = 0; offset < length - 1; offset++) {
            int len = length - offset + 1;
            pattern.update(stringToHyphenate, offset, len - 1);
            while (--len > 0) {
                pattern.myLength = len;
                pattern.myHashCode = 0;
                ZLTextTeXHyphenationPattern toApply =
                        table.get(pattern);
                if (toApply != null) {
                    toApply.apply(values, offset);
                }
            }
        }

        for (int i = 0; i < length - 1; i++) {
            mask[i] = (values[i + 1] % 2) == 1;
        }
    }
}
