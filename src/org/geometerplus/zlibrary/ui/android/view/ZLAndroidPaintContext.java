/*
 * Copyright (C) 2007-2011 Geometer Plus <contact@geometerplus.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

package org.geometerplus.zlibrary.ui.android.view;

import android.content.Context;
import android.graphics.*;
import android.graphics.drawable.Drawable;
import android.text.format.DateFormat;
import com.prestigio.android.ereader.read.maestro.MTextOptions;
import com.prestigio.android.ereader.read.maestro.MTextView;
import com.prestigio.android.myprestigio.utils.Typefacer;
import org.geometerplus.zlibrary.core.filesystem.ZLFile;
import org.geometerplus.zlibrary.core.fonts.FontEntry;
import org.geometerplus.zlibrary.core.image.ZLImageData;
import org.geometerplus.zlibrary.core.util.ZLColor;
import org.geometerplus.zlibrary.core.view.ZLPaintContext;
import org.geometerplus.zlibrary.ui.android.image.ZLAndroidImageData;
import org.geometerplus.zlibrary.ui.android.util.ZLAndroidColorUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public final class ZLAndroidPaintContext extends ZLPaintContext {

    public static final String TAG = ZLAndroidPaintContext.class.getSimpleName();

    private final Paint myTextPaint = new Paint();
    private final Paint myLinePaint = new Paint();
    private final Paint myFillPaint = new Paint();
    private final Paint myOutlinePaint = new Paint();
    private final Paint mStatusBarPaint = new Paint();

    private Canvas myCanvas = new Canvas();
    private Geometry mGeometry;
    private MTextOptions mOptions;
    private ZLColor myBackgroundColor = new ZLColor(0, 0, 0);
    private HashMap<String, Typeface[]> myTypefaces = new HashMap<String, Typeface[]>();
    private int mBottomBarHeight;

    public static class Geometry {
        Size ScreenSize;
        Size AreaSize;
        int LeftMargin;
        int TopMargin;

        public Geometry(int screenWidth, int screenHeight, int width, int height, int leftMargin, int topMargin) {
            ScreenSize = new Size(screenWidth, screenHeight);
            AreaSize = new Size(width, height);
            LeftMargin = leftMargin;
            TopMargin = topMargin;
        }
    }

    public ZLAndroidPaintContext(int bottomBarHeight) {
        mBottomBarHeight = bottomBarHeight;

        myTextPaint.setLinearText(false);
        myTextPaint.setAntiAlias(true);
        myTextPaint.setSubpixelText(false);

        myLinePaint.setStyle(Paint.Style.STROKE);

        myFillPaint.setAntiAlias(true);

        myOutlinePaint.setColor(Color.rgb(255, 127, 0));
        myOutlinePaint.setAntiAlias(true);
        myOutlinePaint.setDither(true);
        myOutlinePaint.setStrokeWidth(4);
        myOutlinePaint.setStyle(Paint.Style.STROKE);
        myOutlinePaint.setPathEffect(new CornerPathEffect(5));
        myOutlinePaint.setMaskFilter(new EmbossMaskFilter(new float[]{1, 1, 1}, .4f, 6f, 3.5f));

        mStatusBarPaint.setTypeface(Typefacer.rRegular);
        mStatusBarPaint.setAntiAlias(true);
        mStatusBarPaint.setTextSize(MTextOptions.getInstance().applyToDpi(14));

    }

    public void setTextOptions(MTextOptions options) {
        mOptions = options;
    }

    public void setGeometry(Geometry geometry) {
        mGeometry = geometry;
    }

    public ZLAndroidPaintContext setup(Bitmap bitmap, int width, int height) {
        myCanvas.setBitmap(bitmap);
        mGeometry.ScreenSize = new Size(width, height);
        return this;
    }

    public ZLAndroidPaintContext setup(Canvas canvas, int width, int height) {
        myCanvas = canvas;
        mGeometry.ScreenSize = new Size(width, height);
        return this;
    }

    public void setupStatusBarPaint() {
        mStatusBarPaint.setColor(mOptions.getColorProfile().FooterTextColorOption.getValue().toRGB());
        mStatusBarPaint.setAlpha(145);
    }

    private static ZLFile ourWallpaperFile;
    private static Bitmap ourWallpaper;
    private static FillMode ourFillMode;

    @Override
    public void clear(ZLFile wallpaperFile, FillMode mode) {
        if (!wallpaperFile.equals(ourWallpaperFile) || mode != ourFillMode) {
            ourWallpaperFile = wallpaperFile;
            ourFillMode = mode;
            ourWallpaper = null;
            try {
                final Bitmap fileBitmap =
                        BitmapFactory.decodeStream(wallpaperFile.getInputStream());
                switch (mode) {
                    default:
                        ourWallpaper = fileBitmap;
                        break;
                    case tileMirror: {
                        final int w = fileBitmap.getWidth();
                        final int h = fileBitmap.getHeight();
                        final Bitmap wallpaper = Bitmap.createBitmap(2 * w, 2 * h, fileBitmap.getConfig());
                        final Canvas wallpaperCanvas = new Canvas(wallpaper);
                        final Paint wallpaperPaint = new Paint();

                        Matrix m = new Matrix();
                        wallpaperCanvas.drawBitmap(fileBitmap, m, wallpaperPaint);
                        m.preScale(-1, 1);
                        m.postTranslate(2 * w, 0);
                        wallpaperCanvas.drawBitmap(fileBitmap, m, wallpaperPaint);
                        m.preScale(1, -1);
                        m.postTranslate(0, 2 * h);
                        wallpaperCanvas.drawBitmap(fileBitmap, m, wallpaperPaint);
                        m.preScale(-1, 1);
                        m.postTranslate(-2 * w, 0);
                        wallpaperCanvas.drawBitmap(fileBitmap, m, wallpaperPaint);
                        ourWallpaper = wallpaper;
                        break;
                    }
                }
            } catch (Throwable t) {
                t.printStackTrace();
            }
        }
        if (ourWallpaper != null) {
            myBackgroundColor = ZLAndroidColorUtil.getAverageColor(ourWallpaper);
            final int w = ourWallpaper.getWidth();
            final int h = ourWallpaper.getHeight();
            final Geometry g = mGeometry;
            switch (mode) {
                case fullscreen: {
                    final Matrix m = new Matrix();
                    m.preScale(1f * g.ScreenSize.Width / w, 1f * g.ScreenSize.Height / h);
                    m.postTranslate(-g.LeftMargin, -g.TopMargin);
                    myCanvas.drawBitmap(ourWallpaper, m, myFillPaint);
                    break;
                }
                case stretch: {
                    final Matrix m = new Matrix();
                    final float sw = 1f * g.ScreenSize.Width / w;
                    final float sh = 1f * g.ScreenSize.Height / h;
                    final float scale;
                    float dx = g.LeftMargin;
                    float dy = g.TopMargin;
                    if (sw < sh) {
                        scale = sh;
                        dx += (scale * w - g.ScreenSize.Width) / 2;
                    } else {
                        scale = sw;
                        dy += (scale * h - g.ScreenSize.Height) / 2;
                    }
                    m.preScale(scale, scale);
                    m.postTranslate(-dx, -dy);
                    myCanvas.drawBitmap(ourWallpaper, m, myFillPaint);
                    break;
                }
                case tileVertically: {
                    final Matrix m = new Matrix();
                    final int dx = g.LeftMargin;
                    final int dy = g.TopMargin - g.TopMargin / h * h;
                    m.preScale(1f * g.ScreenSize.Width / w, 1);
                    m.postTranslate(-dx, -dy);
                    for (int ch = g.AreaSize.Height + dy; ch > 0; ch -= h) {
                        myCanvas.drawBitmap(ourWallpaper, m, myFillPaint);
                        m.postTranslate(0, h);
                    }
                    break;
                }
                case tileHorizontally: {
                    final Matrix m = new Matrix();
                    final int dx = g.LeftMargin - g.LeftMargin / w * w;
                    final int dy = g.TopMargin;
                    m.preScale(1, 1f * g.ScreenSize.Height / h);
                    m.postTranslate(-dx, -dy);
                    for (int cw = g.AreaSize.Width + dx; cw > 0; cw -= w) {
                        myCanvas.drawBitmap(ourWallpaper, m, myFillPaint);
                        m.postTranslate(w, 0);
                    }
                    break;
                }
                case tile:
                case tileMirror: {
                    final int fullw = g.ScreenSize.Width;
                    final int fullh = g.ScreenSize.Height;
                    for (int cw = 0; cw < fullw; cw += w) {
                        for (int ch = 0; ch < fullh; ch += h) {
                            myCanvas.drawBitmap(ourWallpaper, cw, ch, myFillPaint);
                        }
                    }
                    break;
                }
            }
        } else {
            clear(new ZLColor(128, 128, 128));
        }
    }


    @Override
    public void clear(ZLColor color) {
        myBackgroundColor = color;
        myFillPaint.setColor(ZLAndroidColorUtil.rgb(color));
        myCanvas.drawRect(0, 0, mGeometry.ScreenSize.Width, mGeometry.ScreenSize.Height, myFillPaint);
    }

    @Override
    public ZLColor getBackgroundColor() {
        return myBackgroundColor;
    }

    @Override
    protected void setFontInternal(List<FontEntry> entries, int size, boolean bold, boolean italic, boolean underline, boolean strikeThrough) {
        Typeface typeface = null;
        for (FontEntry e : entries) {
            typeface = AndroidFontUtil.typeface(e, bold, italic);
            if (typeface != null) {
                break;
            }
        }
        myTextPaint.setTypeface(typeface);
        myTextPaint.setTextSize(size);
        myTextPaint.setUnderlineText(underline);
        myTextPaint.setStrikeThruText(strikeThrough);
    }

    public void fillPolygon(int[] xs, int ys[]) {
        final Path path = new Path();
        final int last = xs.length - 1;
        path.moveTo(xs[last], ys[last]);
        for (int i = 0; i <= last; ++i) {
            path.lineTo(xs[i], ys[i]);
        }
        myCanvas.drawPath(path, myFillPaint);
    }

    public void drawPolygonalLine(int[] xs, int ys[]) {
        final Path path = new Path();
        final int last = xs.length - 1;
        path.moveTo(xs[last], ys[last]);
        for (int i = 0; i <= last; ++i) {
            path.lineTo(xs[i], ys[i]);
        }
        myCanvas.drawPath(path, myLinePaint);
    }

    public void drawOutline(int[] xs, int ys[]) {
        final int last = xs.length - 1;
        int xStart = (xs[0] + xs[last]) / 2;
        int yStart = (ys[0] + ys[last]) / 2;
        int xEnd = xStart;
        int yEnd = yStart;
        if (xs[0] != xs[last]) {
            if (xs[0] > xs[last]) {
                xStart -= 5;
                xEnd += 5;
            } else {
                xStart += 5;
                xEnd -= 5;
            }
        } else {
            if (ys[0] > ys[last]) {
                yStart -= 5;
                yEnd += 5;
            } else {
                yStart += 5;
                yEnd -= 5;
            }
        }

        final Path path = new Path();
        path.moveTo(xStart, yStart);
        for (int i = 0; i <= last; ++i) {
            path.lineTo(xs[i], ys[i]);
        }
        path.lineTo(xEnd, yEnd);
        myCanvas.drawPath(path, myOutlinePaint);
    }

    @Override
    public void setTextColor(ZLColor color) {
        myTextPaint.setColor(ZLAndroidColorUtil.rgb(color));
    }

    @Override
    public void setLineColor(ZLColor color, int style) {
        // TODO: use style
        myLinePaint.setColor(ZLAndroidColorUtil.rgb(color));
    }

    @Override
    public void setLineWidth(int width) {
        myLinePaint.setStrokeWidth(width);
    }

    @Override
    public void setFillColor(ZLColor color, int alpha, int style) {
        // TODO: use style
        myFillPaint.setColor(ZLAndroidColorUtil.rgba(color, alpha));
    }

    @Override
    public void setFillAlpha(int alpha) {
        myFillPaint.setAlpha(alpha);
    }

    @Override
    public int getWidth() {
        return mGeometry.ScreenSize.Width;
    }

    @Override
    public int getHeight() {
        return mGeometry.ScreenSize.Height;
    }

    @Override
    public int getAreaWidth() {
        return mGeometry.AreaSize.Width;
    }

    @Override
    public int getAreaHeight() {
        return mGeometry.AreaSize.Height;
    }

    public int getStringWidth(char[] string, int offset, int length) {
        return getStringWidth(string, offset, length, myTextPaint);
    }

    public final int getStringWidth(String string, Paint paint) {
        return getStringWidth(string.toCharArray(), 0, string.length(), paint);
    }

    public int getStringWidth(char[] string, int offset, int length, Paint paint) {
        return (int) (paint.measureText(new String(string, offset, length)) + 0.5f);
    }

    protected int getSpaceWidthInternal() {
        return (int) (myTextPaint.measureText(" ", 0, 1) + 0.5f);
    }

    protected int getStringHeightInternal() {
        return (int) (myTextPaint.getTextSize() + 0.5f);
    }

    protected int getDescentInternal() {
        return (int) (myTextPaint.descent() + 0.5f);
    }

    public void drawString(int x, int y, char[] string, int offset, int length) {
        myCanvas.drawText(string, offset, length, x, y, myTextPaint);
    }

    public void drawBookTitle(String title) {
        myTextPaint.setColor(Color.DKGRAY);
        final int stringHeight = getStringHeight();
        final int center = getWidth() / 2 - getStringWidth(title) / 2;
        final int fromTop = stringHeight;
        myCanvas.drawText(title, center, fromTop, myTextPaint);
    }

    final float getTextHeight(Paint paint) {
        Rect rect = new Rect();
        paint.getTextBounds(new char[]{'A'}, 0, 1, rect);
        return rect.height();
    }

    public void drawBookProgress(MTextView.PagePosition position, Context context) {
        final String value = MTextOptions.getInstance().StatusBarReadDisplayType.getValue() == MTextOptions.STATUS_BAR_READ_DISPLAY_TYPE.PERCENT ? position.asPercent() : position.asString(context);
        final int center = (int) (getWidth() / 2 - mStatusBarPaint.measureText(value) / 2);
        final int fromTop = (int) (getHeight() - (int) (mOptions.applyToDpi(mOptions.StatusBarHeightOption.getValue())) / 2 - mOptions.applyToDpi(4));
        myCanvas.drawText(value, center, fromTop + getTextHeight(mStatusBarPaint) / 2, mStatusBarPaint);
    }

    public void drawBatteryLevel(int level, Drawable icon) {
        final String value = new StringBuilder().append(level).append(" ").append("%").toString();
        final int fromTop = (int) (getHeight() - (int) (mOptions.applyToDpi(mOptions.StatusBarHeightOption.getValue())) / 2 - mOptions.applyToDpi(4));
        final int leftMargin = mOptions.vsDPI(mOptions.LeftMarginOption);
        myCanvas.save();
        myCanvas.translate(leftMargin, fromTop - icon.getIntrinsicHeight() / 2);
        icon.draw(myCanvas);
        myCanvas.restore();
        myCanvas.drawText(value, leftMargin + icon.getIntrinsicWidth(),
                fromTop + getTextHeight(mStatusBarPaint) / 2, mStatusBarPaint);
    }

    public void drawTime(Context context, Drawable icon) {
        final String value = DateFormat.getTimeFormat(context).format(new Date());
        final int fromTop = (int) (getHeight() - (int) (mOptions.applyToDpi(mOptions.StatusBarHeightOption.getValue())) / 2 - mOptions.applyToDpi(4));
        final int leftMargin = getWidth() - mOptions.vsDPI(mOptions.RightMarginOption);
        myCanvas.save();
        myCanvas.translate(leftMargin - icon.getIntrinsicWidth(), fromTop - icon.getIntrinsicHeight() / 2);
        icon.draw(myCanvas);
        myCanvas.restore();
        myCanvas.drawText(value, leftMargin - mStatusBarPaint.measureText(value) - icon.getIntrinsicWidth(),
                fromTop + getTextHeight(mStatusBarPaint) / 2, mStatusBarPaint);
    }

    @Override
    public void drawImage(int x, int y, ZLImageData imageData, Size maxSize, ScalingType scaling) {
        final Bitmap bitmap = ((ZLAndroidImageData) imageData).getBitmap(maxSize, scaling);
        if (bitmap != null && !bitmap.isRecycled()) {
            myCanvas.drawBitmap(bitmap, x, y - bitmap.getHeight(), myFillPaint);
            myFillPaint.setXfermode(null);
        }
    }

    public void drawImage(int x, int y, Bitmap bitmap) {
        myCanvas.drawBitmap(bitmap, x, y, myFillPaint);
    }

    public void drawDrawable(int x, int y, Drawable drawable) {
        myCanvas.save();
        myCanvas.translate(x, y);
        drawable.draw(myCanvas);
        myCanvas.restore();
    }

    public void drawLine(int x0, int y0, int x1, int y1) {
        final Canvas canvas = myCanvas;
        final Paint paint = myLinePaint;
        paint.setAntiAlias(false);
        canvas.drawLine(x0, y0, x1, y1, paint);
        canvas.drawPoint(x0, y0, paint);
        canvas.drawPoint(x1, y1, paint);
        paint.setAntiAlias(true);
    }

    public void fillRectangle(int x0, int y0, int x1, int y1, Xfermode xfermode) {
        if (x1 < x0) {
            int swap = x1;
            x1 = x0;
            x0 = swap;
        }
        if (y1 < y0) {
            int swap = y1;
            y1 = y0;
            y0 = swap;
        }
        if (xfermode != null)
            myFillPaint.setXfermode(xfermode);
        myCanvas.drawRect(x0, y0, x1 + 1, y1 + 1, myFillPaint);
        if (xfermode != null)
            myFillPaint.setXfermode(null);
    }

    public void fillRectangle(int x0, int y0, int x1, int y1) {
        fillRectangle(x0, y0, x1, y1, null);
    }

    public void drawFilledCircle(int x, int y, float r) {
        myCanvas.save();
        myCanvas.drawCircle(x, y, r, myFillPaint);
        myCanvas.restore();
    }

    public String realFontFamilyName(String fontFamily) {
        return AndroidFontUtil.realFontFamilyName(fontFamily);
    }

    protected void fillFamiliesList(ArrayList<String> families) {
        AndroidFontUtil.fillFamiliesList(families);
    }

    @Override
    public Size imageSize(ZLImageData image, Size maxSize, ScalingType scaling) {
        return ((ZLAndroidImageData) image).getBitmapSize(maxSize, scaling);
    }

    @Override
    public Canvas getCanvas() {
        return myCanvas;
    }
}
