/*
 * Copyright (C) 2007-2011 Geometer Plus <contact@geometerplus.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

package org.geometerplus.zlibrary.ui.android.library;

import android.app.Application;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.telephony.TelephonyManager;
import android.text.format.DateFormat;
import android.util.DisplayMetrics;
import com.prestigio.android.ereader.read.ShelfBaseReadActivity;
import com.prestigio.android.ereader.read.ShelfReadSettingsHolder;
import com.prestigio.ereader.R;
import org.geometerplus.zlibrary.core.filesystem.ZLFile;
import org.geometerplus.zlibrary.core.filesystem.ZLResourceFile;
import org.geometerplus.zlibrary.core.library.ZLibrary;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

public final class ZLAndroidLibrary extends ZLibrary {
    private volatile ShelfBaseReadActivity myActivity;
    private final Application myApplication;

    ZLAndroidLibrary(Application application) {
        myApplication = application;
    }

    public void setActivity(ShelfBaseReadActivity activity) {
        myActivity = activity;
    }

    public AssetManager getAssets() {
        return myApplication.getAssets();
    }

    public void finish() {
        if ((myActivity != null) && !myActivity.isFinishing()) {
            myActivity.finish();
        }
    }

    public ShelfBaseReadActivity getActivity() {
        return myActivity;
    }

    @Override
    public ZLResourceFile createResourceFile(String path) {
        return new AndroidAssetsFile(path);
    }

    @Override
    public ZLResourceFile createResourceFile(ZLResourceFile parent, String name) {
        return new AndroidAssetsFile((AndroidAssetsFile) parent, name);
    }

    @Override
    public String getVersionName() {
        try {
            return myApplication.getPackageManager().getPackageInfo(myApplication.getPackageName(), 0).versionName;
        } catch (Exception e) {
            return "";
        }
    }

    @Override
    public int getDPIFactor() {
        return isTablet() ? 2 : super.getDPIFactor();
    }

    @Override
    public String getCurrentTimeString() {
        return DateFormat.getTimeFormat(myApplication.getApplicationContext()).format(new Date());
    }

    @Override
    public void setScreenBrightness(int percent) {
        if (myActivity != null) {
            myActivity.setScreenBrightnessInternal(percent);
        }
    }

    @Override
    public int getScreenBrightness() {
        return (myActivity != null) ? ShelfReadSettingsHolder.getInstance().getBrightness() : 0;
    }

    private Float mDensity = null;

    @Override
    public int getDisplayDPI() {
        if (mDensity == null) {
            DisplayMetrics metrics = ZLAndroidApplication.Instance().getResources().getDisplayMetrics();
            mDensity = metrics.density;
        }
        return (int) (160 * mDensity);
    }

    @Override
    public boolean isTablet() {
        return myActivity != null ? myActivity.getResources().getBoolean(R.bool.isBig) : super.isTablet();
    }

    @Override
    public Collection<String> defaultLanguageCodes() {
        final TreeSet<String> set = new TreeSet<String>();
        set.add(Locale.getDefault().getLanguage());
        final TelephonyManager manager = (TelephonyManager) myApplication.getSystemService(Context.TELEPHONY_SERVICE);
        if (manager != null) {
            final String country0 = manager.getSimCountryIso().toLowerCase();
            final String country1 = manager.getNetworkCountryIso().toLowerCase();
            for (Locale locale : Locale.getAvailableLocales()) {
                final String country = locale.getCountry().toLowerCase();
                if (country != null && country.length() > 0 &&
                        (country.equals(country0) || country.equals(country1))) {
                    set.add(locale.getLanguage());
                }
            }
            if ("ru".equals(country0) || "ru".equals(country1)) {
                set.add("ru");
            } else if ("by".equals(country0) || "by".equals(country1)) {
                set.add("ru");
            } else if ("ua".equals(country0) || "ua".equals(country1)) {
                set.add("ru");
            }
        }
        set.add("multi");
        return set;
    }

    private interface StreamStatus {
        int UNKNOWN = -1;
        int NULL = 0;
        int OK = 1;
        int EXCEPTION = 2;
    }

    private static final HashMap<String, ArrayList<ZLFile>> mCache_directoryEntries = new HashMap<>();
    private static final HashMap<String, Boolean> mCache_exists = new HashMap<>();

    //TODO: do not touch this  class!!!
    private final class AndroidAssetsFile extends ZLResourceFile {
        private final AndroidAssetsFile myParent;

        AndroidAssetsFile(AndroidAssetsFile parent, String name) {
            super(parent.getPath().length() == 0 ? name : parent.getPath() + '/' + name);
            myParent = parent;
        }

        AndroidAssetsFile(String path) {
            super(path);
            if (path.length() == 0) {
                myParent = null;
            } else {
                final int index = path.lastIndexOf('/');
                myParent = new AndroidAssetsFile(index >= 0 ? path.substring(0, path.lastIndexOf('/')) : "");
            }
        }

        @Override
        protected List<ZLFile> directoryEntries() {
            try {
                synchronized (mCache_directoryEntries) {
                    if (mCache_directoryEntries.containsKey(getPath())) {
                        return mCache_directoryEntries.get(getPath());
                    }
                    String[] names = myApplication.getAssets().list(getPath());
                    if (names != null && names.length != 0) {
                        ArrayList<ZLFile> files = new ArrayList<ZLFile>(names.length);
                        for (String n : names) {
                            files.add(new AndroidAssetsFile(this, n));
                        }
                        mCache_directoryEntries.put(getPath(), files);
                        return files;
                    } else {
                        mCache_directoryEntries.put(getPath(), new ArrayList<ZLFile>());
                    }
                }
            } catch (IOException e) {
            }
            return Collections.emptyList();
        }

        private int myStreamStatus = StreamStatus.UNKNOWN;
        private int streamStatus() {
            if (myStreamStatus == StreamStatus.UNKNOWN) {
                try {
                    final InputStream stream = myApplication.getAssets().open(getPath());
                    if (stream == null) {
                        myStreamStatus = StreamStatus.NULL;
                    } else {
                        stream.close();
                        myStreamStatus = StreamStatus.OK;
                    }
                } catch (IOException e) {
                    myStreamStatus = StreamStatus.EXCEPTION;
                }
            }
            return myStreamStatus;
        }

        @Override
        public boolean isDirectory() {
            return streamStatus() != StreamStatus.OK;
        }

        @Override
        public boolean exists() {
            synchronized (mCache_exists) {
                if (mCache_exists.containsKey(getPath())) {
                    return mCache_exists.get(getPath());
                }

                // not used???
                if (streamStatus() == StreamStatus.OK) {
                    mCache_exists.put(getPath(), true);
                    return true;
                }
                final String path = getPath();
                if ("".equals(path)) {
                    mCache_exists.put(getPath(), true);
                    return true;
                }
                try {
                    String[] names = myApplication.getAssets().list(getPath());
                    if (names != null && names.length != 0) {
                        // directory exists
                        mCache_exists.put(getPath(), true);
                        return true;
                    }
                } catch (IOException e) {
                }
                mCache_exists.put(getPath(), false);
                return false;
            }

        }

        private long mySize = -1;
        @Override
        public long size() {
            if (mySize == -1) {
                mySize = sizeInternal();
            }
            return mySize;
        }

        private long sizeInternal() {
            try {
                AssetFileDescriptor descriptor = myApplication.getAssets().openFd(getPath());
                // for some files (archives, crt) descriptor cannot be opened
                if (descriptor == null) {
                    return sizeSlow();
                }
                long length = descriptor.getLength();
                descriptor.close();
                return length;
            } catch (IOException e) {
                return sizeSlow();
            }
        }

        private long sizeSlow() {
            try {
                final InputStream stream = getInputStream();
                if (stream == null) {
                    return 0;
                }
                long size = 0;
                final long step = 1024 * 1024;
                while (true) {
                    // TODO: does skip work as expected for these files?
                    long offset = stream.skip(step);
                    size += offset;
                    if (offset < step) {
                        break;
                    }
                }
                return size;
            } catch (IOException e) {
                return 0;
            }
        }

        @Override
        public InputStream getInputStream() throws IOException {
            return myApplication.getAssets().open(getPath());
        }

        @Override
        public ZLFile getParent() {
            return myParent;
        }
    }
}
