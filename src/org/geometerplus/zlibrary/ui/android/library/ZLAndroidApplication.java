/*
 * Copyright (C) 2007-2011 Geometer Plus <contact@geometerplus.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

package org.geometerplus.zlibrary.ui.android.library;

import android.app.*;
import android.content.*;
import android.net.Uri;
import android.os.*;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import com.dream.android.mim.MIM;
import com.dream.android.mim.MIMDiskCache;
import com.dream.android.mim.MIMInternetMaker;
import com.dream.android.mim.MIMManager;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.ivengo.ads.AdManager;
import com.prestigio.android.accountlib.MHelper;
import com.prestigio.android.ereader.billing.IabHelper;
import com.prestigio.android.ereader.billing.IabResult;
import com.prestigio.android.ereader.billing.Inventory;
import com.prestigio.android.ereader.read.ShelfReadSettingsHolder;
import com.prestigio.android.ereader.read.maestro.AudioBookRenderer;
import com.prestigio.android.ereader.read.maestro.MTextOptions;
import com.prestigio.android.ereader.shelf.BookHelper;
import com.prestigio.android.ereader.shelf.MainShelfActivity;
import com.prestigio.android.ereader.shelf.service.EreaderShelfService;
import com.prestigio.android.ereader.utils.AdHelper;
import com.prestigio.android.ereader.utils.ReaderLocalCopyGetter;
import com.prestigio.android.ereader.utils.ThemeHolder;
import com.prestigio.android.myprestigio.MyPrestigioApplication;
import com.prestigio.android.myprestigio.diffs.MyPrestigioHelper;
import com.prestigio.android.myprestigio.utils.Utils;
import com.prestigio.ereader.R;
import com.prestigio.ereader.Sql.SqlModel;
import com.prestigio.ereader.Sql.table.TableCollections;
import maestro.support.v1.svg.SVGHelper;
import org.geometerplus.android.AdobeSDKWrapper.DebugLog;
import org.geometerplus.android.fbreader.library.SQLiteBooksDatabase;
import org.geometerplus.android.fbreader.notification.ServiceNotification;
import org.geometerplus.android.util.StorageInfo;
import org.geometerplus.fbreader.Paths;
import org.geometerplus.zlibrary.core.options.ZLBooleanOption;
import org.geometerplus.zlibrary.core.sqliteconfig.ZLSQLiteConfig;
import org.geometerplus.zlibrary.ui.android.image.ZLAndroidImageManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicBoolean;

public class ZLAndroidApplication extends MyPrestigioApplication {

    public static final String TAG = ZLAndroidApplication.class.getSimpleName();

    private static ZLAndroidApplication ourApplication;
    public final ZLBooleanOption ShowStatusBarOption = new ZLBooleanOption("LookNFeel", "ShowStatusBar", hasNoHardwareMenuButton());
    protected volatile Activity mShelfActivity = null;
    private EreaderShelfService.LocalBinder mLibraryService;
    //private final AtomicBoolean mLibraryServiceLock = new AtomicBoolean(false);
    private volatile EreaderShelfService mShelfService;
    private final Object mShelfServiceLock = new Object();
    private IabHelper mIabHelper;

    private SVGHelper.SVGHolder baseSVGHolder;

    public enum TrackerName {
        APP_TRACKER, // Tracker used only in this app.
        //GLOBAL_TRACKER, // Tracker used by all the apps from a company. eg: roll-up tracking.
        //ECOMMERCE_TRACKER, // Tracker used by all ecommerce transactions from a company.
    }

    HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();

    public ZLAndroidApplication() {
        ourApplication = this;
    }

    public static ZLAndroidApplication Instance() {
        return ourApplication;
    }

    private ArrayList<FileObserver> mObservers = new ArrayList<FileObserver>();

    // fix for android 4.4, android run GC for FileObserver in LibraryService.mFilesObserver | DO NOT DELETE!
    public void addObserver(FileObserver observer) {
        mObservers.add(observer);
    }

    public void removeObserver(FileObserver observer) {
        mObservers.remove(observer);
    }

    //public void setLibraryService(LibraryService.BooksLibraryImplementation _service) {
    //mLibraryService = _service;
    //}

    private float mBatteryLevel;

    private final BroadcastReceiver mBatteryInfoReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            try {
                mBatteryLevel = intent.getIntExtra("level", 100);
            } catch (Exception ignored) {
            }
        }
    };

    private boolean hasNoHardwareMenuButton() {
        return (Build.DISPLAY != null && Build.DISPLAY.contains("simenxie"))
                || (Build.MODEL != null && "PD_Novel".equals(Build.MODEL))
                || (Build.MODEL != null && Build.MODEL
                .contains(Paths.PRESTIGIO_DEVICE_WITH_STRANGE_MOUNT));
    }

    private final Object mSyncCurrentActivity = new Object();
    private volatile Activity mCurrentActivity = null;

    public void setCurrentActivity(Activity activity) {
        synchronized (mSyncCurrentActivity) {
            mCurrentActivity = activity;
            mSyncCurrentActivity.notifyAll();
        }
    }

    public Activity getCurrentActivity() {
        synchronized (mSyncCurrentActivity) {
            if (mCurrentActivity == null) {
                try {
                    mSyncCurrentActivity.wait(1000);
                } catch (InterruptedException e) {
                    //break;
                }
                //return mCurrentActivity;
            }
            return mCurrentActivity;
        }
    }

    private APP_STATE mCurrentAppState;

    public enum APP_STATE {
        HOME, READING
    }

    @Override
    public void onCreate() {
        super.onCreate();
        ThemeHolder.getInstance().load(getApplicationContext(), com.prestigio.android.ereader.utils.Utils.getThemePath(this));
        MyPrestigioHelper.getInstance().setLocalCopyGetter(new ReaderLocalCopyGetter());
        SqlModel.init(this);
        ShelfReadSettingsHolder.getInstance().initialize(this);
        if (getCacheDir() == null) {
            MIMDiskCache.getInstance().init(getExternalCacheDir().getPath());
        } else {
            MIMDiskCache.getInstance().init(getCacheDir().getPath());
        }

        baseSVGHolder = new SVGHelper.SVGHolder(getResources());
        AdHelper.getInstance().init(getApplicationContext());
        AdManager.getInstance().initialize(getApplicationContext());
//        AdHelper.getInstance().prepareAd(AdHelper.READ_FULL_SCREEN_AD);
//        AdHelper.getInstance().prepareAd(AdHelper.STORE_DOWNLOAD_FULL_SCREEN_AD);

        /**********************************/
        /* Check if database is available */
        boolean allOk = TableCollections.getInstance().testDatabase();
        if (!allOk) {
            //ToastMaker.getAndShowErrorToast(this, "Internal database error, please reinstall application from market!");
            stopApplication("Internal database error, please reinstall application from market!");
        }
        /**********************************/
        allOk = SQLiteBooksDatabase.testDatabase(this);
        if (!allOk) {
            //ToastMaker.getAndShowErrorToast(this, "Internal database error, please reinstall application from market!");
            stopApplication("Internal database error, please reinstall application from market!");
        }
        /**********************************/

        try {
            new ZLSQLiteConfig(this);
        } catch (Exception e) {
            DebugLog.e(e.getClass().toString(), e.getLocalizedMessage());
            // TODO: handle exception
        }
        new ZLAndroidImageManager();
        new ZLAndroidLibrary(this);
        MTextOptions.getInstance().init(this);
        SVGHelper.init(this);
        StorageInfo.getInstance().init();
        BookHelper.getInstance().prepare(getApplicationContext());

        //Intent serviceIntent = new Intent(this, LibraryService.class);
        //serviceIntent.putExtra("path", Paths.BooksDirectoryOption().getValue());
        //startService(serviceIntent);

        DebugLog.e("ZLAndroidApplication", "onCreate 1");
        //Intent i = new Intent(this, EreaderShelfService.class);
        //startService(i);
        //bindShelfService();

        //DebugLog.e("ZLAndroidApplication", "onCreate 2");
        //i = new Intent(this, LibraryService.class);
        //startService(i);

        //bindLibraryService();
        //bindShelfService();

        Intent i = new Intent(this, ServiceNotification.class);
        startService(i);

        MHelper.getInstance().initialize(this);

        if (DebugLog.mLoggingEnabled) {
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                    .detectCustomSlowCalls()
                    .detectDiskReads()
                    .detectDiskWrites()
                    .detectNetwork()
                    .penaltyLog()
                    .build());
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                    .detectLeakedSqlLiteObjects()
                    .penaltyLog()
                    .penaltyDeath()
                    .build());
        }

        registerReceiver(mBatteryInfoReceiver, new IntentFilter(new IntentFilter(Intent.ACTION_BATTERY_CHANGED)));

        MIM mim = new MIM(getApplicationContext())
                .setThreadCount(3)
                .maker(new MIMInternetMaker(false)).scaleToFit(true)
                .setDiskCache(MIMDiskCache.getInstance().init(Paths.cacheDirectory()))
                .cacheAnimationEnable(false)
                .size(getResources().getDimensionPixelSize(com.prestigio.android.myprestigio.R.dimen.store_image_width),
                        getResources().getDimensionPixelSize(com.prestigio.android.myprestigio.R.dimen.store_image_height));

        MIMManager.getInstance().addMIM(Utils.MIM_STORE_COVERS, mim);

        mIabHelper = new IabHelper(this, "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlSX0+wcQ+B8GXMZ3JvPXGZPwsVQjGBUFkijocXT0Vv8y5Y0kcMW1zzfsKUf/QuHZAmmoG8+JLyz/PAZv+2U8OHtWZEwWRnnAbq8JhCMdUX1hyZc8vF2IX4uZD+EGwsBFtP0WO3y9ibtVpQqAc6BCyNGdcP4bejFch6LX7SCpjbhXiunyhK6eqfAAm0E+mqKGKQAAym/WZSe0pocirermWA5slOgO6oiK3II9pVy8TPCF2LWokpoKoDD0S64Zr9L1+hbpaYNzinOSvDdGDaKQZ9c8OdGz/Y0EjmVaF1MQ5m4VQa9AwT6hSAf7k/DGnjjn/+Qjld4kMFf0lDKi8Gi8nQIDAQAB");
        mIabHelper.enableDebugLogging(false);
        mIabHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            @Override
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    return;
                }
                mIabHelper.queryInventoryAsync(new IabHelper.QueryInventoryFinishedListener() {
                    @Override
                    public void onQueryInventoryFinished(IabResult result, Inventory inv) {
                        if (result.isFailure()) {
                            return;
                        }
                        if (inv.hasPurchase("ads_remove")) {
                            com.prestigio.android.ereader.utils.Utils.setAdsDisablePurchased(getApplicationContext(), true);
                        }
                    }
                });
            }
        });


//        setUpExceptionHandler(Environment.getExternalStorageDirectory().getPath() + File.separator + "PrestigioEreader ERRORS");

        DebugLog.e("ZLAndroidApplication", "onCreate DONE");
    }

    public IabHelper getIabHelper() {
        return mIabHelper;
    }

    public void startShelfService() {
        bindShelfService();
        //bindLibraryService();
    }

    public float getBatteryLevel() {
        return mBatteryLevel;
    }

    public void crash() {
        throw new RuntimeException("Opps it's crash!");
    }

    @Override
    public void onTerminate() {
        AudioBookRenderer.getInstance().stop();
        unregisterReceiver(mBatteryInfoReceiver);
        super.onTerminate();
    }

    public Activity getShelfActivity() {
        return mShelfActivity;
    }

    public void setShelfActivity(Activity activity) {
        mShelfActivity = activity;
    }

    public void onLowMemory() {
        DebugLog.wtf(TAG, "Low memory");
    }

    public EreaderShelfService.LocalBinder getLibraryService(@Nullable ServiceCallback callback) {
        synchronized (mShelfServiceLock) {
            if (mShelfService == null) {
                if (callback != null) {
                    mQueue.add(callback);
                }
                bindShelfService();
            } else {
                if (callback != null) {
                    callback.run(mShelfService, mLibraryService);
                }
            }
            return mLibraryService;
        }
    }

    public interface ServiceCallback {
        void run(EreaderShelfService service, EreaderShelfService.LocalBinder binder);
    }

    private LinkedList<ServiceCallback> mQueue = new LinkedList<>();

    @Nullable
    public EreaderShelfService getEreaderShelfService(@Nullable ServiceCallback callback) {
        synchronized (mShelfServiceLock) {
            if (mShelfService == null) {
                if (callback != null) {
                    mQueue.add(callback);
                }
                bindShelfService();
            } else {
                if (callback != null) {
                    callback.run(mShelfService, mLibraryService);
                }
            }
            return mShelfService;
        }
    }

    private final AtomicBoolean isOnBind = new AtomicBoolean(false);

    private void bindShelfService() {
        synchronized (isOnBind) {
            if (isOnBind.get()) {
                return;
            }
            isOnBind.set(true);

            synchronized (mShelfServiceLock) {
                if (mShelfService != null) {
                    return;
                }
            }
            final long startTime = System.currentTimeMillis();

            Intent i = new Intent(this.getApplicationContext(), EreaderShelfService.class);
            DebugLog.e(TAG, "start service bind");
            boolean result = ZLAndroidApplication.this.bindService(i, new ServiceConnection() {
                @Override
                public void onServiceConnected(ComponentName name, IBinder service) {
                    DebugLog.e(TAG, "onServiceConnected = " + (System.currentTimeMillis() - startTime));
                    synchronized (isOnBind) {
                        synchronized (mShelfServiceLock) {
                            try {
                                mShelfService = ((EreaderShelfService.LocalBinder) service).getService();
                            } catch (ClassCastException e) {
                                e.printStackTrace();
                                isOnBind.set(false);
                                return;
                            }
                            //DebugLog.e("bindLibraryService", "onServiceConnected 2");
                            mLibraryService = (EreaderShelfService.LocalBinder) service;
                            mLibraryService.setBooksTrackFolder(Paths.BooksDirectoryOption().getValue(), true);
                            //DebugLog.e("bindLibraryService", "onServiceConnected 3");
                            //Log.e(TAG, "onServiceConnected time = " + (System.currentTimeMillis() - startTime));
                            //isOnBind = false;
                            for (ServiceCallback run : mQueue) {
                                run.run(mShelfService, mLibraryService);
                            }
                        }
                        isOnBind.set(false);
                    }
                }

                @Override
                public void onServiceDisconnected(ComponentName name) {
                    synchronized (isOnBind) {
                        synchronized (mShelfServiceLock) {
                            mShelfServiceLock.notifyAll();
                            mShelfService = null;
                        }
                        isOnBind.set(false);
                    }
                }
            }, BIND_AUTO_CREATE);

            if (!result) {
                isOnBind.set(false);
            }
        }
    }

    public void restartApplication(Intent i) {
        if (i == null) {
            i = new Intent(ZLAndroidApplication.Instance().getBaseContext(), MainShelfActivity.class);
        }
        PendingIntent intent = PendingIntent.getActivity(ZLAndroidApplication.Instance().getBaseContext(), 0,
                i, PendingIntent.FLAG_ONE_SHOT);

        PendingIntent main_intent = PendingIntent.getActivity(ZLAndroidApplication.Instance().getBaseContext(), 0,
                new Intent(ZLAndroidApplication.Instance().getBaseContext(), MainShelfActivity.class), PendingIntent.FLAG_ONE_SHOT);

        AlarmManager mgr = (AlarmManager) ZLAndroidApplication.Instance().getBaseContext().getSystemService(Context.ALARM_SERVICE);

        DebugLog.e("ZLAndroidApplication", "schedule intent: " + i.getData());

        //mgr.set(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime() + 2 * 1000, main_intent);
        mgr.set(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime() + 2 * 1000, intent);

        System.exit(2);
    }

    public void restartApplication() {
        restartApplication(null);
    }

    public void stopApplication() {
        System.exit(2);
    }

    public void stopApplication(String message) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setContentTitle("EReader Prestigio").setContentText(message)
                .setSmallIcon(com.prestigio.ereader.R.drawable.prestigio);
        //.setLargeIcon(((BitmapDrawable) this.getResources().getDrawable(R.drawable.prestigio)).getBitmap());
        try {
            final String appPackageName = getPackageName();
            Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName));
            PendingIntent pending = PendingIntent.getActivity(this, 0, i, 0);
            builder.setContentIntent(pending);
        } catch (Exception e) {
            e.printStackTrace();
        }

        builder.setOngoing(false);
        Notification notification = builder.build();
        NotificationManager notificationManager = (NotificationManager) ZLAndroidApplication.Instance().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(99999, notification);

        System.exit(2);
    }

    public SVGHelper.SVGHolder getSVGHolder() {
        return baseSVGHolder;
    }

    /*synchronized Tracker getTracker(TrackerName trackerId) {
        if (!mTrackers.containsKey(trackerId)) {

            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            Tracker t = (trackerId == TrackerName.APP_TRACKER) ? analytics.newTracker(PROPERTY_ID)
                    : (trackerId == TrackerName.GLOBAL_TRACKER) ? analytics.newTracker(R.xml.global_tracker)
                    : analytics.newTracker(R.xml.ecommerce_tracker);
            mTrackers.put(trackerId, t);

        }
        return mTrackers.get(trackerId);
    }*/
    public synchronized Tracker getTracker() {
        if (!mTrackers.containsKey(TrackerName.APP_TRACKER)) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            analytics.enableAutoActivityReports(this);
            Tracker t = analytics.newTracker(R.xml.global_tracker);
            t.enableAutoActivityTracking(true);
            mTrackers.put(TrackerName.APP_TRACKER, t);

        }
        return mTrackers.get(TrackerName.APP_TRACKER);
    }

    public void setAppState(APP_STATE state) {
        mCurrentAppState = state;
    }

    public APP_STATE getAppState() {
        return mCurrentAppState;
    }

}
