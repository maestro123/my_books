/*
 * Copyright (C) 2007-2011 Geometer Plus <contact@geometerplus.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

package org.geometerplus.zlibrary.core.resources;

import java.util.Locale;

import org.geometerplus.zlibrary.core.options.ZLStringOption;

import android.content.res.Resources;

abstract public class ZLResource {
	public final String Name;
	
	public static ZLResource resource(String key) {
		ZLTreeResource.buildTree();
		if (ZLTreeResource.ourRoot == null) {
			return ZLMissingResource.Instance;
		}
		return ZLTreeResource.ourRoot.getResource(key);
	}

	protected ZLResource(String name) {
		Name = name;
	}

	abstract public boolean hasValue();
	abstract public String getValue();
	abstract public ZLResource getResource(String key);
	
	
	public static ZLStringOption getLocaleOption() {
		return new ZLStringOption("Resources", "CurrentLocale", null);
	}
	
	public static Locale parseLocale(String locale) {
		if (locale.length() == 0) {
			return Locale.getDefault();
		}
		final int first = locale.indexOf('_');
		if (first == -1) {
			return new Locale(locale);
		}
		final String language = locale.substring(0, first);
		final int second = locale.indexOf('_', first + 1);
		if (second == -1) {
			final String country = locale.substring(first + 1);
			return new Locale(language, country);
		}
		final String country = locale.substring(first + 1, second);
		final String variant = locale.substring(second + 1);
		return new Locale(language, country, variant);
	}
	
	
	public static String getCurrentLanguage(Resources res) {
        try{
		    final Locale locale = parseLocale(getLocaleOption().getValue());
		    final String language = locale.getLanguage();
            return language;
        } catch (NullPointerException e){
            if (res != null){
                Locale locale = res.getConfiguration().locale;
                return locale.getLanguage();
            } else {
                return "en";
            }
        }
	}
}
