/*
 * Copyright (C) 2007-2011 Geometer Plus <contact@geometerplus.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

package org.geometerplus.zlibrary.core.filesystem;

import android.content.Context;
import android.os.MemoryFile;
import com.prestigio.android.ereader.utils.Utils;
import com.prestigio.android.ereader.utils.ZLMemoryFile;
import com.prestigio.android.ereader.utils.apache.StringUtils;
import org.geometerplus.android.AdobeSDKWrapper.DebugLog;
import org.geometerplus.zlibrary.core.drm.EncryptionMethod;
import org.geometerplus.zlibrary.core.drm.FileEncryptionInfo;
import org.geometerplus.zlibrary.core.drm.embedding.EmbeddingInputStream;
import org.geometerplus.zlibrary.core.util.InputStreamHolder;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class ZLFile implements InputStreamHolder {
    private final static HashMap<String, ZLFile> ourCachedFiles = new HashMap<String, ZLFile>();

    protected interface ArchiveType {
        int NONE = 0;
        int GZIP = 0x0001;
        int BZIP2 = 0x0002;
        int COMPRESSED = 0x00ff;
        int ZIP = 0x0100;
        int TAR = 0x0200;
        int ARCHIVE = 0xff00;
    }

    private String myExtension;

    protected String myShortName;
    protected String myShortNameLowered = null;
    protected int myArchiveType;
    private boolean myIsCached = true;

    protected void init() {
        final String name = getLongName();
        final int index = name.lastIndexOf('.');
        myExtension = (index > 0) ? name.substring(index + 1).toLowerCase().intern() : "";
        myShortName = name.substring(name.lastIndexOf('/') + 1);

        try {
            if (getParent() != null && getParent().isArchive()) {
                myExtension = getParent().getExtension();
            }
        } catch (java.lang.NullPointerException e) {
            DebugLog.d("ZLFile", "NullPointerException, getParent: " + getPath());
            e.printStackTrace();
        }

        int archiveType = ArchiveType.NONE;
        if (!this.isDirectory()) {
            if (myExtension == "zip") {
                archiveType |= ArchiveType.ZIP;
            } else if (myExtension == "oebzip") {
                archiveType |= ArchiveType.ZIP;
            } else if (myExtension == "epub") {
                archiveType |= ArchiveType.ZIP;
            } else if (myExtension == "tar") {
                archiveType |= ArchiveType.TAR;
            }
        }
        myArchiveType = archiveType;

        mPathLowerCase = getPath();

        if (getShortNameLowered().endsWith(".epub")) {
            mIsEpubFile = true;
        }
        mIsBookFile = Utils.isBookFile(this, false);

        int counts = StringUtils.countMatches(getPath(), ":");
        mIsEntryInsideArchive = counts > 0;

        if (mIsEntryInsideArchive) {
            if (isArchive()) {
                if (mIsEpubFile) {
                    mIsZipInsideZip = false;
                }
                if (mIsBookFile && !getPathLowerCase().endsWith(".zip")) {
                    mIsZipInsideZip = false;
                } else {
                    if (getPathLowerCase().endsWith(".zip") && !Utils.isSingleBookInArchive(this)) {
                        mIsZipInsideZip = true;
                    }
                }
            }
        } else {
            mIsZipInsideZip = false;
        }

        if (getShortName() != null) {
            String[] split = getShortName().split(":");
            mSubShortName = split != null && split.length > 1 ? split[1] : getShortName();
        }

    }

    private volatile String mPathLowerCase = null;
    private volatile boolean mPathLowerCase_lowered = false;

    private volatile String mSubShortName = null;
    private volatile boolean mIsBookFile = false;
    private volatile boolean mIsZipInsideZip = false;
    private volatile boolean mIsEpubFile = false;
    private volatile boolean mIsEntryInsideArchive = false;
    private volatile String mCannonicalPath = null;

    // may be SLOW!
    public synchronized boolean isSingleBookInArchive() {
        if (mIsSingleBookInArchive == null) {
            List<ZLFile> childs;
            if (mIsEntryInsideArchive) {
                mIsSingleBookInArchive = getParent().isSingleBookInArchive();
            } else {
                if (isArchive()) {
                    if (mIsEpubFile) {
                        mIsSingleBookInArchive = true;
                    } else {
                        childs = children();
                        if (childs.isEmpty()) {
                            mIsSingleBookInArchive = true;
                        } else {
                            int count = 0;
                            for (ZLFile child : childs) {
                                if (Utils.isBookFile(child, false)) {
                                    count++;
                                }
                                if (count > 1) {
                                    mIsSingleBookInArchive = false;
                                    break;
                                }
                            }
                            mIsSingleBookInArchive = count == 1;
                        }
                    }
                } else {
                    mIsSingleBookInArchive = false;
                }
            }
        }

        return mIsSingleBookInArchive;
    }

    private volatile Boolean mIsSingleBookInArchive = null;

    public boolean isBookFile() {
        return mIsBookFile;
    }

    public static ZLFile createDummyFile() {
        return new ZLDummyFile();
    }

    public static ZLFile createFile(ZLFile parent, String name) {
        ZLFile file = null;
        if (parent == null) {
            ZLFile cached = ourCachedFiles.get(name);
            if (cached != null) {
                return cached;
            }
            if (!name.startsWith("/")) {
                return ZLResourceFile.createResourceFile(name);
            } else {
                return new ZLPhysicalFile(name);
            }
        } else if ((parent instanceof ZLPhysicalFile) && (parent.getParent() == null)) {
            // parent is a directory
            file = new ZLPhysicalFile(parent.getPath() + '/' + name);
        } else if (parent instanceof ZLResourceFile) {
            file = ZLResourceFile.createResourceFile((ZLResourceFile) parent, name);
        } else {
            file = ZLArchiveEntryFile.createArchiveEntryFile(parent, name);
        }

        if (!ourCachedFiles.isEmpty() && (file != null)) {
            ZLFile cached = ourCachedFiles.get(file.getPath());
            if (cached != null) {
                return cached;
            }
        }
        return file;
    }

    public static final HashMap<String, ZLMemoryFile> mMemoryFiles = new HashMap<String, ZLMemoryFile>();

    public static ZLFile createMemoryFile(Context context, String name, MemoryFile memoryFile) {
        synchronized (mMemoryFiles) {
            ZLMemoryFile mf = new ZLMemoryFile(context, memoryFile, name);
            mMemoryFiles.put(name, mf);
            return mf;
        }
    }

    public static ZLFile getMemoryFile(String name) {
        synchronized (mMemoryFiles) {
            return mMemoryFiles.get(name);
        }
    }

    public long lastModified() {
        final ZLFile physicalFile = getPhysicalFile();
        return physicalFile != null ? physicalFile.lastModified() : 0;
    }

    public static ZLFile createFileByPath(String path) {
        if (path == null) {
            return null;
        }
        ZLFile cached = ourCachedFiles.get(path);
        if (cached != null) {
            return cached;
        }

        int len = path.length();
        char first = len == 0 ? '*' : path.charAt(0);
        if (first != '/') {
            while (len > 1 && first == '.' && path.charAt(1) == '/') {
                path = path.substring(2);
                len -= 2;
                first = len == 0 ? '*' : path.charAt(0);
            }
            return ZLResourceFile.createResourceFile(path);
        }
        int index = path.lastIndexOf(':');
        if (index > 1) {
            final ZLFile archive = createFileByPath(path.substring(0, index));
            if (archive != null && archive.myArchiveType != 0) {
                return ZLArchiveEntryFile.createArchiveEntryFile(
                        archive, path.substring(index + 1)
                );
            }
        }
        return new ZLPhysicalFile(path);
    }

    public String getPathLowerCase() {
        if (!mPathLowerCase_lowered) {
            mPathLowerCase = mPathLowerCase.toLowerCase();
            mPathLowerCase_lowered = true;
        }
        return mPathLowerCase;
    }

    public abstract long size();

    public abstract boolean exists();

    public abstract boolean isDirectory();

    public abstract String getPath();

    public abstract ZLFile getParent();

    public abstract ZLPhysicalFile getPhysicalFile();

    public final InputStream getInputStream(FileEncryptionInfo encryptionInfo) throws IOException {
        if (encryptionInfo == null) {
            return getInputStream();
        }

        if (EncryptionMethod.EMBEDDING.equals(encryptionInfo.Method)) {
            return new EmbeddingInputStream(getInputStream(), encryptionInfo.ContentId);
        }

        throw new IOException("Encryption method " + encryptionInfo.Method + " is not supported");
    }

    public boolean isReadable() {
        return true;
    }

    public final boolean isCompressed() {
        return (0 != (myArchiveType & ArchiveType.COMPRESSED));
    }

    public final boolean isArchive() {
        return (0 != (myArchiveType & ArchiveType.ARCHIVE));
    }

    public final boolean isArchive(boolean excludeEpub) {
        if (excludeEpub) {
            if (mIsEpubFile) {
                return false;
            }
        }
        return (0 != (myArchiveType & ArchiveType.ARCHIVE));
    }

    public boolean isEntryInsideArchive() {
        return mIsEntryInsideArchive;
    }

    private final AtomicReference<ZLFile> mSingleBookFile = new AtomicReference<ZLFile>(null);

    private String getCanonicalPath2() {
        if (mCannonicalPath != null) {
            return mCannonicalPath;
        }
        String resultPath = getPath();
        String emulatedPart = getEmulatedFolderPart(resultPath);
        if (emulatedPart != null) {
            resultPath = resultPath.replace(emulatedPart, "/storage/emulated/legacy");
        }
        ArrayList<String> runtimes;
        if (resultPath.contains(" ")) {
            File file = new File(resultPath);
            while (file.getPath().contains(" ") && file.getParentFile() != null) {
                file = file.getParentFile();
            }
            runtimes = Utils.runRuntimeCommand("readlink -f " + file.getPath());
            if (runtimes.size() == 1) {
                return mCannonicalPath = resultPath.replace(file.getPath(), runtimes.get(0));
            } else {
                return mCannonicalPath = null;
            }
        } else {
            runtimes = Utils.runRuntimeCommand("readlink -f " + resultPath);
        }

        if (runtimes.size() == 1) {
            return mCannonicalPath = runtimes.get(0);
        } else {
            return mCannonicalPath = null;
        }
    }

    public String getCanonicalPath() {
        if (mCannonicalPath != null) {
            return mCannonicalPath;
        }

        String canonical = getCanonicalPath2();
        if (canonical != null) {
            return canonical;
        }

        String resultPath = getPath();
        String newPath;
        String emulatedPart = getEmulatedFolderPart(resultPath);
        if (emulatedPart != null) {
            resultPath = resultPath.replace(emulatedPart, "/storage/emulated/legacy");
        }

        int attempts = 0;
        while (attempts < 10) {
            attempts++;
            newPath = resultPath;
            String[] parts = resultPath.split("/");
            String path = "";
            for (String part : parts) {
                if (part.equals("")) {
                    continue;
                }
                path = path + "/" + part;
                ArrayList<String> runtimes = Utils.runRuntimeCommand("ls " + path + " -al");
                if (runtimes.size() == 1) {
                    String emulatedPath = parseRuntimeString(runtimes.get(0));
                    if (emulatedPath != null) {
                        newPath = resultPath.replace(path, emulatedPath);
                        break;
                    } else {
                        return mCannonicalPath = resultPath; // TODO: unknown error
                    }
                }
            }
            if (!newPath.equals(resultPath)) {
                resultPath = newPath;
            } else {
                return mCannonicalPath = resultPath;
            }
        }
        return mCannonicalPath = "";
    }

    private String getEmulatedFolderPart(String fileName) {
        String pattern = "^(\\/storage\\/emulated\\/\\d+).*";
        if (fileName.startsWith("/storage/emulated") && fileName.matches(pattern)) {
            Pattern p = Pattern.compile(pattern);
            Matcher m = p.matcher(fileName);
            m.find();
            return m.group(1);
        }
        return null;
    }

    private String parseRuntimeString(String fileName) {
        String pattern = "^l.+? -> (\\/.+?$)";
        if (fileName.matches(pattern)) {
            Pattern p = Pattern.compile(pattern);
            Matcher m = p.matcher(fileName);
            m.find();
            return m.group(1);
        }
        return null;
    }

    // slow performance!
    public ZLFile getSingleBookFile() {
        if (isArchive(true)) {
            if (isEntryInsideArchive() && !Utils.isSingleBookInArchive(this)) {
                if (mIsBookFile) {
                    return this;
                } else {
                    return null;
                }
            }
            if (mIsBookFile) {
                synchronized (mSingleBookFile) {
                    if (mSingleBookFile.get() != null) {
                        return mSingleBookFile.get();
                    }
                }
                List<ZLFile> childs = children();
                ZLFile book_file = null;
                for (ZLFile child : childs) {
                    if (child.isBookFile()) {
                        if (book_file == null) {
                            book_file = child;
                        } else {
                            return null;
                        }
                    }
                }
                synchronized (mSingleBookFile) {
                    mSingleBookFile.set(book_file);
                    return book_file;
                }
            } else {
                List<ZLFile> childs = children();
                if (childs.size() > 1 || childs.size() == 0) {
                    return null;
                } else {
                    return childs.get(0);
                }
            }
        } else {
            if (mIsBookFile) {
                return this;
            } else {
                return null;
            }
        }
    }

    private final AtomicReference<Boolean> mArchiveWithBooks = new AtomicReference<Boolean>(null);

    // slow performance!
    public boolean isArchiveWithBooks(boolean checkFilesCountInArchive) {
        if (isArchive(true)) {
            if (!checkFilesCountInArchive && isBookFile()) {
                return false;
            }
            synchronized (mArchiveWithBooks) {
                if (mArchiveWithBooks.get() != null) {
                    return mArchiveWithBooks.get();
                }
            }
            List<ZLFile> childs = children();
            int books_count = 0;
            for (ZLFile child : childs) {
                if (child.isBookFile()) {
                    books_count++;
                }
            }
            if (books_count > 1) {
                synchronized (mArchiveWithBooks) {
                    mArchiveWithBooks.set(true);
                    return true;
                }
            }
            synchronized (mArchiveWithBooks) {
                mArchiveWithBooks.set(false);
            }
        }
        return false;
    }

    public boolean singleBook() {
        if (getPhysicalFile() == null) {
            return false;
        }
        if (isDirectory()) {
            return false;
        }
        if (mIsEpubFile) {
            return true;
        }
        if (isEntryInsideArchive()) {
            return isBookFile();
        } else if (isArchive()) {
            return isSingleBookInArchive();
        }
        return isBookFile();
    }

    public boolean isZipInsideZip() { // zip archive inside zip (+ fb2.zip)
        return mIsZipInsideZip;
    }

    public abstract String getLongName();

    public String getShortName() {
        return myShortName;
    }

    public String getShortNameLowered() {
        if (myShortNameLowered == null) {
            myShortNameLowered = myShortName.toLowerCase();
        }
        return myShortNameLowered;
    }

    public String getSubShortName() {
        return mSubShortName;
    }

    public final String getExtension() {
        return myExtension;
    }

    protected List<ZLFile> directoryEntries() {
        return Collections.emptyList();
    }

    protected List<ZLFile> directoryEntries(FileFilter filter) {
        return Collections.emptyList();
    }

    public final List<ZLFile> children(boolean getAll) {
        if (exists()) {
            if ((isDirectory() && !isArchive()) || getAll) {
                return children();
            } else if (isArchive()) {
                return ZLArchiveEntryFile.getFirstLevelFiles(this);
            }
        }
        return Collections.emptyList();
    }

    public final List<ZLFile> children() {
        if (exists()) {
            if (isArchive()) {
                return ZLArchiveEntryFile.archiveEntries(this);
            } else {
                return directoryEntries();
            }
        }
        return Collections.emptyList();
    }

    public final List<ZLFile> children(FileFilter filter) {
        return Collections.emptyList();
    }

    @Override
    public int hashCode() {
        return getPathLowerCase().hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof ZLFile)) {
            return false;
        }
        return getPathLowerCase().equals(((ZLFile) o).getPathLowerCase());
    }

    protected boolean isCached() {
        return myIsCached;
    }

    public void setCached(boolean cached) {
        myIsCached = cached;
        if (cached) {
            ourCachedFiles.put(getPath(), this);
        } else {
            ourCachedFiles.remove(getPath());
            if (0 != (myArchiveType & ArchiveType.ZIP)) {
                ZLZipEntryFile.removeFromCache(this);
            }
        }
    }

}
