package org.geometerplus.zlibrary.core.filesystem;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by yanis on 4/25/15.
 */
public class ZLDummyFile extends ZLFile{

    ZLDummyFile() {
        init();
    }

    @Override
    public boolean exists() {
        return false;
    }

    @Override
    public long size() {
        return 0;
    }

    @Override
    public boolean isDirectory() {
        return false;
    }

    @Override
    public boolean isReadable() {
        return false;
    }

    public boolean delete() {
        return true;
    }

    @Override
    public String getPath() {
        return "/dummy/file.null";
    }

    @Override
    public String getLongName() {
        return "/dummy/file.null";
    }

    @Override
    public ZLFile getParent() {
        return null;
    }

    @Override
    public ZLPhysicalFile getPhysicalFile() {
        return null;
    }

    @Override
    public InputStream getInputStream() throws IOException {
        return null;
    }
}
