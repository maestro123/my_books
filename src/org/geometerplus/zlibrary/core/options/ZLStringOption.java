/*
 * Copyright (C) 2007-2011 Geometer Plus <contact@geometerplus.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

package org.geometerplus.zlibrary.core.options;

import java.util.HashMap;

import android.os.SystemClock;

public final class ZLStringOption extends ZLOption {

    private volatile String myDefaultValue;
    private volatile String myValue;

    private static final Object mSync = new Object();
    private static final int mCacheTime = 2;
    private static volatile HashMap<Integer, CacheOption> mCachesValues = new HashMap<Integer, CacheOption>();

    private Object step;
    private Object min, max;
    private String metrics;

    private static class CacheOption {
        ZLStringOption mOption;
        long mLastUpdateCacheTime;
    }

    public static void clearCache() {
        synchronized (mSync) {
            mCachesValues.clear();
        }
    }

    public ZLStringOption(String group, String optionName, String defaultValue) {
        super(group, optionName);

        int hash = (group + "/$/" + optionName + "/$/" + defaultValue).hashCode();
        boolean addToCache = false;
        synchronized (mSync) {
            CacheOption cache = mCachesValues.get(hash);
            if (cache != null) {
                if ((SystemClock.elapsedRealtime() - cache.mLastUpdateCacheTime) > mCacheTime * 1000) {
                    addToCache = true;
                } else {
                    myDefaultValue = cache.mOption.myDefaultValue;
                    myValue = cache.mOption.myValue;
                    myIsSynchronized = cache.mOption.myIsSynchronized;
                }
            } else {
                addToCache = true;
            }

            if (addToCache) {
                myDefaultValue = (defaultValue != null) ? defaultValue.intern() : "";
                myValue = myDefaultValue;
                cache = new CacheOption();
                cache.mOption = this;
                cache.mLastUpdateCacheTime = SystemClock.elapsedRealtime();
                mCachesValues.put(hash, cache);
            }

        }
    }

    public void setup(Object step, Object min, Object max, String metrics) {
        this.step = step;
        this.min = min;
        this.max = max;
        this.metrics = metrics;
    }

    public String getValue() {
        if (!myIsSynchronized) {
            String value = getConfigValue(myDefaultValue);
            if (value != null) {
                myValue = value;
            }
            myIsSynchronized = true;
        }
        return myValue;
    }

    public String getDefaultValue() {
        return myDefaultValue;
    }

    public void setValue(String value) {
        if (value == null) {
            return;
        }
        value = value.intern();
        if (myIsSynchronized && (myValue == value)) {
            return;
        }
        myValue = value;
        if (value == myDefaultValue) {
            unsetConfigValue();
        } else {
            setConfigValue(value);
        }
        myIsSynchronized = true;
    }

    public void reset() {
        setValue(myDefaultValue);
    }

}
