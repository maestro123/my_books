package org.geometerplus.zlibrary.core.filetypes;

import org.geometerplus.zlibrary.core.filesystem.ZLFile;
import org.geometerplus.zlibrary.core.util.MimeType;

import java.util.List;

/**
 * Created by Artyom on 5/28/2015.
 */
public class FileTypeAudioBook extends FileType {

    public static final String TAG = FileTypeAudioBook.class.getSimpleName();

    private static final String MIMES = "(m4b|mp3)";

    public FileTypeAudioBook() {
        super("AudioBook");
    }

    @Override
    public boolean acceptsFile(ZLFile file) {
        String extension = file.getExtension();
        return extension.matches(MIMES);
    }

    @Override
    public List<MimeType> mimeTypes() {
        return MimeType.TYPES_AUDIO_BOOKS;
    }

    @Override
    public MimeType mimeType(ZLFile file) {
        return acceptsFile(file) ? file.getExtension().endsWith(".m4b") ? MimeType.APP_M4B : MimeType.APP_MP3 : MimeType.NULL;
    }

    @Override
    public String defaultExtension(MimeType mime) {
        return mime.equals(MimeType.APP_M4B) ? "m4b" : "mp3";
    }
}
