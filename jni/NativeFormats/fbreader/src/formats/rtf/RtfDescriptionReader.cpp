/*
 * Copyright (C) 2004-2014 Geometer Plus <contact@geometerplus.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

#include <ZLInputStream.h>
#include <android/log.h>

#include "RtfDescriptionReader.h"

#include "../FormatPlugin.h"
#include "../../library/Book.h"
#include "../../library/Author.h"
#include <typeinfo>

#define LOG_TAG "NativeFormat"
#define LOGE(x...) __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, x)

RtfDescriptionReader::RtfDescriptionReader(Book &book) : RtfReader(book.encoding()), myBook(book) {
}

void RtfDescriptionReader::setEncoding(int code) {
	ZLEncodingCollection &collection = ZLEncodingCollection::Instance();

    //LOGE("setEncoding, code = %d", code);

	myConverter = collection.converter(code);
	if (!myConverter.isNull()) {
		myBook.setEncoding(myConverter->name());
	} else {
		myConverter = collection.defaultConverter();
	}
}

bool RtfDescriptionReader::readDocument(const ZLFile &file) {
	myDoRead = false;
	return RtfReader::readDocument(file);
}

void RtfDescriptionReader::addCharData(const char *data, std::size_t len, bool convert) {
    //LOGE("addCharData, myBuffer = %s, data = %s", myBuffer.c_str(), data);
    //LOGE("addCharData, len = %d", len);
	if (myDoRead && len > 0) {
		if (convert) {
            //LOGE("addCharData, convert, class = %s", typeid(myConverter).name());
			myConverter->convert(myBuffer, data, data + len);
		} else {
            //LOGE("addCharData, !convert");
			myBuffer.append(data, len);
		}
	}
    //LOGE("addCharData, myBuffer = %s", myBuffer.c_str());
}

void RtfDescriptionReader::switchDestination(DestinationType destination, bool on) {
	switch (destination) {
		case DESTINATION_INFO:
			if (!on) {
				interrupt();
			}
			break;
		case DESTINATION_TITLE:
			myDoRead = on;
			if (!on) {
                for(int i = 0; i < strlen(myBuffer.c_str()); i++){
                    //LOGE("switchDestination, title = %u", myBuffer.c_str()[i]);
                }
                //LOGE("switchDestination, title = %s", myBuffer.data());
				myBook.setTitle(myBuffer);
				myBuffer.erase();
			}
			break;
		case DESTINATION_AUTHOR:
			myDoRead = on;
			if (!on) {
				myBook.addAuthor(myBuffer);
				myBuffer.erase();
			}
			break;
		default:
			break;
	}
	if (!myBook.title().empty() && !myBook.authors().empty() && !myBook.encoding().empty()) {
		interrupt();
	}
}

void RtfDescriptionReader::insertImage(const std::string&, const std::string&, std::size_t, std::size_t) {
}

void RtfDescriptionReader::setFontProperty(FontProperty) {
}

void RtfDescriptionReader::newParagraph() {
}

void RtfDescriptionReader::setAlignment() {
}
