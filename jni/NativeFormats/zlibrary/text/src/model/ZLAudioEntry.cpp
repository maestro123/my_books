#include "ZLAudioEntry.h"

void ZLAudioEntry::addSource(const std::string &type, const std::string &path) {
	mySources.insert(std::make_pair(type, path));
}

const std::map<std::string,std::string> &ZLAudioEntry::sources() const {
	return mySources;
}
