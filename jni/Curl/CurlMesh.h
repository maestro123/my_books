#include <jni.h>
#include <stddef.h>
#include <cmath>
#include <vector>

#define PI = 3.141592653589793;

using std::vector;

class CurlMesh {

public:
    CurlMesh(int meshCount);

    ~CurlMesh();

private:
    void addVertex(int position, Vertex vertex);

    void curl(Point curlPos, Point curlDir, double radius);

    Array <Vertex> *mArrIntersections;
    Array <Vertex> *mArrOutputVertices;
    Array <Vertex> *mArrRotatedVertices;
    Array<double> *mArrScanLines;
    Array <ShadowVertex> *mArrShadowDropVertices;
    Array <ShadowVertex> *mArrShadowSelfVertices;
    Array <ShadowVertex> *mArrShadowTempVertices;
    Array <Vertex> *mArrTempVertices;
    Vertex mRectangle[4];
    int *mTextureIds;
    CurlPage page;

    int mCountShadowDrop;
    int mCountShadowSelf;
    int mCountVertices;
    int mMaxCurlSplits;

    FloatBuffer mBufNormals[];
    FloatBuffer mBufShadowPenumbra[];
    FloatBuffer mBufShadowVertices[];
    FloatBuffer mBufTexCoords[];
    FloatBuffer mBufVertices[];

    bool mFlipTexture = false;

    friend class CurlPage;

    friend class Vertext;

    friend class FloatBuffer;

    friend class Array;
};

class CurlPage {

public:
    CurlPage();

    ~CurlPage();

    enum SIDE {
        SIDE_FRONT, SIDE_BACK
    };

    void setJBitmap(jobject jBitmap, SIDE side) {
        switch (side) {
            case SIDE_FRONT:
                jBitmapFront = jBitmap;
                break;
            case SIDE_BACK:
                jBitmapBack = jBitmap;
                break;
        }
        haveChanges = true;
    };

    jobject getJBitmap(SIDE side) {
        switch (side) {
            case SIDE_FRONT:
                return jBitmapFront;
            case SIDE_BACK:
                return jBitmapBack;
        }
        return NULL;
    }

    void setChangesApplyed() {
        haveChanges = false;
    }

    void reset() {
        colorFront = 0;
        colorBack = 0;
        haveChanges = true;
    };

private:
    jobject jBitmapFront, jBitmapBack;
    int colorFront, colorBack;
    bool haveChanges;
};

class Vertex {

public:
    double mNormalX;
    double mNormalY;
    double mNormalZ;
    double mPenumbraX;
    double mPenumbraY;
    double mPosX;
    double mPosY;
    double mPosZ;
    double mTexX;
    double mTexY;

    Vertex() {
        mNormalX = mNormalY = 0;
        mNormalZ = 1;
        mPosX = mPosY = mPosZ = mTexX = mTexY = 0;
    }

    ~Vertex();

    void rotateZ(double theta) {
        double cos = cos(theta);
        double sin = sin(theta);
        double x = mPosX * cos + mPosY * sin;
        double y = mPosX * -sin + mPosY * cos;
        mPosX = x;
        mPosY = y;
        double nx = mNormalX * cos + mNormalY * sin;
        double ny = mNormalX * -sin + mNormalY * cos;
        mNormalX = nx;
        mNormalY = ny;
        double px = mPenumbraX * cos + mPenumbraY * sin;
        double py = mPenumbraX * -sin + mPenumbraY * cos;
        mPenumbraX = px;
        mPenumbraY = py;
    }

    void set(Vertex vertex) {
        mPosX = vertex.mPosX;
        mPosY = vertex.mPosY;
        mPosZ = vertex.mPosZ;
        mTexX = vertex.mTexX;
        mTexY = vertex.mTexY;
        mNormalX = vertex.mNormalX;
        mNormalY = vertex.mNormalY;
        mNormalZ = vertex.mNormalZ;
        mPenumbraX = vertex.mPenumbraX;
        mPenumbraY = vertex.mPenumbraY;
    }

    void translate(double dx, double dy) {
        mPosX += dx;
        mPosY += dy;
    }
};

class ShadowVertex {
public:
    double mPenumbraX;
    double mPenumbraY;
    double mPosX;
    double mPosY;
    double mPosZ;

    ShadowVertex();

    ~ShadowVertex();

};

class FloatBuffer {

    float values[];
    int offset;

public:

    FloatBuffer(int alocated) {
        values = new float[alocated];
    };

    ~FloatBuffer() {
        operator delete(values);
    };

    void rewind() {
        offset = 0;
    };

    void put(float value) {
        offset++;
        values[offset] = value;
    }

    float *getValues() {
        return values;
    }

};

template<class T>
class Array {

    T elements[];
    int size;

public:

    Array(int capacity) {
        elements = new T[capacity];
    }

    void add(T item) {
        size++;
        elements[size] = item;
    }

    void add(T item, int index) {
        for (int i = size; i > index; --i) {
            elements[i] = elements[i - 1];
        }
        elements[index] = item;
        size++;
    }

    void addAll(T array[]) {
        for (int i = 0; i < sizeof(array); ++i) {
            elements[size++] = array[i];
        }
    }

    void addAll(Array<T> array) {
        for (int i = 0; i < array.size; ++i) {
            elements[size++] = array.get(i);
        }
    }

    T get(int index) {
        return elements[index];
    }

    T remove(int index) {
        T item = elements[index];
        for (int i = index; i < size; ++i) {
            elements[i] = elements[i + 1];
        }
        size--;
        return item;
    }

};

struct Point {
    int x;
    int y;
};

