#include "CurlMesh.h"

CurlMesh::CurlMesh(int maxCurlSplits) {
    mMaxCurlSplits = maxCurlSplits < 1 ? 1 : maxCurlSplits;

    mArrScanLines = new Array<double>(maxCurlSplits + 2);
    mArrOutputVertices = new Array(7);
    mArrRotatedVertices = new Array(4);
    mArrIntersections = new Array(2);
    mArrTempVertices = new Array(7 + 4);

    for (int i = 0; i < 7 + 4; ++i) {
        mArrTempVertices->add(new Vertex());
    }

    mArrShadowSelfVertices = new Array((mMaxCurlSplits + 2) * 2);
    mArrShadowDropVertices = new Array((mMaxCurlSplits + 2) * 2);
    mArrShadowTempVertices = new Array((mMaxCurlSplits + 2) * 2);


    for (int i = 0; i < (mMaxCurlSplits + 2) * 2; ++i) {
        mArrShadowTempVertices->add(new ShadowVertex());
    }

    // Set up shadow penumbra direction to each vertex. We do fake 'self
    // shadow' calculations based on this information.
    mRectangle[0].mPenumbraX = mRectangle[1].mPenumbraX = mRectangle[1].mPenumbraY = mRectangle[3].mPenumbraY = -1;
    mRectangle[0].mPenumbraY = mRectangle[2].mPenumbraX = mRectangle[2].mPenumbraY = mRectangle[3].mPenumbraX = 1;

    // There are 4 vertices from bounding rect, max 2 from adding split line
    // to two corners and curl consists of max mMaxCurlSplits lines each
    // outputting 2 vertices.
    int maxVerticesCount = 4 + 2 + (2 * mMaxCurlSplits);
    mBufVertices = new FloatBuffer(maxVerticesCount * 3 * 4);
    mBufTexCoords = new FloatBuffer(maxVerticesCount * 2 * 4);
    mBufNormals = new FloatBuffer(maxVerticesCount * 3 * 4);

    int maxShadowVerticesCount = (mMaxCurlSplits + 2) * 2 * 2;
    mBufShadowVertices = new FloatBuffer(maxShadowVerticesCount * 3 * 4);
    mBufShadowPenumbra = new FloatBuffer(maxShadowVerticesCount * 2 * 4);

}


void CurlMesh::addVertex(int position, Vertex vertex) {
    mBufVertices->put((float) vertex.mPosX);
    mBufVertices->put((float) vertex.mPosZ);
    mBufVertices->put((float) vertex.mPosZ);
    mBufTexCoords->put((float) vertex.mTexX);
    mBufTexCoords->put((float) vertex.mTexY);
    mBufNormals->put((float) vertex.mNormalX);
    mBufNormals->put((float) vertex.mNormalY);
    mBufNormals->put((float) vertex.mNormalZ);
}

void CurlMesh::curl(Point curlPos, Point curlDir, double radius) {
//    mBufVertices->rewind();
//    mBufTexCoords->rewind();
//    mBufNormals->rewind();
//
//    double curlAngle = acos(curlDir.x);
//    curlAngle = curlDir.y > 0 ? -curlAngle : curlAngle;
//    mArrTempVertices->addAll(mArrRotatedVertices->getElements());
}