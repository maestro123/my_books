#include <android/log.h>
#include "CurlRenderer.h"

#define LOG_TAG "CurlNative"
#define LOGE(x...) __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, x)

void CurlRenderer::onDrawFrame() {
    LOGE("onDrawFrame");
}

void CurlRenderer::onSurfaceCreated() {
    LOGE("onSurfaceCreated");
}

void CurlRenderer::onSurfaceChange() {
    LOGE("onSurfaceChange");
}