#include <jni.h>
#include <GLES/gl.h>
#include <android/bitmap.h>
#include <time.h>
#include <android/log.h>
#include "CurlRenderer.h"

#define LOG_TAG "CurlNative"
#define LOGE(x...) __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, x)

#ifdef __cplusplus
extern "C" {
#endif

static const char SHADER_SHADOW_FRAGMENT[] =
        "precision mediump float;\n"
                "varying vec2 vPenumbra;\n"
                "void main() {\n"
                "  float alpha = 1.0 - length(vPenumbra);\n"
                "  alpha = smoothstep(0.0, 2.0, alpha);\n"
                "  gl_FragColor = vec4(0.0, 0.0, 0.0, alpha);\n"
                "}\n";

static const char SHADER_SHADOW_VERTEX[] =
        "uniform mat4 uProjectionM;                                     \n"
                "attribute vec3 aPosition;                                      \n"
                "attribute vec2 aPenumbra;                                      \n"
                "varying vec2 vPenumbra;                                        \n"
                "void main() {                                                  \n"
                "  vec3 pos = vec3(aPosition.xy + aPenumbra, aPosition.z);      \n"
                "  gl_Position = uProjectionM * vec4(pos, 1.0);                 \n"
                "  vPenumbra = normalize(aPenumbra);                            \n"
                "}                                                              \n";

static const char SHADER_TEXTURE_FRAGMENT[] =
        "precision mediump float;                                       \n"
                "uniform sampler2D sTextureFront;                               \n"
                "uniform sampler2D sTextureBack;                                \n"
                "uniform vec4 uColorFront;                                      \n"
                "uniform vec4 uColorBack;                                       \n"
                "varying vec3 vNormal;                                          \n"
                "varying vec2 vTexCoord;                                        \n"
                "void main() {                                                  \n"
                "  vec3 normal = normalize(vNormal);                            \n"
                "  if (normal.z >= 0.0) {                                       \n"
                "    gl_FragColor = texture2D(sTextureFront, vTexCoord);        \n"
                "    gl_FragColor.rgb = mix(gl_FragColor.rgb,                   \n"
                "                   uColorFront.rgb, uColorFront.a);            \n"
                "  }                                                            \n"
                "  else {                                                       \n"
                "    gl_FragColor = texture2D(sTextureBack, vTexCoord);         \n"
                "    gl_FragColor.rgb = mix(gl_FragColor.rgb,                   \n"
                "                   uColorBack.rgb, uColorBack.a);              \n"
                "  }                                                            \n"
                "  gl_FragColor.rgb *= 0.5 + abs(normal.z) * 0.5;               \n"
                "}                                                              \n";

static const char SHADER_TEXTURE_VERTEX[] =
        "uniform mat4 uProjectionM;                                     \n"
                "attribute vec3 aPosition;                                      \n"
                "attribute vec3 aNormal;                                        \n"
                "attribute vec2 aTexCoord;                                      \n"
                "varying vec3 vNormal;                                          \n"
                "varying vec2 vTexCoord;                                        \n"
                "void main() {                                                  \n"
                "  gl_Position = uProjectionM * vec4(aPosition, 1.0);           \n"
                "  vNormal = aNormal;                                           \n"
                "  vTexCoord = aTexCoord;                                       \n"
                "}                                                              \n";

CurlRenderer *mRenderer;
//std::map<int, CurlShader> mShaders;

static long now_ms(void) {
    struct timespec res;
    clock_gettime(CLOCK_REALTIME, &res);
    return res.tv_nsec / 1000000;
}

//CurlShader getShader(int shaderId) {
//    CurlShader shader;
//    if (mShaders.find(shaderId) == mShaders.end()) {
//        shader = CurlShader();
//        mShaders.insert(std::pair<int, CurlShader>(shaderId, shader));
//    } else {
//        shader = mShaders.find(shaderId)->second;
//    }
//    return shader;
//}

void Java_com_prestigio_android_ereader_read_curl_CurlMesh_bindTexture(JNIEnv *env, jobject jObj, jint textureId, jobject jbitmap, jboolean subImage, jint activeTexture) {
    glEnable(GL_TEXTURE_2D);
    AndroidBitmapInfo info = {0};
    if (AndroidBitmap_getInfo(env, jbitmap, &info) != 0) {
        return;
    }
    int width = info.width;
    int height = info.height;

    char *pixels;
    if (AndroidBitmap_lockPixels(env, jbitmap, (void **) &pixels) < 0) {
        return;
    }
    GLenum format;
    GLenum dataType;

    if (info.format == ANDROID_BITMAP_FORMAT_RGBA_8888) {
        format = GL_RGBA;
        dataType = GL_UNSIGNED_BYTE;
    } else if (info.format == ANDROID_BITMAP_FORMAT_RGB_565) {
        format = GL_RGB;
        dataType = GL_UNSIGNED_SHORT_5_6_5;
    }

    glBindTexture(GL_TEXTURE_2D, textureId);

    if (!subImage) {
        glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, dataType, pixels);
    } else {
        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height, format, dataType, pixels);
    }
    glDisable(GL_TEXTURE_2D);
    AndroidBitmap_unlockPixels(env, jbitmap);
}

jintArray Java_com_prestigio_android_ereader_read_curl_CurlMesh_genTextures(JNIEnv *env, jobject jObj, jint count) {
    jintArray result = (env)->NewIntArray(count);
    GLuint texIds[count];
    int textureIds[count];
    glGenTextures(count, texIds);
    for (int i = 0; i < sizeof(texIds); i++) {
        glBindTexture(GL_TEXTURE_2D, texIds[i]);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        textureIds[i] = texIds[i];
    }

    (env)->SetIntArrayRegion(result, 0, count, textureIds);

    return result;
}

void Java_com_prestigio_android_ereader_read_curl_CurlMesh_deleteTextures(JNIEnv *env, jobject jObj, jintArray textureIds, jint count) {
    LOGE("delete textures step 1");
    jint *texIds = (env)->GetIntArrayElements(textureIds, NULL);
    LOGE("delete textures step 2 = %d", count);
    GLuint ids[count];
    for (int i = 0; i < count; ++i) {
        ids[i] = texIds[i];
    }
    LOGE("delete textures step 3");
    glDeleteTextures(count, ids);
    LOGE("delete textures step 4 - end");
}

//void Java_com_prestigio_android_ereader_read_curl_CurlMeshNative_loadShader(JNIEnv *env, jobject jObj, jint shaderId, jint shaderType, jstring shaderSource) {
//    LOGE("load shader");
//    const char *nativeString = (env)->GetStringUTFChars(shaderSource, NULL);
//    getShader(shaderId).loadShader(shaderType, nativeString);
//    env->ReleaseStringUTFChars(shaderSource, nativeString);
//}
//
//void Java_com_prestigio_android_ereader_read_curl_CurlMeshNative_setProgram(JNIEnv *env, jobject jObj, jint shaderId, jstring vertexSource, jstring fragmentSource) {
//    LOGE("load shader");
//    const char *nVertexSource = (env)->GetStringUTFChars(vertexSource, NULL);
//    const char *nFragmentSource = (env)->GetStringUTFChars(fragmentSource, NULL);
//    getShader(shaderId).setProgram(nVertexSource, nFragmentSource);
//    env->ReleaseStringUTFChars(vertexSource, nVertexSource);
//    env->ReleaseStringUTFChars(fragmentSource, nFragmentSource);
//}
//
//void Java_com_prestigio_android_ereader_read_curl_CurlMeshNative_useProgram(JNIEnv *env, jobject jObj, jint shaderId) {
//    getShader(shaderId).useProgram();
//}
//
//jint Java_com_prestigio_android_ereader_read_curl_CurlMeshNative_getHandle(JNIEnv *env, jobject jObj, jint shaderId, jstring name) {
//    const char *nName = env->GetStringUTFChars(name, NULL);
//    int handle = getShader(shaderId).getHandle(nName);
//    env->ReleaseStringUTFChars(name, nName);
//}

void Java_com_prestigio_android_ereader_read_curl_CurlRenderer_OnSurfaceCreated(JNIEnv *env, jobject jObj) {
    mRenderer->onSurfaceCreated();
}

void Java_com_prestigio_android_ereader_read_curl_CurlRenderer_OnSurfaceChange(JNIEnv *env, jobject jObj) {
    mRenderer->onSurfaceChange();
}

void Java_com_prestigio_android_ereader_read_curl_CurlRenderer_OnDrawFrame(JNIEnv *env, jobject jObj) {
    mRenderer->onDrawFrame();
}

}