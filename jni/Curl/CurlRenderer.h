#include "jni.h"

class CurlRenderer {

public:
    CurlRenderer();

    ~CurlRenderer();

    void onSurfaceCreated();

    void onSurfaceChange();

    void onDrawFrame();

};