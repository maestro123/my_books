#include <GLES2/gl2.h>
#include <bits/stl_bvector.h>

struct {
    char *key;
    int value;
} ShaderHandle;

class CurlShader {

public:

    GLuint mProgram;
    std::vector<ShaderHandle> mShaderHandleMap;

    CurlShader();

    ~CurlShader();

    int getHandle(const char *name);

    int loadShader(int shaderType, const char *source);

    void setProgram(const char *vertexSource, const char *fragmentSource);

    void useProgram();

private:
    bool hasValue(char *key) {

    }

};