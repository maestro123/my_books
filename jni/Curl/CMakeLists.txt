cmake_minimum_required(VERSION 2.8.4)
project(Curl)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

include_directories(X:/android-ndk-r10c/platforms/android-19/arch-arm/usr/include)
include_directories(/home/artyom/android-ndk-r10b/platforms/android-19/arch-arm/usr/include)
include_directories(/home/artyom/ndk/platforms/android-19/arch-arm/usr/include)

set(SOURCE_FILES
    CurlNativeHelper.cpp
    CurlMesh.cpp
    CurlRenderer.cpp)

add_executable(Curl ${SOURCE_FILES})