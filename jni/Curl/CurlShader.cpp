#include <bits/stl_pair.h>
#include <malloc.h>
#include "CurlShader.h"

static void checkGlError(const char *op) {
    for (GLint error = glGetError(); error; error
            = glGetError()) {
//        LOGI("after %s() glError (0x%x)\n", op, error);
    }
}

void CurlShader::setProgram(const char *pVertexSource, const char *pFragmentSource) {
    GLuint vertexShader = loadShader(GL_VERTEX_SHADER, pVertexSource);
    if (!vertexShader) {
        return;
    }

    GLuint pixelShader = loadShader(GL_FRAGMENT_SHADER, pFragmentSource);
    if (!pixelShader) {
        return;
    }

    GLuint program = glCreateProgram();
    if (program) {
        glAttachShader(program, vertexShader);
        checkGlError("glAttachShader");
        glAttachShader(program, pixelShader);
        checkGlError("glAttachShader");
        glLinkProgram(program);
        GLint linkStatus = GL_FALSE;
        glGetProgramiv(program, GL_LINK_STATUS, &linkStatus);
        if (linkStatus != GL_TRUE) {
            GLint bufLength = 0;
            glGetProgramiv(program, GL_INFO_LOG_LENGTH, &bufLength);
            if (bufLength) {
                char *buf = (char *) malloc(bufLength);
                if (buf) {
                    glGetProgramInfoLog(program, bufLength, NULL, buf);
//                    LOGE("Could not link program:\n%s\n", buf);
                    free(buf);
                }
            }
            glDeleteProgram(program);
            program = 0;
        }
    }
    mProgram = program;
}

int CurlShader::getHandle(const char *name) {
    if (mShaderHandleMap.find((char *) name) != mShaderHandleMap.end()) {
        return mShaderHandleMap.find((char *) name)->second;
    }
    int handle = glGetAttribLocation(mProgram, name);
    if (handle == -1) {
        handle = glGetUniformLocation(mProgram, name);
    }

    if (handle != -1) {
        mShaderHandleMap.insert(std::pair<char *, int>((char *) name, handle));
    }

    return handle;
}

int CurlShader::loadShader(int shaderType, const char *pSource) {
    GLuint shader = glCreateShader(shaderType);
    if (shader) {
        glShaderSource(shader, 1, &pSource, NULL);
        glCompileShader(shader);
        GLint compiled = 0;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
        if (!compiled) {
            GLint infoLen = 0;
            glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLen);
            if (infoLen) {
                char *buf = (char *) malloc(infoLen);
                if (buf) {
                    glGetShaderInfoLog(shader, infoLen, NULL, buf);
//                    LOGE("Could not compile shader %d:\n%s\n",
//                            shaderType, buf);
                    free(buf);
                }
                glDeleteShader(shader);
                shader = 0;
            }
        }
    }
    return shader;
}

void CurlShader::useProgram() {
    glUseProgram(mProgram);
}